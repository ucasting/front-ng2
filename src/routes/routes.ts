import {Routes} from '@angular/router';
import {AuthGuard} from '../guards/auth-guard';
import {CandidateGuard} from '../guards/candidate-guard';
import {MainComponent} from '../app/+main/main.component';
import {SecurityComponent} from '../app/+security/security.component';
import {LogoutComponent} from '../app/+security/+logout/logout.component';
import {LoginComponent} from '../app/+security/+login/login.component';
import {EditAccountComponent} from '../app/+main/+candidate/+edit-account/edit-account.component';
import {ViewMyProfileComponent} from '../app/+main/+candidate/+view-my-profile/view-my-profile.component';
import {CompanyComponent} from '../app/+main/+company/company.component';
import {SearchCandidatesComponent} from '../app/+main/+company/+search-candidates/search-candidates.component';
import {CandidateComponent} from '../app/+main/+candidate/candidate.component';
import {NotFoundComponent} from '../app/+not-found/not-found.component';
import {ProjectsComponent} from '../app/+main/+company/+projects/projects.component';
import {EditProjectComponent} from '../app/+main/+company/+edit-project/edit-project.component';
import {ProjectListComponent} from '../app/+main/+company/+project-list/project-list.component';
import {ProjectViewComponent} from '../app/+main/+company/+project-view/project-view.component';
import {HirerGuard} from '../guards/hirer-guard';

export const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'security/login',
    pathMatch: 'full'
  },
  {
    path: 'security',
    component: SecurityComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'logout',
        component: LogoutComponent
      },
    ]
  },
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'candidate',
        canActivate: [CandidateGuard],
        component: CandidateComponent,
        children: [
          {
            path: 'edit-account',
            component: EditAccountComponent,
            data: {title: 'Editar Perfil'}
          },
          {
            path: 'profile',
            component: ViewMyProfileComponent,
            data: {title: 'Visualizar Perfil'}
          },
          {
            path: 'search-user-jobs',
            // component: SearchUserJobsComponent,
            loadChildren: 'app/+search-user-jobs/search-user-jobs.module#SearchUserJobsModule'
          }
        ]
      },

      {
        path: 'hirer',
        canActivate: [HirerGuard],
        component: CompanyComponent,
        children: [
          {
            path: 'search-candidates',
            component: SearchCandidatesComponent,
            data: {title: 'Busca de Candidatos'}
          },
          {
            path: 'projects',
            component: ProjectsComponent,
            data: {title: 'Trabalhos'},
            children: [
              {
                path: '',
                component: ProjectListComponent,
                data: {title: 'Lista de Trabalhos'}
              },
              {
                path: 'create',
                component: EditProjectComponent,
                data: {title: 'Cadastrar Trabalho'}
              },
              {
                path: ':id/edit',
                component: EditProjectComponent,
                data: {title: 'Editar Trabalho'}
              },
              {
                path: ':id',
                component: ProjectViewComponent,
              }
            ]
          },
          {
            path: 'user-jobs-selection',
            loadChildren: 'app/+user-jobs-selection/user-jobs-selection.module#UserJobsSelectionModule'
          }
        ]
      }
    ]
  },
  // {
  //   path: 'security',
  //   component: SecurityComponent,
  //   children: [
  //     {
  //       path: 'login',
  //       component: LoginComponent
  //     },
  //     {
  //       path: 'logout',
  //       component: LogoutComponent
  //     },
  //   ]
  // },
  {
    path: '**',
    component: NotFoundComponent
  }
];
