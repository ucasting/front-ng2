import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'entitiesNames'
})
export class EntitiesNames implements PipeTransform {

  transform(value: { name: string }[], args?: any): any {
    if (value && value.length > 0) {
      // map to names
      let names: string[] = value.map(entity => entity.name);
      switch (names.length) {
        case 1:
          return names[0];
        case 2:
          return names.join(' e ');
        default:
          const last = names[value.length - 1];
          const remaining = names.slice(0, -1);
          return `${remaining.join(', ')} e ${last}`;
      }
    }
    return '';
  }

}
