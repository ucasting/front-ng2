import {FirstWordPipe} from './first-word.pipe';

describe('FirstWordPipe', () => {
  const pipe = new FirstWordPipe();

  it('should only get the first word out of a multiword string', () => {
    expect(pipe.transform('hello word')).toBe('hello');
  });

  it('should return the same result if the string is just one word',() => {
    expect(pipe.transform('hello')).toBe('hello');
  });
});
