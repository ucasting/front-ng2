import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'firstWord'
})
export class FirstWordPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    if (value) {
      if (value.indexOf(' ') === -1) {
        return value;
      } else {
        return value.substr(0, value.indexOf(' '));
      }
    }
    return '';
  }

}
