import {Pipe, PipeTransform} from '@angular/core';
import {EntitiesService} from '../services/entities.service';

@Pipe({
  name: 'entity',
  pure: false
})
export class EntityPipe implements PipeTransform {

  constructor(private entitiesService: EntitiesService) {
  }

  transform(value: any, args?: any): any {
    return this.entitiesService.getEntity(args, value);
  }

}
