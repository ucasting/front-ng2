import {MomentPipe} from './moment.pipe';

describe('MomentPipe', () => {
  const pipe = new MomentPipe();

  it('should format a date string into the another format', () => {
    const date = new Date('2016-10-10T12:00:00-00:00');
    expect(pipe.transform(date, 'DD/MM/YY')).toBe('10/10/16');
  });

  it('should format a date string into the another format using the UTC timezone', () => {
    const date = new Date('2016-10-10T12:00:00-00:00');
    expect(pipe.transform(date, 'HH:mm')).toBe('12:00');
  });
});
