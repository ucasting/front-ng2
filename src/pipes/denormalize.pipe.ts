import {Pipe, PipeTransform} from '@angular/core';
import {EntitiesService} from '../services/entities.service';

@Pipe({
  name: 'denormalize',
  pure: false
})
export class DenormalizePipe implements PipeTransform {

  constructor(private entitiesService: EntitiesService) {
  }

  transform(value: any, args: any): any {
    return this.entitiesService.denormalizeEntity(value, args);
  }

}
