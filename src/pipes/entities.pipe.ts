import {Pipe, PipeTransform} from '@angular/core';
import {EntitiesService} from '../services/entities.service';

@Pipe({
  name: 'entities',
  pure: false
})
export class EntitiesPipe implements PipeTransform {

  constructor(private entitiesService: EntitiesService) {
  }

  transform(value: any, args?: any): any {
    return this.entitiesService.getEntities(args, value);
  }

}
