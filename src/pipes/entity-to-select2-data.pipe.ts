import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'entityToSelect2'
})
export class EntityToSelect2DataPipe implements PipeTransform {

  /**
   *
   */
  transform(value: {id: string|number, name: string}[], args?: any): any {
    if (value) {
      return value.map(entry => {
        return {id: entry.id, text: entry.name};
      });
    }
    return [];

  }

}
