import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'roundUp'
})
export class RoundUpPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    if (value) {
      return Math.ceil(value);
    }
    return '';
  }

}
