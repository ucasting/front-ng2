import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers/index';
import {NotificationsActions} from '../actions/notifications-actions';
import {Observable} from 'rxjs/Rx';
import {Notification} from '../models/notification.interface';
// import { v4 } from "node-uuid";
// import v4 = require('uuid');

@Injectable()
export class NotificationsService {

  static TYPE_WARNING: string = 'warning';
  static TYPE_DANGER: string = 'danger';
  static TYPE_INFO: string = 'info';
  static TYPE_SUCCESS: string = 'success';

  private notifications$: Observable<Notification[]>;

  constructor(private store: Store<AppState>, private notificationsActions: NotificationsActions) {
    this.notifications$ = store.select((state: AppState) => state.notifications);
  }

  public getNotifications$() {
    return this.notifications$;
  }

  private add(title: string, message: string, type: string) {
    let id = parseInt((Math.random() * 99999).toFixed(0), 0);
    this.store.dispatch(this.notificationsActions.add(title, message, type, id));
  }

  public remove(id: number) {
    this.store.dispatch(this.notificationsActions.remove(id));
  }

  public addInfo(title: string, message: string) {
    this.add(title, message, NotificationsService.TYPE_INFO);
  }

  public addDanger(title: string, message: string) {
    this.add(title, message, NotificationsService.TYPE_DANGER);
  }

  public addWarning(title: string, message: string) {
    this.add(title, message, NotificationsService.TYPE_WARNING);
  }

  public addSuccess(title: string, message: string) {
    this.add(title, message, NotificationsService.TYPE_SUCCESS);
  }


}
