import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {environment} from '../environments/environment';
import {URLSearchParams} from '@angular/http';
import {convertPayloadToSearchParams} from '../utils/helpers';
import {PaginatedQuery} from '../models/paginated-query.interface';

export const CANDIDATE_TYPE = 'candidate';
export const COMPANY_USER = 'company_user';

export interface QueryParams extends PaginatedQuery {
  type?: string;
  ids?: Array<number>;
  eligible?: boolean;
  genders?: Array<number>;
  approved?: boolean;
  smoker?: boolean;
  eyeColors?: Array<number>;
  skins?: Array<number>;
  hairColors?: Array<number>;
  hairLengths?: Array<number>;
  hairTypes?: Array<number>;
  particularities?: Array<number>;
  physicalTraits?: Array<number>;
  interests?: Array<number>;
  minAge?: number;
  maxAge?: number;
  paginated?: boolean;
}

@Injectable()
export class UsersApiService {

  private path = `${environment.APIPath}users`;

  constructor(private http: AuthHttp) {}

  searchUsers(query: QueryParams = {}) {
    const params: URLSearchParams = convertPayloadToSearchParams(query);
    return this.http.get(`${this.path}`, {search: params});
  }

}
