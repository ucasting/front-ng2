import {Injectable} from '@angular/core';
import {UsersActions} from '../actions/users-actions';
import {UsersState} from '../reducers/users.reducer';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers/index';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UsersService {

  public usersState$: Observable<UsersState>;
  public loadingCandidates$: Observable<boolean>;
  public candidatesFilters$: Observable<any>;
  public candidatesPages$: Observable<number>;
  public currentCandidatesPage$: Observable<number>;
  public maxCandidatesPerPage$: Observable<number>;
  public totalCandidates$: Observable<number>;
  public candidatesIds$: Observable<number[]>;

  constructor(private store: Store<AppState>, private usersActions: UsersActions) {
    this.usersState$ = store.select((state: AppState) => state.users);

    this.loadingCandidates$ = this.usersState$.map((state) => state.loadingCandidates);
    this.candidatesFilters$ = this.usersState$.map((state) => state.candidatesFilters);
    this.candidatesPages$ = this.usersState$.map((state) => state.candidatesPages);
    this.currentCandidatesPage$ = this.usersState$.map((state) => state.currentCandidatesPage);
    this.maxCandidatesPerPage$ = this.usersState$.map((state) => state.maxCandidatesPerPage);
    this.totalCandidates$ = this.usersState$.map((state) => state.totalCandidates);
    this.candidatesIds$ = this.usersState$.map((state) => state.candidatesIds);
  }

  changeCandidatesFilters(filters: any) {
    this.store.dispatch(this.usersActions.changeCandidatesFilters(filters));
  }

  changeCandidatesPage(pageNumber: number) {
    this.store.dispatch(this.usersActions.changeCandidatesPage(pageNumber));
  }

  searchCandidates() {
    this.store.dispatch(this.usersActions.searchCandidates());
  }
}
