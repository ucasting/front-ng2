import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {Store} from '@ngrx/store';
import {Profile} from '../models/profile.model';
import {Observable} from 'rxjs/Rx';
import {Gallery} from '../models/gallery.model';
import {FileUploader} from 'ng2-file-upload/ng2-file-upload';
import {AppState} from '../reducers/index';
import {ProfileActions} from '../actions/profile-actions';
import {ProfileState} from '../reducers/profile.reducer';
import {EntitiesService} from './entities.service';
import {gallery} from '../models/schemas';
import {normalize} from 'normalizr';
import {GalleryMedia} from '../models/gallery-media.model';

@Injectable()
export class ProfileService {

  public profileState$: Observable<ProfileState>;
  public profileId$: Observable<string>;
  public profile$: Observable<Profile>;
  public albumIds$: Observable<string[]>;
  public albums$: Observable<Gallery>;
  public imageUploader: FileUploader;
  public videosAlbum$: Observable<string>;

  constructor(private http: AuthHttp,
              private store: Store<AppState>,
              private profileActions: ProfileActions,
              private entitiesService: EntitiesService) {
    this.profileState$ = this.store.select((state: AppState) => state.profile);
    this.profileId$ = this.store
      .select((state: AppState) => state.profile)
      .map((profile: ProfileState) => profile.profile);
    // this.profile$ = this.store
    // this.albumIds$ = this.entitiesService.getProfile((profile:Profile) => profile.albums);
    // this.videosAlbum$ = this.profileId$.map((profile:Profile) => profile.videos);
    // this.albums
    // this.albums$ = this.entitiesService.getGalleries();
  }

  addAlbum() {
    this.store.dispatch(this.profileActions.addAlbum());
  }

  editAlbum(id: string, formValues: any) {
    this.store.dispatch(this.profileActions.editAlbum(id, formValues));
  }

  removeAlbum(id: string) {
    this.store.dispatch(this.profileActions.removeAlbum(id));
  }

  uploadPhoto(album: Gallery) {
    this.store.dispatch(this.profileActions.addAlbumImage(album));
  }

  addImageSuccess(response: any) {
    this.store.dispatch(this.profileActions.addAlbumImageSuccess(normalize(response, gallery)));
  }

  addImageFail(response: any) {
    this.store.dispatch(this.profileActions.addAlbumImageFail(response));
  }

  deleteGalleryMedia(galleryMedia: GalleryMedia) {
    this.store.dispatch(this.profileActions.removeAlbumImage(galleryMedia.id));
  }


}
