import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState, getEntitiesState, getUser} from '../reducers';
import {Observable} from 'rxjs/Rx';
import {
  getProfile,
  getGallery,
  getGalleryMedia,
  getGender,
  getMedia,
  getParticularity,
  getPhysicalTrait,
  getGroup,
  getIdiom,
  getJobType,
  getJobCategory,
  getCourse,
  getExperience,
  getHashtag,
  getProfileIdiom,
  getSkin,
  getHairColor,
  getHairLength,
  getHairType,
  getEyeColor,
  EntitiesState, EntitiesEntries, EntitiesIds
} from '../reducers/entities.reducer';
import {Schema} from 'normalizr';
import {denormalize} from 'denormalizr';
import {User} from '../models/user.model';

@Injectable()
export class EntitiesService {

  private entitiesState$: Observable<EntitiesState>;

  // async
  private entities$: Observable<EntitiesEntries>;
  private ids$: Observable<EntitiesIds>;
  // sync
  private entities: EntitiesEntries;
  private ids: EntitiesIds;

  // dictionaries
  private users$: Observable<any>;
  private profiles$: Observable<any>;
  private genders$: Observable<any>;
  private skins$: Observable<any>;
  private eyeColors$: Observable<any>;
  private hairColors$: Observable<any>;
  private hairLengths$: Observable<any>;
  private hairTypes$: Observable<any>;
  private medias$: Observable<any>;
  private galleries$: Observable<any>;
  private galleryMedias$: Observable<any>;
  private groups$: Observable<any>;
  private particularities$: Observable<any>;
  private physicalTraits$: Observable<any>;
  private idioms$: Observable<any>;
  private profileIdioms$: Observable<any>;
  private proficiencies$: Observable<any>;
  private jobTypes$: Observable<any>;
  private jobCategories$: Observable<any>;
  private selectionStatuses$: Observable<any>;
  private remunerationTypes$: Observable<any>;
  private benefits$: Observable<any>;
  private courseLevels$: Observable<any>;
  private projects$: Observable<any>;

  // collections
  public userArray$: any;
  public profileArray$: any;
  public genderArray$: any;
  public eyeColorArray$: any;
  public skinArray$: any;
  public hairColorArray$: any;
  public hairLengthArray$: any;
  public hairTypeArray$: any;
  public mediaArray$: any;
  public galleryArray$: any;
  public galleryMediaArray$: any;
  public groupArray$: any;
  public particularityArray$: any;
  public physicalTraitArray$: any;
  public idiomArray$: any;
  public profileIdiomArray$: any;
  public proficiencyArray$: any;
  public jobTypeArray$: any;
  public jobCategoryArray$: any;
  public selectionStatusArray$: any;
  public remunerationTypeArray$: any;
  public benefitArray$: any;
  public courseLevelArray$: any;
  public projectsArray$: any;

  constructor(private store: Store<AppState>) {
    this.entitiesState$ = this.store.let(getEntitiesState());
    this.ids$           = this.entitiesState$
      .select((entitiesState: EntitiesState) => entitiesState.ids);
    this.entities$      = this.entitiesState$
      .select((entitiesState: EntitiesState) => entitiesState.entities);

    this.entities$.subscribe(entities => {
      this.entities = entities;
    });
    this.ids$.subscribe(ids => {
      this.ids = ids;
    });

    // stores the dictionaries for each entity type
    this.users$             = this.entities$.select((entities) => entities.users).share();
    this.profiles$          = this.entities$.select((entities) => entities.profiles).share();
    this.genders$           = this.entities$.select((entities) => entities.genders).share();
    this.eyeColors$         = this.entities$.select((entities) => entities.eyeColors).share();
    this.skins$             = this.entities$.select((entities) => entities.skins).share();
    this.eyeColors$         = this.entities$.select((entities) => entities.eyeColors).share();
    this.hairColors$        = this.entities$.select((entities) => entities.hairColors).share();
    this.hairLengths$       = this.entities$.select((entities) => entities.hairLengths).share();
    this.hairTypes$         = this.entities$.select((entities) => entities.hairTypes).share();
    this.medias$            = this.entities$.select((entities) => entities.medias).share();
    this.galleries$         = this.entities$.select((entities) => entities.galleries).share();
    this.galleryMedias$     = this.entities$.select((entities) => entities.galleryMedias).share();
    this.groups$            = this.entities$.select((entities) => entities.groups).share();
    this.particularities$   = this.entities$.select((entities) => entities.particularities).share();
    this.physicalTraits$    = this.entities$.select((entities) => entities.physicalTraits).share();
    this.idioms$            = this.entities$.select((entities) => entities.idioms).share();
    this.profileIdioms$     = this.entities$.select((entities) => entities.profileIdioms).share();
    this.proficiencies$     = this.entities$.select((entities) => entities.proficiencies).share();
    this.jobTypes$          = this.entities$.select((entities) => entities.jobTypes).share();
    this.jobCategories$     = this.entities$.select((entities) => entities.jobCategories).share();
    this.selectionStatuses$ = this.entities$.select((entities) => entities.selectionStatuses).share();
    this.remunerationTypes$ = this.entities$.select((entities) => entities.remunerationTypes).share();
    this.benefits$          = this.entities$.select((entities) => entities.benefits).share();
    this.courseLevels$      = this.entities$.select((entities) => entities.courseLevels).share();
    this.projects$          = this.entities$.select((entities) => entities.projects).share();

    // Arrays holds the entities themselves
    this.userArray$          = this.users$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.profileArray$       = this.profiles$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.genderArray$        = this.genders$.map(dictionary => {
      return Object.keys(dictionary).map(id => dictionary[id]);
    }).share();
    this.skinArray$          = this.skins$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.eyeColorArray$      = this.eyeColors$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.hairColorArray$     = this.hairColors$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.hairLengthArray$    = this.hairLengths$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.hairTypeArray$      = this.hairTypes$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.mediaArray$         = this.medias$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.galleryArray$       = this.galleries$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.galleryMediaArray$  = this.galleryMedias$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.groupArray$         = this.groups$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.particularityArray$ = this.particularities$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.physicalTraitArray$ = this.physicalTraits$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.idiomArray$         = this.idioms$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.profileIdiomArray$  = this.profileIdioms$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.proficiencyArray$   = this.proficiencies$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.jobTypeArray$       = this.jobTypes$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.jobCategoryArray$   = this.jobCategories$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();

    this.selectionStatusArray$  = this.selectionStatuses$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.remunerationTypeArray$ = this.remunerationTypes$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.benefitArray$          = this.benefits$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.courseLevelArray$      = this.courseLevels$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
    this.projectsArray$         = this.projects$.map(dictionary => Object.keys(dictionary).map(id => dictionary[id])).share();
  }


  public denormalizeEntity(entity: any, schema: Schema): any {
    return denormalize(entity, this.entities, schema);
  };

  public denormalizeIds(entityIds: number[], schema: Schema, entityType: string): any[] {
    return entityIds.map((id) => {
      const entity = this.getEntity(entityType, id.toString());
      return denormalize(entity, this.entities, schema);
    });
  };

  public getEntity(entityType: string, id: string|number) {
    return this.entities[entityType][id];
  }

  public getEntities(entityType: string, ids: Array<string|number>) {
    const entities = [];
    if (ids) {
      ids.map(id => {
        entities.push(this.entities[entityType][id]);
      });
    }
    return entities;
  }

  public getEntitiesDictionary() {
    return this.entities;
  }

  public getEntitiesIds() {
    return this.ids;
  }

  //
  // public getUser(id:string|number):Observable<User>{
  //   return this.entitiesState$.let(getUser(id));
  // }
  public getUser(id$: Observable<string|number>): Observable<User> {
    return id$.switchMap(id => this.store.let(getUser(id)));
  }

  // public getProfile(id: string|number): Observable<Profile> {
  //   return this.entitiesState$.let<Profile>(
  //     getProfile(id)
  //   );
  // }
  //
  // public getMedia(id: string|number): Observable<Media> {
  //   return this.entitiesState$.let<Media>(getMedia(id));
  // }
  //
  // public getGalleryMedia(id: string|number): Observable<GalleryMedia> {
  //   return this.entitiesState$.let<GalleryMedia>(getGalleryMedia(id));
  // }
  //
  // public getGallery(id: string|number): Observable<Gallery> {
  //   return this.entitiesState$.let<Gallery>(getGallery(id));
  // }
  //
  // public getParticularity(id: string|number): Observable<Particularity> {
  //   return this.entitiesState$.let<Particularity>(getParticularity(id));
  // }
  //
  // public getPhysicalTrait(id: string|number): Observable<PhysicalTrait> {
  //   return this.entitiesState$.let<PhysicalTrait>(getPhysicalTrait(id));
  // }
  //
  // public getIdiom(id: string|number): Observable<Idiom> {
  //   return this.entitiesState$.let<Idiom>(getIdiom(id));
  // }
  //
  // public getGroup(id: string|number): Observable<Group> {
  //   return this.entitiesState$.let<Group>(getGroup(id));
  // }
  //
  // public getJobType(id: string|number): Observable<JobType> {
  //   return this.entitiesState$.let<JobType>(getJobType(id));
  // }
  //
  // public getJobCategory(id: string|number): Observable<JobCategory> {
  //   return this.entitiesState$.let<JobCategory>(getJobCategory(id));
  // }
  //
  // public getProfileIdiom(id: string|number): Observable<ProfileIdiom> {
  //   return this.entitiesState$.let<ProfileIdiom>(getProfileIdiom(id));
  // }
  //
  // public getCourse(id: string|number): Observable<Course> {
  //   return this.entitiesState$.let<Course>(getCourse(id));
  // }
  //
  // public getExperience(id: string|number): Observable<Experience> {
  //   return this.entitiesState$.let<Experience>(getExperience(id));
  // }
  //
  // public getHashtag(id: string|number): Observable<Hashtag> {
  //   return this.entitiesState$.let<Hashtag>(getHashtag(id));
  // }
  //
  // public getSkin(id: string|number): Observable<Skin> {
  //   return this.entitiesState$.let<Skin>(getSkin(id));
  // }
  //
  // public getHairColor(id: string|number): Observable<HairColor> {
  //   return this.entitiesState$.let<HairColor>(getHairColor(id));
  // }
  //
  // public getHairLength(id: string|number): Observable<HairLength> {
  //   return this.entitiesState$.let<HairLength>(getHairLength(id));
  // }
  //
  // public getHairType(id: string|number): Observable<HairType> {
  //   return this.entitiesState$.let<HairType>(getHairType(id));
  // }
  //
  // public getEyeColor(id: string|number): Observable<EyeColor> {
  //   return this.entitiesState$.let<EyeColor>(getEyeColor(id));
  // }
  //
  // public getGender(id: string|number): Observable<Gender> {
  //   return this.entitiesState$.let<Gender>(getGender(id));
  // }

  //
  // public getGalleries(ids:string[]):Observable<Gallery>{
  //   return this.entitiesState$.let(getGalleries(ids));
  // }


}
