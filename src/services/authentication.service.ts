import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Store} from '@ngrx/store';
import {AuthState} from '../reducers/auth.reducer';
import {AppState} from '../reducers/index';
import {AuthActions} from '../actions/auth-actions';
import {JwtHelper} from 'angular2-jwt/angular2-jwt';

@Injectable()
export class AuthenticationService {

  private state$: Observable<AuthState>;
  private token$: Observable<string>;
  private token: string;
  private hasError$: Observable<boolean>;
  private authChecking$: Observable<boolean>;

  private jwtHelper: JwtHelper;


  constructor(private store: Store<AppState>,
              private authActions: AuthActions) {
    this.jwtHelper = new JwtHelper();
    this.state$ = this.store.select((state: AppState) => {
      return state.auth;
    });
    this.token$ = this.state$.map((state: AuthState) => {
      return state.token;
    });
    this.token$.subscribe((token: string) => {
      this.token = token;
    });
    this.authChecking$ = this.state$.map((state: AuthState) => {
      return state.authChecking;
    });
    this.hasError$ = this.state$.map((state: AuthState) => {
      return state.hasError;
    });
  }

  public login(username: string, password: string) {
    this.store.dispatch(this.authActions.login(username, password));
  }

  public logout(): void {
    this.store.dispatch(this.authActions.logout());
  }

  public getToken$(): Observable<string> {
    return this.token$;
  }

  public getAuthChecking(): Observable<boolean> {
    return this.authChecking$;
  }

  public getHasError(): Observable<boolean> {
    return this.hasError$;
  }

  hasValidToken(): boolean {
    if (this.token) {
      return !this.jwtHelper.isTokenExpired(this.token);
    }
    return false;
  }
}
