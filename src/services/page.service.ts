import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {Observable} from 'rxjs/Rx';
import {PageState} from '../reducers/page.reducer';
import {PageActions} from '../actions/page-actions';

@Injectable()
export class PageService {

  page$: Observable<PageState>;

  constructor(private store: Store<AppState>, private pageActions: PageActions) {
    this.page$ = this.store.select((state) => state.page);

  }

  public changeTitle(title: string) {
    this.store.dispatch(this.pageActions.changeTitle(title));
  }

  public toggleNavigation() {
    this.store.dispatch(this.pageActions.toggleNavigation());
  }

}
