import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {environment} from '../environments/environment';
import {Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {convertPayloadToSearchParams} from '../utils/helpers';

@Injectable()
export class ProjectsApiService {

  constructor(private http: AuthHttp) {

  }

  loadMyCompanyProjects() {
    return this.http.get(`${environment.APIPath}projects/my/company`);
  }

}
