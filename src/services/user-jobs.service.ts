import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {User} from '../models/user.model';
import {UserJobsActions} from '../actions/user-jobs-actions';
import {UserJob} from '../models/user-job.model';

@Injectable()
export class UserJobsService {

  constructor(private store: Store<AppState>,
              private userJobsActions: UserJobsActions) {
  }

  refreshForm() {
    this.store.dispatch(this.userJobsActions.refreshForm());
  }

  inviteCandidate(job: UserJob, candidate: User) {
    this.store.dispatch(this.userJobsActions.inviteCandidate(job, candidate));
  }

  public selectCandidateLegacy(candidate: User) {
    this.store.dispatch(this.userJobsActions.selectCandidateLegacy(candidate));
  }

  public createJob(projectId, job: UserJob) {
    this.store.dispatch(this.userJobsActions.createJob(projectId, job));
  }

  public saveJob(projectId, id: number, job: UserJob) {
    this.store.dispatch(this.userJobsActions.saveJob(projectId, id, job));
  }


}
