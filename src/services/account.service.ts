import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {FileUploader, FileUploaderOptions} from 'ng2-file-upload/ng2-file-upload';
import {AuthenticationService} from './authentication.service';
import {User} from '../models/user.model';
import {AccountState} from '../reducers/account.reducer';
import {AppState} from '../reducers/index';
import {AccountActions} from '../actions/account-actions';
import {EntitiesService} from './entities.service';
import {normalize} from 'normalizr';
import {user} from '../models/schemas';


@Injectable()
export class AccountService {

  private account$: Observable<AccountState>;
  private userId$: Observable<string>;
  private user$: Observable<User>;
  private updating$: Observable<boolean>;
  private error$: Observable<string>;
  private saved$: Observable<boolean>;
  private validationErrors$: Observable<any>;

  private avatarUploader: FileUploader;
  private coverUploader: FileUploader;

  private roles$: Observable<string[]>;
  private roles: string[];


  constructor(private http: AuthHttp,
              private store: Store<AppState>,
              private authService: AuthenticationService,
              private accountActions: AccountActions,
              private entitiesService: EntitiesService) {
    this.account$ = store.select((state) => state.account);
    this.userId$ = this.account$.map((account: AccountState) => account.user);
    this.user$ = this.entitiesService.getUser(this.userId$);
    this.updating$ = this.account$.map((account: AccountState) => account.updating);
    this.error$ = this.account$.map((account: AccountState) => account.error);
    this.saved$ = this.account$.map((account: AccountState) => account.saved);
    this.validationErrors$ = this.account$.map((account: AccountState) => account.validationErrors);

    this.avatarUploader = new FileUploader(this.getBaseUploaderOptions());
    this.coverUploader = new FileUploader(this.getBaseUploaderOptions());

    this.avatarUploader.options.url = `${environment.APIPath}my/avatar`;
    this.coverUploader.options.url = `${environment.APIPath}my/cover`;

    this.roles$ = this.user$.map((user: User) => {
      if (user) {
        return user.roles;
      }
      return [];
    });

    this.roles$.subscribe((roles) => {
      this.roles = roles;
    });

    this.avatarUploader.onSuccessItem = (item, response, status, header) => {
      this.store.dispatch(this.accountActions.editAvatarSuccess(normalize(JSON.parse(response), user)));
    };

    this.avatarUploader.onErrorItem = (item, response, status, header) => {
      this.store.dispatch(this.accountActions.editAvatarFail(JSON.parse(response)));
    };

    this.coverUploader.onSuccessItem = (item, response, status, header) => {
      this.store.dispatch(this.accountActions.editCoverSuccess(normalize(JSON.parse(response), user)));
    };

    this.coverUploader.onErrorItem = (item, response, status, header) => {
      this.store.dispatch(this.accountActions.editCoverFail(JSON.parse(response)));
    };

    // updates the authtoken for the uploaders whenever it is changed
    authService.getToken$().subscribe(
      token => {
        this.updateUploaderTokens(token);
      }
    );
  }

  private getBaseUploaderOptions(): FileUploaderOptions {
    return {
      // allowedFileType: ['jpg', 'jpeg', 'png', 'gif'],
      autoUpload: true,
      // isHTML5: true,
      removeAfterUpload: true
    };
  }

  private updateUploaderTokens(token: string): void {
    this.avatarUploader.authToken = 'Bearer ' + token;
    this.coverUploader.authToken = 'Bearer ' + token;
  }

  public getUserId(): Observable<string> {
    return this.userId$;
  }

  public getUser(): Observable<User> {
    return this.user$;
  }

  public getUpdating(): Observable<boolean> {
    return this.updating$;
  }

  public hasRole(role: string): boolean {
    return (this.roles.indexOf(role) !== -1);
  }

  editAccount(userData) {
    this.store.dispatch(this.accountActions.editAccount(userData));
  }

  getAvatarUploader(): FileUploader {
    return this.avatarUploader;
  }

  getCoverUploader(): FileUploader {
    return this.coverUploader;
  }

  getErrorMessage(): Observable<string> {
    return this.error$;
  }

  getHasSaved(): Observable<boolean> {
    return this.saved$;
  }

  getValidationErrors(): Observable<any> {
    return this.validationErrors$;
  }

}
