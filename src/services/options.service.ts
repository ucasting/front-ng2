import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {Observable} from 'rxjs/Rx';
import {OptionsState} from '../reducers/options.reducer';

@Injectable()
export class OptionsService {

  options$: Observable<OptionsState>;
  isLoaded$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.options$ = this.store.select((state) => state.options);
    this.isLoaded$ = this.options$.map((options: OptionsState) => options.loaded);

  }

}
