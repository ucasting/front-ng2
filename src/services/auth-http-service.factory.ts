import {AuthConfig, AuthHttp} from 'angular2-jwt';
import {RequestOptions, Http} from '@angular/http';
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({
    tokenName:     'id_token',
    tokenGetter:   (() => localStorage.getItem('id_token')),
    globalHeaders: [{'Content-Type': 'application/json'}],
  }), http, options);
}
