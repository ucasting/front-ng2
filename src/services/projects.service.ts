import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../reducers';
import {ProjectsActions} from '../actions/projects-actions';
import {Project} from '../models/project.model';
import {MyCompanyProjectsState} from '../reducers/my-company-projects.reducer';
import {Observable} from 'rxjs/Observable';
import {UserJob} from '../models/user-job.model';

@Injectable()
export class ProjectsService {

  public myCompanyProjectsState$: Observable<MyCompanyProjectsState>;
  public state: MyCompanyProjectsState;
  public myCompanyUserJobs$: Observable<string[]>;
  public myCompanyProjects$: Observable<string[]>;

  constructor(private store: Store<AppState>,
              private projectsActions: ProjectsActions) {
    this.myCompanyProjectsState$ = store.select((state: AppState) => state.myCompanyProjects);
    this.myCompanyProjectsState$.subscribe((state) => {
      this.state = state;
    });
    this.myCompanyProjects$ = this.myCompanyProjectsState$.map((state) => state.myCompanyProjects);
    this.myCompanyUserJobs$ = this.myCompanyProjectsState$.map((state) => state.myCompanyUserJobs);
  }

  loadMyCompanyProjects(refresh = false) {
    if (refresh) {
      this.store.dispatch(this.projectsActions.loadMyCompanyProjects());
    } else if (!this.state.projectsLoaded) {
      this.store.dispatch(this.projectsActions.loadMyCompanyProjects());
    }
  }

  refreshForm() {
    this.store.dispatch(this.projectsActions.refreshForm());
  }

  refreshUserJobForm() {
    this.store.dispatch(this.projectsActions.refreshUserJobForm());
  }

  public createProject(project: Project) {
    this.store.dispatch(this.projectsActions.createProject(project));
  }

  public saveProject(project: Project, id = null) {
    if (id !== null) {
      this.store.dispatch(this.projectsActions.saveProject(id, project));
    } else {
      this.store.dispatch(this.projectsActions.createProject(project));
    }
  }


  newUserJobForm() {
    this.store.dispatch(this.projectsActions.newUserJobForm());
  }

  closeUserJobForm() {
    this.store.dispatch(this.projectsActions.closeUserJobForm());
  }

  saveUserJob(projectId, userJob: UserJob, userJobId = null) {
    if (userJobId !== null) {
      this.store.dispatch(this.projectsActions.saveUserJob(projectId, userJobId, userJob));
    } else {
      this.store.dispatch(this.projectsActions.createUserJob(projectId, userJob));
    }
  }

  publishUserJob(userJobId: any) {
    this.store.dispatch(this.projectsActions.publishUserJob(userJobId));
  }

  cancelUserJob(userJobId: any) {
    this.store.dispatch(this.projectsActions.cancelUserJob(userJobId));
  }

  editUserJob(userJob: UserJob) {
    this.store.dispatch(this.projectsActions.editUserJob(userJob));
  }
}
