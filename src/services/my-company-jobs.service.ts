import {Store} from '@ngrx/store';
import {AppState} from '../reducers/index';
import {UserJobsActions} from '../actions/user-jobs-actions';
import {Observable} from 'rxjs/Rx';
import {MyCompanyJobsState} from '../reducers/my-company-jobs.reducer';
import {Injectable} from '@angular/core';
import {FormErrors} from '../models/form-errors.interface';

@Injectable()
export class MyCompanyJobsService {


  protected state$: Observable<MyCompanyJobsState>;
  protected userJobValidationErrors$: Observable<FormErrors>;
  private myCompanyUserJobs$: Observable<any[]>;

  constructor(private store: Store<AppState>,
              private userJobsActions: UserJobsActions) {
    this.state$ = store.select((state: AppState) => state.myCompanyJobs);
    this.userJobValidationErrors$ = this.state$.map((state: MyCompanyJobsState) => state.userJobValidationErrors);
    this.myCompanyUserJobs$ = this.state$.map((state: MyCompanyJobsState) => state.myCompanyUserJobs);
  }

  getState$(): Observable<MyCompanyJobsState> {
    return this.state$;
  }

  getUserJobValidationErrors$(): Observable<FormErrors> {
    return this.userJobValidationErrors$;
  }

  getMyCompanyUserJobs$(): Observable<any[]> {
    return this.myCompanyUserJobs$;
  }

}
