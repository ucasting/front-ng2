import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {environment} from '../environments/environment';
import {Response, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {convertPayloadToSearchParams} from '../utils/helpers';

@Injectable()
export class UserJobsApiService {

  private path = `${environment.APIPath}userjobs`;

  constructor(private http: AuthHttp) {

  }

  searchUserJobs(query: any = {}) {
    let params: URLSearchParams = convertPayloadToSearchParams(query);
    return this.http.get(`${this.path}`, {search: params});
  }

  searchCompatibleUserJobs(query: any = {}) {
    let params: URLSearchParams = convertPayloadToSearchParams(query);
    return this.http.get(`${this.path}/compatible`, {search: params});
  }

  applyForJob(jobId: number) {
    return this.http.post(`${this.path}/${jobId}/apply`, null);
  }

  cancelApplicationForJob(jobId: number) {
    return this.http.post(`${this.path}/${jobId}/cancel-application`, null);
  }

  inviteCandidate(jobId, candidateId): Observable<Response> {
    return this.http.post(`${this.path}/${jobId}/invite/${candidateId}`, null);
  }

  selectCandidate(jobId: number, selectionId: number) {
    return this.http.post(`${this.path}/${jobId}/selections/${selectionId}/select`, null);
  }

  approveCandidate(jobId: number, selectionId: number) {
    return this.http.post(`${this.path}/${jobId}/selections/${selectionId}/approve`, null);
  }

  rejectCandidate(jobId: number, selectionId: number) {
    return this.http.post(`${this.path}/${jobId}/selections/${selectionId}/reject`, null);
  }



  loadSelections(jobId: number) {
    return this.http.get(`${this.path}/${jobId}/selections`);
  }
}
