// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { AccountAvatarComponent } from './account-avatar.component';
//
// describe('Component: AccountAvatar', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [AccountAvatarComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([AccountAvatarComponent],
//       (component: AccountAvatarComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(AccountAvatarComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(AccountAvatarComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-account-avatar></uc-account-avatar>
//   `,
//   directives: [AccountAvatarComponent]
// })
// class AccountAvatarComponentTestController {
// }
//
