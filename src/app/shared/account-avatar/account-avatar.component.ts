import {Component, Input, ChangeDetectionStrategy} from '@angular/core';
import {User} from '../../../models/user.model';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'uc-account-avatar',
  templateUrl: 'account-avatar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountAvatarComponent {
  @Input() user: User;

  backendPath: string;

  constructor() {
    this.backendPath = environment.backendPath;
  }

}
