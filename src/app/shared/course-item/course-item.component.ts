import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Course} from '../../../models/course.model';

@Component({
  selector: 'uc-course-item',
  templateUrl: 'course-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemComponent implements OnInit {

  @Input() course: Course;

  constructor() {
  }

  ngOnInit() {
  }

}
