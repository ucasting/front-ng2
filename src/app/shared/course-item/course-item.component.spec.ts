// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { CourseItemComponent } from './course-item.component';
//
// describe('Component: CourseItem', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [CourseItemComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([CourseItemComponent],
//       (component: CourseItemComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(CourseItemComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(CourseItemComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-course-item></uc-course-item>
//   `,
//   directives: [CourseItemComponent]
// })
// class CourseItemComponentTestController {
// }
//
