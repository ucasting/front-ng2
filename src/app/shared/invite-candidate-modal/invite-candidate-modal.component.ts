import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user.model';
import {UserJob} from '../../../models/user-job.model';
import {UserJobsService} from '../../../services/user-jobs.service';
import {Store} from '@ngrx/store';
import {AppState} from '../../../reducers/index';
import {FormControl, Validators} from '@angular/forms';
import {UserJobsSelectionState} from '../../+user-jobs-selection/user-jobs-selection.reducer';
import {Project} from '../../../models/project.model';
import {EntitiesService} from '../../../services/entities.service';
import {
  SelectProjectAction,
  SelectUserJobAction,
  LoadProjectsAction
} from '../../+user-jobs-selection/user-jobs-selection.actions';
import {MdDialogRef} from '@angular/material';

@Component({
  selector:    'uc-invite-candidate-modal',
  templateUrl: 'invite-candidate-modal.component.html',
})
export class InviteCandidateModalComponent implements OnInit {

  public candidate: User;
  public userJobs: UserJob[] = [];
  public selectControl: FormControl;
  private state;
  public loadingProjects;
  public projects;
  private state$;
  private entitiesState$;
  public selectedUserJob: UserJob;

  constructor(public dialog: MdDialogRef<InviteCandidateModalComponent>,
              protected userJobsService: UserJobsService,
              protected store: Store<AppState>,
              protected entitiesService: EntitiesService) {
    this.selectControl = new FormControl('', Validators.required);
    this.state$ = this.store.select((state: AppState) => state.userJobsSelection);
    this.entitiesState$ = this.store.select((state: AppState) => state.entities);
    this.state$.subscribe((state: UserJobsSelectionState) => {
      this.state = state;
      this.loadingProjects = state.loadingProjects;
      this.projects = this.entitiesService.getEntities('projects', state.projects);
      if (state.selectedProjectId) {
        const selectedProject: Project = this.projects.find(project => project.id === state.selectedProjectId);
        this.userJobs = this.entitiesService.getEntities('userJobs', selectedProject.userJobs as number[]);
        this.userJobs = this.userJobs.filter(userJob => userJob.approved);
      } else {
        this.userJobs = [];
      }
      if (state.selectedUserJobId) {
        this.selectedUserJob = this.userJobs.find(userJob => userJob.id === state.selectedUserJobId);
      }
    });
  }

  projectChanged(event) {
    const projectId = event.target.value !== '' ? parseInt(event.target.value, 0) : null;
    this.store.dispatch(new SelectProjectAction(projectId));
  }

  userJobChanged(event) {
    const jobId = event.target.value !== '' ? parseInt(event.target.value, 0) : null;
    this.store.dispatch(new SelectUserJobAction(jobId));
  }

  confirm() {
    this.userJobsService.inviteCandidate(this.selectedUserJob, this.candidate);
    this.dialog.close();
  }

  ngOnInit() {
    this.store.dispatch(new LoadProjectsAction());
  }

  close(): void {
    this.dialog.close();
  }

  beforeDismiss(): boolean {
    return false;
  }

  beforeClose(): boolean {
    return false;
  }

}
