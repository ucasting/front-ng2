import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {ProfileIdiom} from '../../../models/profile-idiom.model';

@Component({
  selector: 'uc-idiom-list',
  templateUrl: 'idiom-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IdiomListComponent implements OnInit {

  @Input() profileIdioms: ProfileIdiom[];

  constructor() {}

  ngOnInit() {
  }

}
