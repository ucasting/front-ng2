// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { AvatarBoxComponent } from './avatar-box.component';
//
// describe('Component: AvatarBox', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [AvatarBoxComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([AvatarBoxComponent],
//       (component: AvatarBoxComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(AvatarBoxComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(AvatarBoxComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-avatar-box></uc-avatar-box>
//   `,
//   directives: [AvatarBoxComponent]
// })
// class AvatarBoxComponentTestController {
// }
//
