import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-avatar-box',
  templateUrl: 'avatar-box.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarBoxComponent implements OnInit {

  @Input() avatar;

  constructor() {
  }

  ngOnInit() {
  }

}
