import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Course} from '../../../models/course.model';

@Component({
  selector: 'uc-course-list',
  templateUrl: 'course-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseListComponent implements OnInit {

  @Input() courses: Course[];

  constructor() {
  }

  ngOnInit() {
  }

}
