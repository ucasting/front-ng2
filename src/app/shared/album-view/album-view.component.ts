import {Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {Gallery} from '../../../models/gallery.model';
import {Media} from '../../../models/media.model';

@Component({
  selector: 'uc-album-view',
  templateUrl: 'album-view.component.html',
})
export class AlbumViewComponent implements OnInit, OnChanges {

  @Input() album: Gallery;
  @Output() goBack: EventEmitter<any> = new EventEmitter<any>();

  public medias: Media[] = [];

  constructor() {
  }

  ngOnInit() {
    console.log(this.album);
    this.medias = this.album.medias.map((galleryMedia) => galleryMedia.media);
    console.log(this.medias);
  }

  ngOnChanges(changes: SimpleChanges): any {
    console.log(changes);
  }

  public onBackClicked(event: Event) {
    // bug fix for modal closing
    event.stopPropagation();
    this.goBack.emit(null);
  }

}
