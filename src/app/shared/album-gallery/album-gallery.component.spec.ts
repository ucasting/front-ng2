// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { AlbumGalleryComponent } from './album-gallery.component';
//
// describe('Component: AlbumGallery', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [AlbumGalleryComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([AlbumGalleryComponent],
//       (component: AlbumGalleryComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(AlbumGalleryComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(AlbumGalleryComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-album-gallery></uc-album-gallery>
//   `,
//   directives: [AlbumGalleryComponent]
// })
// class AlbumGalleryComponentTestController {
// }
//
