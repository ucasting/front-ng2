import {Component, OnInit, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import {Notification} from '../../../models/notification.interface';
import {Observable, Subscription} from 'rxjs/Rx';

@Component({
  selector: 'uc-notification-item',
  templateUrl: 'notification-item.component.html',
})
export class NotificationItemComponent implements OnInit, OnDestroy {

  @Input() notification: Notification;
  @Output() remove: EventEmitter<any> = new EventEmitter<any>();

  subscription: Subscription;

  constructor() {
  }

  ngOnInit() {
    this.subscription = Observable.of(true).delay(4000).subscribe(
      () => {
        this.remove.emit(this.notification);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
