import {
  Component, Input, EventEmitter, Output, ChangeDetectionStrategy, OnChanges,
  SimpleChanges
} from '@angular/core';
import {User} from '../../../models/user.model';
import {Media} from '../../../models/media.model';
import {Profile} from '../../../models/profile.model';
import {CandidateModalComponent} from '../candidate-modal/candidate-modal.component';
import {MdDialog} from '@angular/material';

@Component({
  selector:        'uc-candidate-card',
  templateUrl:     'candidate-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandidateCardComponent implements OnChanges {

  @Input() candidate: User;
  @Input() showInvite: boolean;
  @Input() showSelect = true;
  @Input() showApprove: boolean;
  @Input() showReject: boolean;
  // @Output() viewUser: EventEmitter<User> = new EventEmitter<User>();
  // @Output() viewPortfolio: EventEmitter<User> = new EventEmitter<User>();
  @Output() selectUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() inviteUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() rejectUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() approveUser: EventEmitter<User> = new EventEmitter<User>();

  private currentImageIndex = 0;
  private images: Media[] = [];

  constructor(private dialog: MdDialog) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // loaded
    if (this.candidate && this.candidate.firstName) {
      if (this.candidate.avatar) {
        this.images.push(this.candidate.avatar);
      }
      const profile: Profile = this.candidate.profile;
      // get main gallery
      const composite = profile.albums.find((album) => album.main);
      // get main gallery images
      if (composite) {
        composite.medias.forEach((galleryMedia) => {
          this.images.push(galleryMedia.media);
        });
      }
    }
  }

  public viewProfile(): void {
    this.openCandidateModal(this.candidate, 0);
  }

  public viewPortfolio(): void {
    this.openCandidateModal(this.candidate, 1);
  }

  protected openCandidateModal(candidate: User, tab = 0) {
    const dialogComponent = this.dialog.open(CandidateModalComponent).componentInstance;
    dialogComponent.candidate = candidate;
    dialogComponent.tabIndex = tab;
  }

  public nextImage() {
    if (this.images.length > 0) {
      if (this.currentImageIndex < (this.images.length - 1)) {
        this.currentImageIndex++;
      } else {
        this.currentImageIndex = 0;
      }
    }

  }

  public prevImage() {
    if (this.images.length > 0) {
      if (this.currentImageIndex > 0) {
        this.currentImageIndex--;
      } else {
        this.currentImageIndex = this.images.length - 1;
      }
    }
  }

}
