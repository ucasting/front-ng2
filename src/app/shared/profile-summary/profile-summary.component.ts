import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-profile-summary',
  templateUrl: 'profile-summary.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileSummaryComponent implements OnInit {

  @Input() profile;

  constructor() {}

  ngOnInit() {
  }

}
