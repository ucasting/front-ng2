// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ProfileSummaryComponent } from './profile-summary.component';
//
// describe('Component: ProfileSummary', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ProfileSummaryComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ProfileSummaryComponent],
//       (component: ProfileSummaryComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ProfileSummaryComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ProfileSummaryComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-profile-summary></uc-profile-summary>
//   `,
//   directives: [ProfileSummaryComponent]
// })
// class ProfileSummaryComponentTestController {
// }
//
