import {Component} from '@angular/core';
import {NotificationsService} from '../../../services/notifications.service';
import {Notification} from '../../../models/notification.interface';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'uc-notification-list',
  templateUrl: 'notification-list.component.html',
  styleUrls: ['notification-list.component.css'],
})
export class NotificationListComponent {

  notifications$: Observable<Notification[]>;

  constructor(private notificationsService: NotificationsService) {
    this.notifications$ = this.notificationsService.getNotifications$();
  }

  notificationToRemove(notification: Notification) {
    this.notificationsService.remove(notification.id);
  }

}
