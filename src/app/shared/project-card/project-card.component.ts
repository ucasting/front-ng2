import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {Project} from '../../../models/project.model';

@Component({
  selector: 'uc-project-card',
  templateUrl: 'project-card.component.html',
  styles: [`
    .jobs-text {
        margin-bottom: 3px;
    }        
`]
})
export class ProjectCardComponent implements OnInit {

  @Input() project: Project;
  @Output() selectProject: EventEmitter<Project> = new EventEmitter<Project>();

  constructor() {
  }

  ngOnInit() {

  }

}
