import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Experience} from '../../../models/experience.model';

@Component({
  selector: 'uc-experience-item',
  templateUrl: 'experience-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceItemComponent implements OnInit {

  @Input() experience: Experience;

  constructor() {
  }

  ngOnInit() {
  }

}
