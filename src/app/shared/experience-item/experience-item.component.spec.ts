// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ExperienceItemComponent } from './experience-item.component';
//
// describe('Component: ExperienceItem', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ExperienceItemComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ExperienceItemComponent],
//       (component: ExperienceItemComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ExperienceItemComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ExperienceItemComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-experience-item></uc-experience-item>
//   `,
//   directives: [ExperienceItemComponent]
// })
// class ExperienceItemComponentTestController {
// }
//
