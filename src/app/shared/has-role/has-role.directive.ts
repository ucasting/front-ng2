import {Directive, ViewContainerRef, TemplateRef, Input} from '@angular/core';
import {AccountService} from '../../../services/account.service';

@Directive({selector: '[ucHasRole]'})
export class HasRoleDirective {
  private _prevCondition: boolean = null;

  constructor(private _viewContainer: ViewContainerRef, private _templateRef: TemplateRef<Object>, private accountService: AccountService) {
  }

  @Input()
  set ucHasRole(role: string) {
    if (this.accountService.hasRole(role) && (this._prevCondition === null || !this._prevCondition)) {
      this._prevCondition = true;
      this._viewContainer.createEmbeddedView(this._templateRef);
    } else if (!this.accountService.hasRole(role) && (this._prevCondition !== null || this._prevCondition)) {
      this._prevCondition = false;
      this._viewContainer.clear();
    }
  }
}
