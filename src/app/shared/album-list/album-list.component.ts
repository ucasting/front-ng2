import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Gallery} from '../../../models/gallery.model';

@Component({
  selector: 'uc-album-list',
  templateUrl: 'album-list.component.html',
})
export class AlbumListComponent implements OnInit {

  @Input() albums: Gallery[];
  @Output() albumSelect: EventEmitter<Gallery> = new EventEmitter<Gallery>();

  constructor() {
  }

  ngOnInit() {
  }

}
