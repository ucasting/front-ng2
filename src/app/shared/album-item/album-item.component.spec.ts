// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { AlbumItemComponent } from './album-item.component';
//
// describe('Component: AlbumItem', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [AlbumItemComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([AlbumItemComponent],
//       (component: AlbumItemComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(AlbumItemComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(AlbumItemComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-album-item></uc-album-item>
//   `,
//   directives: [AlbumItemComponent]
// })
// class AlbumItemComponentTestController {
// }
//
