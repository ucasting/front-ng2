import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {Gallery} from '../../../models/gallery.model';

@Component({
  selector: 'uc-album-item',
  templateUrl: 'album-item.component.html',
})
export class AlbumItemComponent implements OnInit {

  @Input() album: Gallery;
  @Output() albumSelect: EventEmitter<Gallery> = new EventEmitter<Gallery>();

  constructor() {
  }

  ngOnInit() {
  }

  public onAlbumClick(event: Event) {
    // prevents bug from modal closing
    event.stopPropagation();
    this.albumSelect.emit(this.album);
  }

}
