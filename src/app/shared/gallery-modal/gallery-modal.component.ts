import {Component, OnInit} from '@angular/core';
import {ModalComponent, DialogRef} from 'angular2-modal';
import {Media} from '../../../models/media.model';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap/modal-context';

export class GalleryModalContext extends BSModalContext {
  constructor(public medias: Media[], public title = 'Álbum', public description = '') {
    super();
  }
}

@Component({
  selector: 'uc-gallery-modal',
  templateUrl: 'gallery-modal.component.html',
  styleUrls: ['gallery-modal.component.css'],
})
export class GalleryModalComponent implements OnInit, ModalComponent<GalleryModalContext> {
  public medias: Media[];
  public title = 'Álbum';
  public description = '';

  constructor(public dialog: DialogRef<GalleryModalContext>) {
    this.medias = this.dialog.context.medias;
    this.title = this.dialog.context.title || 'Álbum';
    this.description = this.dialog.context.description;
  }

  ngOnInit() {
  }

  close(): void {
    this.dialog.close();
  }

  beforeDismiss(): boolean {
    return false;
  }

  beforeClose(): boolean {
    return false;
  }

}
