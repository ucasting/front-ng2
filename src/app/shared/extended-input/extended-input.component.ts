import {
  Component, OnChanges, Input, AfterContentInit, ContentChild, OnDestroy
} from '@angular/core';
import {FormErrors} from '../../../models/form-errors.interface';
import {FormControlName, AbstractControl} from '@angular/forms';
import {Subscription} from 'rxjs/Rx';

@Component({
  selector: 'uc-extended-input',
  templateUrl: 'extended-input.component.html',
})
export class ExtendedInputComponent implements OnChanges, AfterContentInit, OnDestroy {

  @Input() labelText: string = '';
  @Input() id: string;
  @Input() errorDefs: any = {required: 'Este campo é obrigatório.'};
  @Input() serverErrors: FormErrors;

  @ContentChild(FormControlName) formControlName: FormControlName;

  control: AbstractControl;
  errorMessage: string = '';

  controlSubscription: Subscription;

  ngAfterContentInit(): void {
    this.control = this.formControlName.control;
    this.controlSubscription = this.control.valueChanges.subscribe(
      (values) => {
        this.checkErrors();
      }
    );
  }

  private checkErrors() {
    this.errorMessage = '';

    if (this.control && this.control.dirty) {
      if (this.control.errors) {
        Object.keys(this.errorDefs).some(key => {
          if (this.control.errors[key]) {
            this.errorMessage = this.errorDefs[key];
            return true;
          }
        });
      }
    }
    if (this.serverErrors && this.serverErrors.errors && this.serverErrors.errors.length > 0) {
      this.errorMessage = this.serverErrors.errors[0];
    }

  }

  ngOnChanges(changes: any): void {
    if (changes.serverErrors) {
      this.checkErrors();
    }
  }

  ngOnDestroy(): void {
    if (this.controlSubscription) {
      this.controlSubscription.unsubscribe();
    }
  }
}
