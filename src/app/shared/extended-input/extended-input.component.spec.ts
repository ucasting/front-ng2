// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ExtendedInputComponent } from './extended-input.component';
//
// describe('Component: ExtendedInput', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ExtendedInputComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ExtendedInputComponent],
//       (component: ExtendedInputComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ExtendedInputComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ExtendedInputComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-extended-input></uc-extended-input>
//   `,
//   directives: [ExtendedInputComponent]
// })
// class ExtendedInputComponentTestController {
// }
//
