import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {Project} from '../../../models/project.model';
import {STATES} from '../../../utils/helpers';
import {ProjectsService} from '../../../services/projects.service';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {FormErrors} from '../../../models/form-errors.interface';
import {EntitiesService} from '../../../services/entities.service';
import {JobCategory} from '../../../models/job-category.model';
import {Observable} from 'rxjs';

@Component({
  selector: 'uc-project-form',
  templateUrl: 'project-form.component.html',
})
export class ProjectFormComponent implements OnInit {
  @Input() serverErrors: FormErrors;
  @Output() save: EventEmitter<Project> = new EventEmitter<Project>();
  public states: string[] = STATES;
  public projectForm: FormGroup;
  public locationsArray: FormArray;
  public jobCategories$: Observable<JobCategory>;

  constructor(public projectsService: ProjectsService, public formBuilder: FormBuilder, public entitiesService: EntitiesService) {
    this.locationsArray = formBuilder.array([]);
    this.projectForm = formBuilder.group({
      name: [null, Validators.required],
      clientName: [null, Validators.required],
      description: [null],
      budget: [null, Validators.required],
      jobCategory: [null, Validators.required],
      totalPositions: [null, Validators.required],
    });
    this.jobCategories$ = entitiesService.jobCategoryArray$;
    this.projectForm.addControl('locations', this.locationsArray);
    this.addLocationForm();
  }

  @Input()
  public set value(value: any) {
    const locations = value.locations;
    // reset locations array
    while (this.locationsArray.length > 0) {
      this.locationsArray.removeAt(0);
    }
    // adjusts the length correctly
    while (this.locationsArray.length < locations.length) {
      this.addLocationForm();
    }
    this.projectForm.setValue(value);
    // adds one form if none is set
    if (this.locationsArray.length === 0) {
      this.addLocationForm();
    }
  }

  public addLocationForm() {
    const locationForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      address: ['', Validators.required],
      district: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipcode: ['', Validators.required],
      latitude: [''],
      longitude: ['']
    });
    this.locationsArray.push(locationForm);
  }

  public removeLocationForm(index: number) {
    this.locationsArray.removeAt(index);
  }

  public saveProject() {
    this.save.emit(this.projectForm.value);
  }

  ngOnInit() {

  }

}
