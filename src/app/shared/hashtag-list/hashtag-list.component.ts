import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Hashtag} from '../../../models/hashtag.model';

@Component({
  selector: 'uc-hashtag-list',
  templateUrl: 'hashtag-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HashtagListComponent implements OnInit {

  @Input() hashtags: Hashtag[];

  constructor() {
  }

  ngOnInit() {
  }

}
