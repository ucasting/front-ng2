// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { HashtagListComponent } from './hashtag-list.component';
//
// describe('Component: HashtagList', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [HashtagListComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([HashtagListComponent],
//       (component: HashtagListComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(HashtagListComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(HashtagListComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-hashtag-list></uc-hashtag-list>
//   `,
//   directives: [HashtagListComponent]
// })
// class HashtagListComponentTestController {
// }
//
