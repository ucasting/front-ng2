// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ParticularityListComponent } from './particularity-list.component';
//
// describe('Component: ParticularityList', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ParticularityListComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ParticularityListComponent],
//       (component: ParticularityListComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ParticularityListComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ParticularityListComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-particularity-list></uc-particularity-list>
//   `,
//   directives: [ParticularityListComponent]
// })
// class ParticularityListComponentTestController {
// }
//
