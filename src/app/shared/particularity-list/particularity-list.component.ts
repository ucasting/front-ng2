import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-particularity-list',
  templateUrl: 'particularity-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ParticularityListComponent implements OnInit {

  @Input() particularities;

  constructor() {
  }

  ngOnInit() {
  }

}
