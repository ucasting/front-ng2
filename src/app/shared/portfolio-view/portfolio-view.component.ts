import {Component, OnInit, Input} from '@angular/core';
import {Profile} from '../../../models/profile.model';
import {Gallery} from '../../../models/gallery.model';

@Component({
  selector: 'uc-portfolio-view',
  templateUrl: 'portfolio-view.component.html',
})
export class PortfolioViewComponent implements OnInit {

  @Input() profile: Profile;
  public selectedAlbum: Gallery;


  constructor() {
  }

  ngOnInit() {
  }

  setSelectedAlbum(album: Gallery) {
    this.selectedAlbum = album;
  }

}
