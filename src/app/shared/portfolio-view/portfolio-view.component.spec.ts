// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { PortfolioViewComponent } from './portfolio-view.component';
//
// describe('Component: PortfolioView', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [PortfolioViewComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([PortfolioViewComponent],
//       (component: PortfolioViewComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(PortfolioViewComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(PortfolioViewComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-portfolio-view></uc-portfolio-view>
//   `,
//   directives: [PortfolioViewComponent]
// })
// class PortfolioViewComponentTestController {
// }
//
