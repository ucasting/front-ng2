// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { IdiomItemComponent } from './idiom-item.component';
//
// describe('Component: IdiomItem', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [IdiomItemComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([IdiomItemComponent],
//       (component: IdiomItemComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(IdiomItemComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(IdiomItemComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-idiom-item></uc-idiom-item>
//   `,
//   directives: [IdiomItemComponent]
// })
// class IdiomItemComponentTestController {
// }
//
