import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {ProfileIdiom} from '../../../models/profile-idiom.model';

@Component({
  selector: 'uc-idiom-item',
  templateUrl: 'idiom-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IdiomItemComponent implements OnInit {

  @Input() profileIdiom: ProfileIdiom;

  constructor() {
  }

  ngOnInit() {
  }

}
