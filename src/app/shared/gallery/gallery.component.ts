import {Component, OnInit, Input} from '@angular/core';
import {Media} from '../../../models/media.model';

@Component({
  selector: 'uc-gallery',
  templateUrl: 'gallery.component.html',
  styleUrls: ['gallery.component.css'],
})
export class GalleryComponent implements OnInit {

  @Input() medias: Media[];

  constructor() {
  }

  ngOnInit() {
    console.log(this.medias);
  }

}
