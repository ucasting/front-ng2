// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { CandidateViewComponent } from './candidate-view.component';
//
// describe('Component: CandidateView', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [CandidateViewComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([CandidateViewComponent],
//       (component: CandidateViewComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(CandidateViewComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(CandidateViewComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-candidate-view></uc-candidate-view>
//   `,
//   directives: [CandidateViewComponent]
// })
// class CandidateViewComponentTestController {
// }
//
