import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {User} from '../../../models/user.model';
import {Media} from '../../../models/media.model';

@Component({
  selector: 'uc-candidate-view',
  templateUrl: 'candidate-view.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandidateViewComponent implements OnInit {


  @Input() user: User;

  mainImages: Media[];

  @Input()
  currentTab = 0;

  constructor() {
  }

  ngOnInit() {
  }

  setTab(index: number) {
    this.currentTab = index;
  }

}
