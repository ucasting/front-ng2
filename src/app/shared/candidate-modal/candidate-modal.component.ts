import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user.model';
import {FirstWordPipe} from '../../../pipes/first-word.pipe';
import {MdDialogRef} from '@angular/material';


@Component({
  selector: 'uc-candidate-modal',
  templateUrl: 'candidate-modal.component.html',
  styleUrls: ['candidate-modal.component.css']
})
export class CandidateModalComponent implements OnInit {
  public candidate: User;
  public title: string;
  public tabIndex = 0;

  constructor(private dialog: MdDialogRef<CandidateModalComponent>,
              private firstWordPipe: FirstWordPipe) {

  }

  ngOnInit() {
    this.title = this.firstWordPipe.transform(this.candidate.firstName);
  }

  close(): void {
    this.dialog.close();
  }

}
