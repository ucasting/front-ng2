import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {FileItem} from 'ng2-file-upload';

@Component({
  selector: 'uc-album-image-progress',
  templateUrl: 'album-image-progress.component.html',
  styleUrls: ['album-image-progress.component.css']
})
export class AlbumImageProgressComponent implements OnInit, OnChanges {

  @Input() item: FileItem;

  fileReader: FileReader;

  image: any;

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: any): void {
    if (changes.item.currentValue) {
      this.fileReader = new FileReader();
      this.fileReader.onload = (e: any) => {
        this.image = e.target.result;
      };
      this.fileReader.readAsDataURL(this.item._file);
    }
  }

  remove(): void {
    if (this.item.isUploading) {
      this.item.cancel();
    }
    this.item.remove();
  }

}
