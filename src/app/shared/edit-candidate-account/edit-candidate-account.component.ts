import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-edit-candidate-account',
  templateUrl: 'edit-candidate-account.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCandidateAccountComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
