// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { EditCandidateAccountComponent } from './edit-candidate-account.component';
//
// describe('Component: EditCandidateAccount', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [EditCandidateAccountComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([EditCandidateAccountComponent],
//       (component: EditCandidateAccountComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(EditCandidateAccountComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(EditCandidateAccountComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-edit-candidate-account></uc-edit-candidate-account>
//   `,
//   directives: [EditCandidateAccountComponent]
// })
// class EditCandidateAccountComponentTestController {
// }
//
