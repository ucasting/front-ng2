// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ParticularityItemComponent } from './particularity-item.component';
//
// describe('Component: ParticularityItem', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ParticularityItemComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ParticularityItemComponent],
//       (component: ParticularityItemComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ParticularityItemComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ParticularityItemComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-particularity-item></uc-particularity-item>
//   `,
//   directives: [ParticularityItemComponent]
// })
// class ParticularityItemComponentTestController {
// }
//
