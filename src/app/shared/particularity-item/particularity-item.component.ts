import {Component, OnInit, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-particularity-item',
  templateUrl: 'particularity-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParticularityItemComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
