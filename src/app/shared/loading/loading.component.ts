import {Component, OnInit, Input} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'uc-loading',
  templateUrl: 'loading.component.html',
})
export class LoadingComponent implements OnInit {

  @Input() isLoading: Observable<boolean>;
  @Input() text: string;

  constructor() {
  }

  ngOnInit() {
  }

}
