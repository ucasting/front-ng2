import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';

@Component({
  selector: 'uc-profile-characteristics',
  templateUrl: 'profile-characteristics.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileCharacteristicsComponent implements OnInit {

  @Input() profile;

  constructor() {}

  ngOnInit() {
  }

}
