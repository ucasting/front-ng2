// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ProfileCharacteristicsComponent } from './profile-characteristics.component';
//
// describe('Component: ProfileCharacteristics', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ProfileCharacteristicsComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ProfileCharacteristicsComponent],
//       (component: ProfileCharacteristicsComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ProfileCharacteristicsComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ProfileCharacteristicsComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-profile-characteristics></uc-profile-characteristics>
//   `,
//   directives: [ProfileCharacteristicsComponent]
// })
// class ProfileCharacteristicsComponentTestController {
// }
//
