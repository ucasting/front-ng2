import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Profile} from '../../../models/profile.model';
import {Media} from '../../../models/media.model';
import {Gallery} from '../../../models/gallery.model';

@Component({
  selector: 'uc-profile-view',
  templateUrl: 'profile-view.component.html',
})
export class ProfileViewComponent implements OnInit, OnChanges {

  @Input() profile: Profile;
  public mainImages: Media[];

  constructor() {
  }


  ngOnInit() {
  }

  ngOnChanges(changes: any): any {
    if (changes.profile && changes.profile.currentValue) {
      let albums: Gallery[] = changes.profile.currentValue.albums;
      this.mainImages = [];
      let count = 0;
      albums.forEach((album) => {
        album.medias.forEach((galleryMedia) => {
          if (count < 6) {
            if (galleryMedia.media) {
              this.mainImages.push(galleryMedia.media);
              count++;
            }
          }
        });
      });
    }
  }

}
