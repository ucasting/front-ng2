import {Component, OnDestroy, Input, EventEmitter, Output} from '@angular/core';
import {Subscription} from 'rxjs/Rx';
import {FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import {EntitiesService} from '../../../services/entities.service';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {FormErrors} from '../../../models/form-errors.interface';
import {ProjectLocation} from '../../../models/project-location.model';
import {UserJob} from '../../../models/user-job.model';
import * as moment from 'moment';

@Component({
  selector:    'uc-user-job-form',
  templateUrl: 'user-job-form.component.html',
})
export class UserJobFormComponent implements OnDestroy {

  @Input() locations: ProjectLocation[] = [];
  @Input() jobId: number;
  @Input() serverErrors: FormErrors;
  @Output() save: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() publish: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() cancel: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() formValue: EventEmitter<any> = new EventEmitter<any>();

  public visibleCharacteristics = false;
  public visibleRequirements = false;
  public courseLevels: any[];
  public idioms: any[];
  public proficiencies: any[];
  private subscriptions: Subscription[] = [];
  public jobForm: FormGroup;
  public coursesForm: FormArray;
  public idiomsForm: FormArray;
  public _userJob: UserJob;

  public benefits: any[] = [];
  public eyeColors: any[] = [];
  public skins: any[] = [];
  public particularities: any[] = [];
  public physicalTraits: any[] = [];
  public hairColors: any[] = [];
  public hairLengths: any[] = [];
  public hairTypes: any[] = [];

  public formDisabled = false;


  constructor(public modal: Modal,
              public entitiesService: EntitiesService,
              private formBuilder: FormBuilder) {
    this.jobForm = this.formBuilder.group({
      name:                 ['', Validators.required],
      description:          ['', Validators.required],
      observations:         [''],
      location:             [null, Validators.required],
      start:                [null, Validators.required],
      end:                  [null, Validators.required],
      totalPositions:       [1, Validators.required],
      journeyStartTime:     [null, Validators.required],
      journeyEndTime:       [null, Validators.required],
      genders:              [[]],
      jobType:              [null],
      remuneration:         [null, Validators.required],
      remunerationType:     [null, Validators.required],
      daysToPayment:        [null, Validators.required],
      minAge:               [18],
      maxAge:               [null],
      hairAllowCut:         [null],
      hairAllowChangeColor: [null],
      hairAllowStraighten:  [null],
      minHeight:            [null],
      maxHeight:            [null],
      minWeight:            [null],
      maxWeight:            [null],
      minBust:              [null],
      maxBust:              [null],
      minWaist:             [null],
      maxWaist:             [null],
      minHip:               [null],
      maxHip:               [null],
      minThorax:            [null],
      maxThorax:            [null],
      minShoulder:          [null],
      maxShoulder:          [null],
      minManequim:          [null],
      maxManequim:          [null],
      minShoesSize:         [null],
      maxShoesSize:         [null],
      eyeColors:            [[]],
      skins:                [[]],
      hairLengths:          [[]],
      hairColors:           [[]],
      hairTypes:            [[]],
      physicalTraits:       [[]],
      particularities:      [[]],
      benefits:             [[]],
    });

    this.idiomsForm = new FormArray([]);
    this.coursesForm = new FormArray([]);
    this.jobForm.addControl('idioms', this.idiomsForm);
    this.jobForm.addControl('courses', this.coursesForm);

    this.subscriptions.push(
      this.entitiesService.courseLevelArray$.subscribe((courseLevels) => {
        this.courseLevels = courseLevels;
      }),
      this.entitiesService.proficiencyArray$.subscribe((proficiencies) => {
        this.proficiencies = proficiencies;
      }),
      this.entitiesService.idiomArray$.subscribe((idioms) => {
        this.idioms = idioms;
      }),
      this.jobForm.valueChanges
        .distinctUntilChanged()
        .subscribe((value) => {
          this.formValue.emit(value);
        }),
    );
  }

  get userJob(): UserJob {
    return this._userJob;
  }

  @Input() set userJob(userJob: UserJob) {
    this._userJob = userJob;
    this.formDisabled = !!(userJob && (userJob.published || userJob.cancelled));
    // reset idioms
    while (this.idiomsForm.length > 0) {
      this.idiomsForm.removeAt(0);
    }
    // reset courses
    while (this.coursesForm.length > 0) {
      this.coursesForm.removeAt(0);
    }
    if (userJob) {
      const idioms = userJob.idioms;
      // adjusts the length correctly
      while (this.idiomsForm.length < idioms.length) {
        this.addIdiomForm();
      }

      const courses = userJob.courses;
      while (this.coursesForm.length < courses.length) {
        this.addCourseForm();
      }

      // normalizing data
      const value: any = Object.assign({}, userJob);
      const start: any = moment(userJob.start).utc();
      const end: any = moment(userJob.end).utc();
      const journeyStart: any = moment(userJob.journeyStartTime).utc();
      const journeyEnd: any = moment(userJob.journeyEndTime).utc();
      value.start = start.format('DD/MM/YYYY');
      value.end = end.format('DD/MM/YYYY');
      value.journeyStartTime = journeyStart.format('HH:mm');
      value.journeyEndTime = journeyEnd.format('HH:mm');
      value.courses = this.entitiesService.getEntities('userJobCourses', userJob.courses);
      value.idioms = this.entitiesService.getEntities('userJobIdioms', userJob.idioms);
      this.jobForm.patchValue(value);
    } else {
      this.jobForm.reset();
      this.jobForm.controls['minAge'].setValue(18);
      this.jobForm.controls['totalPositions'].setValue(1);
      this.jobForm.controls['genders'].setValue([]);
      this.jobForm.controls['benefits'].setValue([]);
      this.jobForm.controls['skins'].setValue([]);
      this.jobForm.controls['eyeColors'].setValue([]);
      this.jobForm.controls['physicalTraits'].setValue([]);
      this.jobForm.controls['particularities'].setValue([]);
      this.jobForm.controls['hairColors'].setValue([]);
      this.jobForm.controls['hairLengths'].setValue([]);
      this.jobForm.controls['hairTypes'].setValue([]);
    }

    this.formDisabled ? this.jobForm.disable() : this.jobForm.enable();
  }

  public refreshValue(value: any): void {

  }

  public addCharacteristics() {
    this.visibleCharacteristics = true;
  }

  public removeCharacteristics() {
    this.visibleCharacteristics = false;
  }

  public addRequirements() {
    this.visibleRequirements = true;
  }

  public removeRequirements() {
    this.visibleRequirements = false;
  }

  public saveJob() {
    this.save.emit(this.jobForm.value);
  }

  public publishJob() {
    this.publish.emit(this.userJob);
  }

  public cancelJob() {
    this.cancel.emit(this.userJob);
  }

  public selected(value: any, controlName: string): void {
    const control: FormControl = (this.jobForm.controls[controlName] as FormControl);
    control.setValue(control.value.concat(value.id), {emitEvent: true});
    // this.filterForm.controls['interests'].patchValueAndValidity({emitEvent: true});
  }

  public removed(value: any, controlName: string): void {
    const control: FormControl = (this.jobForm.controls[controlName] as FormControl);
    control.setValue((control.value as number[]).filter(id => id !== value.id));
  }

  addIdiomForm() {
    const idiomForm = this.formBuilder.group({
      idiom:       ['', Validators.required],
      proficiency: ['', Validators.required],
    });
    this.idiomsForm.push(idiomForm);
  }

  removeIdiomForm(index: number) {
    this.idiomsForm.removeAt(index);
  }

  addCourseForm() {
    const courseForm = this.formBuilder.group({
      name:  ['', Validators.required],
      level: ['', Validators.required],
    });
    this.coursesForm.push(courseForm);
  }

  removeCourseForm(index: number) {
    this.coursesForm.removeAt(index);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

}
