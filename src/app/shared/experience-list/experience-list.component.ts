import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Experience} from '../../../models/experience.model';

@Component({
  selector: 'uc-experience-list',
  templateUrl: 'experience-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExperienceListComponent implements OnInit {

  @Input() experiences: Experience;

  constructor() {
  }

  ngOnInit() {
  }

}
