import {NgModule} from '@angular/core';
import {UcastingAppComponent} from './ucasting-app.component';
import {BrowserModule} from '@angular/platform-browser';
import {FileUploadModule} from 'ng2-file-upload';
import * as main from './+main';
import {SecurityComponent} from './+security/security.component';
import {LoginComponent} from './+security/+login/login.component';
import {LogoutComponent} from './+security/+logout/logout.component';
import {NotAuthorizedComponent} from './+security/not-authorized/not-authorized.component';
import {NotFoundComponent} from './+not-found/not-found.component';
import * as shared from './shared';
import {rootReducer} from '../reducers';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {RouterModule} from '@angular/router';
import {AuthEffects} from '../effects/auth-effects';
import {OptionsEffects} from '../effects/options-effects';
import {UsersEffects} from '../effects/users-effects';
import {AccountEffects} from '../effects/account-effects';
import {ProfileEffects} from '../effects/profile-effects';
import {UserJobsEffects} from '../effects/user-jobs-effects';
import {ProjectsEffects} from '../effects/projects-effects';
import {SharedModule} from './shared.module';
import {RouterStoreModule} from '@ngrx/router-store';
import {CoreModule} from './core.module';
import {ModalModule} from 'angular2-modal';
import {SearchUserJobsEffects} from './+search-user-jobs/search-user-jobs.effects';
import {UserJobsSelectionEffects} from './+user-jobs-selection/user-jobs-selection.effects';
import {appRoutes} from '../routes/routes';
import {WindowObject} from '../utils/window.token';
@NgModule({
  declarations: [
    // DevtoolsComponent,
    // FILE_UPLOAD_DIRECTIVES,
    UcastingAppComponent,
    // pages
    main.MainComponent,
    main.CandidateComponent,
    main.EditAccountComponent,
    main.EditPortfolioComponent,
    main.ViewMyProfileComponent,
    main.CompanyComponent,
    main.ProjectsComponent,
    main.EditProjectComponent,
    main.ProjectViewComponent,
    main.SearchCandidatesComponent,
    main.ProjectListComponent,
    SecurityComponent,
    LoginComponent,
    LogoutComponent,
    NotAuthorizedComponent,
    NotFoundComponent,
    // shared components
  ],
  imports: [
    CoreModule,
    BrowserModule,
    SharedModule,
    FileUploadModule,
    StoreModule.provideStore(rootReducer),
    RouterStoreModule.connectRouter(),
    RouterModule.forRoot(appRoutes),
    EffectsModule.run(AuthEffects),
    EffectsModule.run(OptionsEffects),
    EffectsModule.run(UsersEffects),
    EffectsModule.run(AccountEffects),
    EffectsModule.run(ProfileEffects),
    EffectsModule.run(UserJobsEffects),
    EffectsModule.run(ProjectsEffects),
    EffectsModule.run(SearchUserJobsEffects),
    EffectsModule.run(UserJobsSelectionEffects),
    ModalModule.forRoot([
      shared.CandidateModalComponent,
      shared.GalleryModalComponent,
      shared.InviteCandidateModalComponent,
    ]),
    // feature modules
  ],
  providers: [
    // {provide: APP_BASE_HREF, useValue: '/'},
    {provide: WindowObject, useValue: window},
  ],
  bootstrap: [UcastingAppComponent],
})
export class UcastingAppModule {
}
