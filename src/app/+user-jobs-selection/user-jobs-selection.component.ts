import {Component, OnInit, ViewChild} from '@angular/core';
import {AppState} from '../../reducers/index';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {UserJobsSelectionState} from './user-jobs-selection.reducer';
import {EntitiesState} from '../../reducers/entities.reducer';
import {HasSubscriptions} from '../../utils/has-subscriptions.class';
import {AccountService} from '../../services/account.service';
import {EntitiesService} from '../../services/entities.service';
import {PageService} from '../../services/page.service';
import {
  LoadProjectsAction,
  SelectProjectAction,
  SelectUserJobAction,
  ChangeTabAction,
  LoadCandidatesAction,
  SelectCandidateAction,
  ApproveCandidateAction,
  RejectCandidateAction
} from './user-jobs-selection.actions';
import {Project} from '../../models/project.model';
import {UserJob} from '../../models/user-job.model';
import {UserSelection} from '../../models/user-selection.model';
import {AngularMasonry} from 'angular2-masonry/src/masonry';
import {user} from '../../models/schemas';
import {Schema} from 'normalizr';

@Component({
  selector:    'uc-user-jobs-selection',
  templateUrl: 'user-jobs-selection.component.html'
})
export class UserJobsSelectionComponent extends HasSubscriptions implements OnInit {

  private state$: Observable<UserJobsSelectionState>;
  private entitiesState$: Observable<EntitiesState>;

  public state: UserJobsSelectionState;

  public currentTab = 2;
  public currentSelections: Array<UserSelection> = [];
  public appliedCount = 0;
  public invitedCount = 0;
  public rejectedCount = 0;
  public selectedCount = 0;
  public approvedCount = 0;

  public userSchema: Schema;

  public userId;
  public loadingProjects: boolean;
  public projects: Array<Project>;
  public userJobs: Array<UserJob>;
  public selectedUserJob: UserJob;

  @ViewChild(AngularMasonry) masonry: AngularMasonry;

  constructor(private store: Store<AppState>,
              private accountService: AccountService,
              private entitiesService: EntitiesService,
              private pageService: PageService) {
    super();
    this.userSchema = user;
    this.state$ = this.store.select((state: AppState) => state.userJobsSelection);
    this.entitiesState$ = this.store.select((state: AppState) => state.entities);
    this.accountService.getUserId().take(1).subscribe(id => this.userId = id);
    this.addSubscription(
      this.state$.subscribe((state: UserJobsSelectionState) => {
        this.state = state;
        this.currentTab = state.currentTab;

        this.loadingProjects = state.loadingProjects;
        this.projects = this.entitiesService.getEntities('projects', state.projects);
        if (state.selectedProjectId) {
          const selectedProject: Project = this.projects.find(project => project.id === state.selectedProjectId);
          this.userJobs = this.entitiesService.getEntities('userJobs', selectedProject.userJobs as number[]);
          this.userJobs = this.userJobs.filter(userJob => userJob.approved);
        } else {
          this.userJobs = [];
        }

        if (state.selectedUserJobId) {
          this.selectedUserJob = this.userJobs.find(userJob => userJob.id === state.selectedUserJobId);
          const selections: Array<UserSelection> = this.entitiesService.getEntities(
            'userSelections', this.selectedUserJob.selections as number[]);
          this.invitedCount = 0;
          this.appliedCount = 0;
          this.rejectedCount = 0;
          this.selectedCount = 0;
          this.approvedCount = 0;
          selections.forEach(selection => {
            switch (selection.status) {
              case 1:
                this.invitedCount++;
                break;
              case 2:
                this.appliedCount++;
                break;
              case 3:
                this.rejectedCount++;
                break;
              case 4:
                this.selectedCount++;
                break;
              case 5:
                this.approvedCount++;
                break;
            }
          });
          this.currentSelections = selections.filter(selection => selection.status === this.currentTab);
          if (this.masonry) {
            this.masonry.layout();
          }
        } else {
          this.selectedUserJob = null;
          this.currentSelections = [];
        }
      })
    );
  }

  projectChanged(event) {
    const projectId = event.target.value !== '' ? parseInt(event.target.value, 0) : null;
    this.store.dispatch(new SelectProjectAction(projectId));
  }

  private loadCandidatesFromSelection() {
    if (this.currentSelections.length > 0) {
      this.currentSelections.forEach(selection => {
        this.store.dispatch(new LoadCandidatesAction([selection.user as number]));
      });
      // this.store.dispatch(new LoadCandidatesAction(this.currentSelections.map(selection => selection.user as number)));
    }
  }

  userJobChanged(event) {
    const jobId = event.target.value !== '' ? parseInt(event.target.value, 0) : null;
    this.store.dispatch(new SelectUserJobAction(jobId));
    this.loadCandidatesFromSelection();
  }

  changeTab(index: number) {
    this.store.dispatch(new ChangeTabAction(index));
    this.loadCandidatesFromSelection();
  }

  getSelectionId(index: number, selection: UserSelection) {
    return selection.id;
  }

  ngOnInit() {
    this.pageService.changeTitle('Seleções');
    // if (!this.state.projectsLoaded) {
    this.store.dispatch(new LoadProjectsAction());
    this.loadCandidatesFromSelection();
  }

  selectUser(selection: UserSelection) {
    this.store.dispatch(new SelectCandidateAction(
      {jobId: this.selectedUserJob.id as number, selectionId: selection.id as number}));
  }

  approveUser(selection: UserSelection) {
    this.store.dispatch(new ApproveCandidateAction(
      {jobId: this.selectedUserJob.id as number, selectionId: selection.id as number}));
  }

  rejectUser(selection: UserSelection) {
    this.store.dispatch(new RejectCandidateAction(
      {jobId: this.selectedUserJob.id as number, selectionId: selection.id as number}));
  }

}
