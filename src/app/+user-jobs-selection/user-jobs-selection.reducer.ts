import {UserJobsSelectionActions, UserJobsSelectionActionTypes} from './user-jobs-selection.actions';

export interface UserJobsSelectionState {
  loadingProjects: boolean;
  loadingCandidates: boolean;
  projectsLoaded: boolean;
  projects: Array<number>;
  selectedProjectId: number;
  selectedUserJobId: number;
  currentTab: number;
  rejectingCandidates: number[];
  selectingCandidates: number[];
  approvingCandidates: number[];
}

export const initialUserJobsSelectionState: UserJobsSelectionState = {
  loadingProjects:     false,
  loadingCandidates:   false,
  projectsLoaded:      false,
  projects:            [],
  selectedProjectId:   null,
  selectedUserJobId:   null,
  currentTab:          2,
  rejectingCandidates: [],
  selectingCandidates: [],
  approvingCandidates: [],
};

export function userJobsSelectionReducer(state: UserJobsSelectionState = initialUserJobsSelectionState,
                                         action: UserJobsSelectionActions): UserJobsSelectionState {
  switch (action.type) {
    case UserJobsSelectionActionTypes.LOAD_PROJECTS: {
      return Object.assign({}, state, {loadingProjects: true});
    }
    case UserJobsSelectionActionTypes.LOAD_PROJECTS_SUCCESS: {
      return Object.assign({}, state,
        {
          loadingProjects: false,
          projectsLoaded:  true,
          projects:        action.payload.result
        }
      );
    }
    case UserJobsSelectionActionTypes.LOAD_PROJECTS_FAIL: {
      return Object.assign({}, state, {loadingProjects: false});
    }
    case UserJobsSelectionActionTypes.LOAD_CANDIDATES: {
      return Object.assign({}, state, {loadingCandidates: true});
    }
    case UserJobsSelectionActionTypes.LOAD_CANDIDATES_SUCCESS: {
      return Object.assign({}, state, {loadingCandidates: false}
      );
    }
    case UserJobsSelectionActionTypes.LOAD_CANDIDATES_FAIL: {
      return Object.assign({}, state, {loadingCandidates: false});
    }
    case UserJobsSelectionActionTypes.SELECT_PROJECT: {
      return Object.assign({}, state, {
        selectedProjectId: action.payload,
        selectedUserJobId: null
      });
    }
    case UserJobsSelectionActionTypes.SELECT_USER_JOB: {
      return Object.assign({}, state, {selectedUserJobId: action.payload});
    }

    case UserJobsSelectionActionTypes.CHANGE_TAB: {
      return Object.assign({}, state, {currentTab: action.payload});
    }
    case UserJobsSelectionActionTypes.SELECT_CANDIDATE: {
      return Object.assign({}, state, {selectingCandidates: state.selectingCandidates.concat(action.payload.selectionId)});
    }
    case UserJobsSelectionActionTypes.APPROVE_CANDIDATE: {
      return Object.assign({}, state, {approvingCandidates: state.approvingCandidates.concat(action.payload.selectionId)});
    }
    case UserJobsSelectionActionTypes.REJECT_CANDIDATE: {
      return Object.assign({}, state, {rejectingCandidates: state.rejectingCandidates.concat(action.payload.selectionId)});
    }
    case UserJobsSelectionActionTypes.SELECT_CANDIDATE_SUCCESS: {
      return Object.assign({}, state, {selectingCandidates: state.selectingCandidates.filter(id => id !== action.payload.result)});
    }
    case UserJobsSelectionActionTypes.SELECT_CANDIDATE_FAIL: {
      return Object.assign({}, state,
        {selectingCandidates: state.selectingCandidates.filter(id => id !== action.payload.selectionId)});
    }
    case UserJobsSelectionActionTypes.APPROVE_CANDIDATE_SUCCESS: {
      return Object.assign({}, state, {approvingCandidates: state.approvingCandidates.filter(id => id !== action.payload.result)});
    }
    case UserJobsSelectionActionTypes.APPROVE_CANDIDATE_FAIL: {
      return Object.assign({}, state,
        {approvingCandidates: state.approvingCandidates.filter(id => id !== action.payload.selectionId)});
    }
    case UserJobsSelectionActionTypes.REJECT_CANDIDATE_SUCCESS: {
      return Object.assign({}, state,
        {
          rejectingCandidates: state.rejectingCandidates
                                 .filter(id => id !== action.payload.result)
        });
    }
    case UserJobsSelectionActionTypes.REJECT_CANDIDATE_FAIL: {
      return Object.assign({}, state,
        {
          rejectingCandidates: state.rejectingCandidates
                                 .filter(id => id !== action.payload.selectionId)
        });
    }
    default:
      return state;
  }
}
