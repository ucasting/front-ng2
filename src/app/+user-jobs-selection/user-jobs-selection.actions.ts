import {Action} from '@ngrx/store';
import {NormalizedResponse} from '../../models/schemas';
import {ApiError} from '../../models/api-error.interface';
import {type} from '../../utils/redux-helpers';

export class UserJobsSelectionActionTypes {
  static readonly LOAD_PROJECTS = type('[UserJobsSelection] Load projects');
  static readonly LOAD_PROJECTS_SUCCESS = type('[UserJobsSelection] Load projects success');
  static readonly LOAD_PROJECTS_FAIL = type('[UserJobsSelection] Load projects fail');
  static readonly LOAD_SELECTIONS = type('[UserJobsSelection] Load selections');
  static readonly LOAD_SELECTIONS_SUCCESS = type('[UserJobsSelection] Load selections success');
  static readonly LOAD_SELECTIONS_FAIL = type('[UserJobsSelection] Load selections fail');
  static readonly SELECT_PROJECT = type('[UserJobsSelection] Select project');
  static readonly SELECT_USER_JOB = type('[UserJobsSelection] Select user job');
  static readonly CHANGE_TAB = type('[UserJobsSelection] Change Tab');
  static readonly LOAD_CANDIDATES = type('[UserJobsSelection] Load Candidates');
  static readonly LOAD_CANDIDATES_SUCCESS = type('[UserJobsSelection] Load Candidates success');
  static readonly LOAD_CANDIDATES_FAIL = type('[UserJobsSelection] Load Candidates fail');
  static readonly SELECT_CANDIDATE = type('[UserJobsSelection] Select candidate');
  static readonly REJECT_CANDIDATE = type('[UserJobsSelection] Reject candidate');
  static readonly APPROVE_CANDIDATE = type('[UserJobsSelection] Approve candidate');
  static readonly SELECT_CANDIDATE_SUCCESS = type('[UserJobsSelection] Select candidate success');
  static readonly REJECT_CANDIDATE_SUCCESS = type('[UserJobsSelection] Reject candidate success');
  static readonly APPROVE_CANDIDATE_SUCCESS = type('[UserJobsSelection] Approve candidate success');
  static readonly SELECT_CANDIDATE_FAIL = type('[UserJobsSelection] Select candidate fail');
  static readonly REJECT_CANDIDATE_FAIL = type('[UserJobsSelection] Reject candidate fail');
  static readonly APPROVE_CANDIDATE_FAIL = type('[UserJobsSelection] Approve candidate fail');
}

export class LoadProjectsAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_PROJECTS;

  constructor(public payload: any = null) {}
}

export class LoadProjectsSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_PROJECTS_SUCCESS;

  constructor(public payload: NormalizedResponse) {}
}
export class LoadProjectsFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_PROJECTS_FAIL;

  constructor(public payload: ApiError) {}
}

export class LoadSelectionsAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_SELECTIONS;

  constructor(public payload: number) {}
}

export class LoadSelectionsSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_SELECTIONS_SUCCESS;

  constructor(public payload: NormalizedResponse) {}
}

export class LoadSelectionsFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_SELECTIONS_FAIL;

  constructor(public payload: { error: ApiError, jobId: number }) {}
}

export class SelectProjectAction implements Action {
  readonly type = UserJobsSelectionActionTypes.SELECT_PROJECT;

  constructor(public payload: number) {}
}

export class SelectUserJobAction implements Action {
  readonly type = UserJobsSelectionActionTypes.SELECT_USER_JOB;

  constructor(public payload: number) {};
}
export class ChangeTabAction implements Action {
  readonly type = UserJobsSelectionActionTypes.CHANGE_TAB;

  constructor(public payload: number) {};
}

export class LoadCandidatesAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_CANDIDATES;

  constructor(public payload: number[]) {};
}

export class LoadCandidatesSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_CANDIDATES_SUCCESS;

  constructor(public payload: NormalizedResponse) {};
}

export class LoadCandidatesFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.LOAD_CANDIDATES_FAIL;

  constructor(public payload: ApiError) {};
}

export class SelectCandidateAction implements Action {
  readonly type = UserJobsSelectionActionTypes.SELECT_CANDIDATE;

  constructor(public payload: { jobId: number, selectionId: number }) {};
}

export class SelectCandidateSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.SELECT_CANDIDATE_SUCCESS;

  constructor(public payload: NormalizedResponse) {};
}

export class SelectCandidateFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.SELECT_CANDIDATE_FAIL;

  constructor(public payload: { error: ApiError, selectionId: number }) {};
}

export class ApproveCandidateAction implements Action {
  readonly type = UserJobsSelectionActionTypes.APPROVE_CANDIDATE;

  constructor(public payload: { jobId: number, selectionId: number }) {};
}

export class ApproveCandidateSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.APPROVE_CANDIDATE_SUCCESS;

  constructor(public payload: NormalizedResponse) {};
}

export class ApproveCandidateFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.APPROVE_CANDIDATE_FAIL;

  constructor(public payload: { error: ApiError, selectionId: number }) {};
}

export class RejectCandidateAction implements Action {
  readonly type = UserJobsSelectionActionTypes.REJECT_CANDIDATE;

  constructor(public payload: { jobId: number, selectionId: number }) {};
}

export class RejectCandidateSuccessAction implements Action {
  readonly type = UserJobsSelectionActionTypes.REJECT_CANDIDATE_SUCCESS;

  constructor(public payload: NormalizedResponse) {};
}

export class RejectCandidateFailAction implements Action {
  readonly type = UserJobsSelectionActionTypes.REJECT_CANDIDATE_FAIL;

  constructor(public payload: { error: ApiError, selectionId: number }) {};
}


export type UserJobsSelectionActions =
  LoadProjectsAction
    | LoadProjectsSuccessAction
    | LoadProjectsFailAction
    | LoadSelectionsAction
    | LoadSelectionsSuccessAction
    | LoadSelectionsFailAction
    | SelectProjectAction
    | SelectUserJobAction
    | ChangeTabAction
    | LoadCandidatesAction
    | LoadCandidatesSuccessAction
    | LoadCandidatesFailAction
    | SelectCandidateAction
    | SelectCandidateSuccessAction
    | SelectCandidateFailAction
    | ApproveCandidateAction
    | ApproveCandidateSuccessAction
    | ApproveCandidateFailAction
    | RejectCandidateAction
    | RejectCandidateSuccessAction
    | RejectCandidateFailAction;
