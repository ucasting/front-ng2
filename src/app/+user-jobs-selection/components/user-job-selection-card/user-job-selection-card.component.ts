import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {UserSelection} from '../../../../models/user-selection.model';
import {user} from '../../../../models/schemas';
import {Schema} from 'normalizr';
import {
  SELECTION_STATUS_INVITED,
  SELECTION_STATUS_APPLIED,
  SELECTION_STATUS_SELECTED,
  SELECTION_STATUS_APPROVED,
  SELECTION_STATUS_REJECTED
} from '../../../../models/selection-status.constants';
import {User} from '../../../../models/user.model';

@Component({
  selector:    'uc-user-job-selection-card',
  templateUrl: 'user-job-selection-card.component.html'
})
export class UserJobSelectionCardComponent implements OnInit {
  @Input() selection: UserSelection;

  @Output() selectUser: EventEmitter<UserSelection> = new EventEmitter<UserSelection>();
  @Output() rejectUser: EventEmitter<UserSelection> = new EventEmitter<UserSelection>();
  @Output() approveUser: EventEmitter<UserSelection> = new EventEmitter<UserSelection>();

  public userSchema: Schema = user;
  public showApprove: boolean;
  public showSelect: boolean;
  public showReject: boolean;

  constructor() { }

  ngOnInit() {
    switch (this.selection.status) {
      case SELECTION_STATUS_INVITED: {
        this.showReject = false;
        this.showSelect = false;
        this.showApprove = false;
        break;
      }
      case SELECTION_STATUS_APPLIED: {
        this.showReject = true;
        this.showSelect = true;
        this.showApprove = false;
        break;
      }
      case SELECTION_STATUS_SELECTED: {
        this.showReject = true;
        this.showSelect = false;
        this.showApprove = true;
        break;
      }
      case SELECTION_STATUS_APPROVED: {
        this.showReject = true;
        this.showSelect = false;
        this.showApprove = false;
        break;
      }
      case SELECTION_STATUS_REJECTED: {
        this.showReject = false;
        this.showSelect = true;
        this.showApprove = true;
        break;
      }
    }

  }

  select(user: User) {
    this.selectUser.emit(this.selection);
  }

  approve(user: User) {
    this.approveUser.emit(this.selection);
  }

  reject(user: User) {
    this.rejectUser.emit(this.selection);
  }

}
