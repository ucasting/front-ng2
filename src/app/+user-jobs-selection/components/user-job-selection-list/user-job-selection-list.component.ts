import {Component, OnInit, Input} from '@angular/core';
import {UserSelection} from '../../../../models/user-selection.model';

@Component({
  selector:    'uc-user-job-selection-list',
  templateUrl: 'user-job-selection-list.component.html'
})
export class UserJobSelectionListComponent implements OnInit {
  @Input() selections: Array<UserSelection>;

  constructor() { }

  ngOnInit() { }

  getSelectionId(index, selection: UserSelection) {
    return selection.id;
  }

}
