import {NgModule} from '@angular/core';
import {UserJobsSelectionRoutes} from './user-jobs-selection-routes';
import {CommonModule} from '@angular/common';
import {UserJobsSelectionComponent} from './user-jobs-selection.component';
import {SharedModule} from '../shared.module';
import {UserJobSelectionListComponent} from './components/user-job-selection-list/user-job-selection-list.component';
import {UserJobSelectionCardComponent} from './components/user-job-selection-card/user-job-selection-card.component';

@NgModule({
  declarations: [
    UserJobsSelectionComponent,
    UserJobSelectionListComponent,
    UserJobSelectionCardComponent
  ],
  imports: [
    CommonModule,
    UserJobsSelectionRoutes,
    SharedModule,
    // EffectsModule.run(UserJobsSelectionEffects),
  ]
})
export class UserJobsSelectionModule {
}
