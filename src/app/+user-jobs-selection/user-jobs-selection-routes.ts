import {RouterModule, Routes} from '@angular/router';
import {UserJobsSelectionComponent} from './user-jobs-selection.component';

export const userJobsSelectionRoutes: Routes = [
  {
    path: '',
    component: UserJobsSelectionComponent,
    pathMatch: 'full'
  },
  {

  }
];


export const UserJobsSelectionRoutes = RouterModule.forChild(userJobsSelectionRoutes);
