import {UserJobsApiService} from '../../services/user-jobs-api.service';
import {Actions, Effect} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../reducers/index';
import {
  UserJobsSelectionActionTypes,
  LoadProjectsSuccessAction,
  LoadProjectsFailAction,
  LoadCandidatesFailAction,
  LoadCandidatesSuccessAction,
  SelectCandidateAction,
  SelectCandidateSuccessAction,
  SelectCandidateFailAction,
  ApproveCandidateAction,
  ApproveCandidateSuccessAction,
  RejectCandidateAction,
  RejectCandidateSuccessAction,
  RejectCandidateFailAction,
  ApproveCandidateFailAction, LoadCandidatesAction
} from './user-jobs-selection.actions';
import {ProjectsApiService} from '../../services/projects-api.service';
import {project, user, userSelection} from '../../models/schemas';
import {arrayOf, normalize} from 'normalizr';
import {Observable} from 'rxjs/Observable';
import {UsersApiService, CANDIDATE_TYPE} from '../../services/users-api.service';
import {User} from '../../models/user.model';

@Injectable()
export class UserJobsSelectionEffects {

  @Effect() loadProjects$ = this.actions$
    .ofType(UserJobsSelectionActionTypes.LOAD_PROJECTS)
    .switchMap(action => {
      return this.projectsApi.loadMyCompanyProjects()
        .map((res) => {
          const normalized = normalize(res.json(), arrayOf(project));
          return new LoadProjectsSuccessAction(normalized);
        })
        .catch(error => {
          return Observable.of(new LoadProjectsFailAction(error.json()));
        });
    })
  ;

  @Effect() loadCandidates$ = this.actions$
    .ofType(UserJobsSelectionActionTypes.LOAD_CANDIDATES)
    // .filter(action => false)
    .mergeMap((action: LoadCandidatesAction) => {
        return this.usersApi.searchUsers({ids: action.payload, type: CANDIDATE_TYPE, paginated: false})
          .map((res) => {
            const users: User[] = res.json();
            const normalized = normalize(users, arrayOf(user));
            return new LoadCandidatesSuccessAction(normalized);
          })
          .catch(res => Observable.of(new LoadCandidatesFailAction(res.json())));
      }
      //   // TODO fix this incompatibility with typescript
      //   return Observable.of(new LoadCandidatesSuccessAction({result: []}));
      // }
    );

  @Effect() selectCandidate$ = this.actions$
    .ofType(UserJobsSelectionActionTypes.SELECT_CANDIDATE)
    .mergeMap((action: SelectCandidateAction) => {
      return this.userJobsApi.selectCandidate(action.payload.jobId, action.payload.selectionId)
        .map(res => {
          const normalized = normalize(res.json(), userSelection);
          return new SelectCandidateSuccessAction(normalized);
        })
        .catch((res) => Observable.of(new SelectCandidateFailAction({
          error:       res.json(),
          selectionId: action.payload.selectionId
        })));
    });

  @Effect() approveCandidate$ = this.actions$
    .ofType(UserJobsSelectionActionTypes.APPROVE_CANDIDATE)
    .mergeMap((action: ApproveCandidateAction) => {
      return this.userJobsApi.approveCandidate(action.payload.jobId, action.payload.selectionId)
        .map(res => {
          const normalized = normalize(res.json(), userSelection);
          return new ApproveCandidateSuccessAction(normalized);
        })
        .catch((res) => Observable.of(new ApproveCandidateFailAction({
          error:       res.json(),
          selectionId: action.payload.selectionId
        })));
    });

  @Effect() rejectCandidate$ = this.actions$
    .ofType(UserJobsSelectionActionTypes.REJECT_CANDIDATE)
    .mergeMap((action: RejectCandidateAction) => {
      return this.userJobsApi.rejectCandidate(action.payload.jobId, action.payload.selectionId)
        .map(res => {
          const normalized = normalize(res.json(), userSelection);
          return new RejectCandidateSuccessAction(normalized);
        })
        .catch((res) => Observable.of(new RejectCandidateFailAction({
          error:       res.json(),
          selectionId: action.payload.selectionId
        })));
    });

  constructor(private projectsApi: ProjectsApiService,
              private userJobsApi: UserJobsApiService,
              private usersApi: UsersApiService,
              private actions$: Actions,
              private store$: Store<AppState>) {
  }
}
