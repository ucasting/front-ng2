import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {User} from '../../models/user.model';
import {AccountService} from '../../services/account.service';
import {PageService} from '../../services/page.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector:    'uc-main',
  templateUrl: 'main.component.html',
})
export class MainComponent implements OnInit {
  public userId$: Observable<string>;
  public user$: Observable<User>;

  protected toggledNavigation: boolean = false;


  constructor(private accountService: AccountService,
              public pageService: PageService,
              private authService: AuthenticationService) {
    this.userId$ = this.accountService.getUserId();
    this.user$ = this.accountService.getUser();
  }

  ngOnInit() {

  }

  public toggleNavigation() {
    this.pageService.toggleNavigation();
  }

  public logout() {
    this.authService.logout();
  }

}
