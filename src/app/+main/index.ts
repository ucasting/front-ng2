export {MainComponent} from './main.component';
export {
  CandidateComponent,
  EditPortfolioComponent,
  ViewMyProfileComponent,
  EditAccountComponent
} from './+candidate';
export {
  CompanyComponent,
  ProjectsComponent,
  ProjectListComponent,
  ProjectViewComponent,
  SearchCandidatesComponent,
  EditProjectComponent
} from './+company';
