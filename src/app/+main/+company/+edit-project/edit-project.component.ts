import {Component} from '@angular/core';
import {ProjectsService} from '../../../../services/projects.service';
import {HasSubscriptions} from '../../../../utils/has-subscriptions.class';
import {MyCompanyProjectsState} from '../../../../reducers/my-company-projects.reducer';
import {Router, ActivatedRoute} from '@angular/router';
import {Project} from '../../../../models/project.model';
import {EntitiesService} from '../../../../services/entities.service';
import {PageService} from '../../../../services/page.service';
import {FormErrors} from '../../../../models/form-errors.interface';

@Component({
  selector: 'uc-edit-project',
  templateUrl: 'edit-project.component.html',
})
export class EditProjectComponent extends HasSubscriptions {
  private state: MyCompanyProjectsState;
  private projectId: string;
  public currentProject: any;
  public serverErrors: FormErrors;

  constructor(public projectsService: ProjectsService,
              public router: Router,
              public route: ActivatedRoute,
              public entitiesService: EntitiesService,
              public pageService: PageService) {
    super();
    this.addSubscription(
      this.projectsService.myCompanyProjectsState$.subscribe(
        (state: MyCompanyProjectsState) => {
          this.state = state;
          this.serverErrors = state.projectValidationErrors;
        }
      ));
    this.addSubscription(
      this.route.params.subscribe((params) => {
        if (params['id']) {
          this.projectId = params['id'];
          let project: Project = this.entitiesService.getEntity('projects', this.projectId);
          console.log(project);
          let locations = this.entitiesService.getEntities('projectLocations', project.locations as string[]);
          // locations = locations.map((location) => {
          //   return Object.assign({}, {
          //     name: location.name,
          //     address: location.address,
          //     district: location.district,
          //     city: location.city,
          //     state: location.state,
          //     zipcode: location.zipcode
          //   });
          //   });
          let formValue = Object.assign({}, {
            name: project.name,
            clientName: project.clientName,
            description: project.description,
            budget: project.budget,
            totalPositions: project.totalPositions,
            jobCategory: project.jobCategory
          }, { locations: locations} );
          this.pageService.changeTitle('Gerenciando ' + project.name);
          this.currentProject = formValue;
        } else {
          this.pageService.changeTitle('Novo Trabalho');
          this.projectId = null;
          this.currentProject = {
            name: null,
            clientName: null,
            description: null,
            budget: null,
            jobCategory: null,
            totalPositions: null,
            locations: [
              {
                id: null,
                name: null,
                address: null,
                district: null,
                city: null,
                state: null,
                zipcode: null,
                latitude: null,
                longitude: null
              }
              ]
          };
          this.projectsService.refreshForm();
        }
      })
    );
  }

  public saveProject(project: Project) {
    console.log(project);
    if (!this.state.savingProject) {
      this.projectsService.saveProject(project, this.projectId);
    }
  }
}
