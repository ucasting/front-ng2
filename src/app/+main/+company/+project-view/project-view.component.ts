import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../../../services/projects.service';
import {HasSubscriptions} from '../../../../utils/has-subscriptions.class';
import {MyCompanyProjectsState} from '../../../../reducers/my-company-projects.reducer';
import {Router, ActivatedRoute} from '@angular/router';
import {Project} from '../../../../models/project.model';
import {EntitiesService} from '../../../../services/entities.service';
import {PageService} from '../../../../services/page.service';
import {ProjectLocation} from '../../../../models/project-location.model';
import {UserJob} from '../../../../models/user-job.model';
import {Observable} from 'rxjs/Observable';
import * as lodash from 'lodash';

@Component({
  selector: 'uc-project-view',
  templateUrl: 'project-view.component.html',
  styles: [`
    .project-stats {
        margin-top: 0.9rem;
    }
`]
})
export class ProjectViewComponent extends HasSubscriptions implements OnInit {

  public state: MyCompanyProjectsState;
  public state$: Observable<MyCompanyProjectsState>;
  public projectId: string;
  public userJobId$: Observable<any>;
  public openUserJobForm$: Observable<boolean>;
  public userJobServerErrors$: Observable<any>;
  public project: Project;
  public locations: ProjectLocation[];
  public userJobIds: any[];
  public userJobs: UserJob[];
  public remainingBudget: number;
  public remainingPositions: number;
  public userJobAlertClosed: boolean;

  constructor(public projectsService: ProjectsService,
              public router: Router,
              public route: ActivatedRoute,
              public entitiesService: EntitiesService,
              public pageService: PageService) {
    super();
    this.projectId = this.route.snapshot.params['id'];
    this.project = this.entitiesService.getEntity('projects', this.projectId);
    this.locations = this.entitiesService.getEntities('projectLocations', this.project.locations as string[]);
    this.pageService.changeTitle('Gerenciando ' + this.project.name);
    this.state$ = this.projectsService.myCompanyProjectsState$;
    this.userJobId$ = this.state$.map(state => state.editingUserJobId);
    this.openUserJobForm$ = this.state$.map(state => state.openUserJobForm);
    this.userJobServerErrors$ = this.state$.map(state => state.userJobValidationErrors);
    this.addSubscription(
      this.projectsService.myCompanyProjectsState$.subscribe(
        (state: MyCompanyProjectsState) => {
          this.state = state;
          this.project = this.entitiesService.getEntity('projects', this.projectId);
          this.userJobIds = this.project.userJobs;
          this.userJobs = this.entitiesService.getEntities('userJobs', this.userJobIds);
          this.userJobs = lodash.sortBy(this.userJobs, ['cancelled', 'published']);
          const activeUserJobs = this.userJobs.filter((job) => !job.cancelled);
          this.remainingBudget = this.project.budget - activeUserJobs
              .reduce((sum, job: UserJob) => sum + job.totalCost, 0);
          this.remainingPositions = this.project.totalPositions - activeUserJobs
              .reduce((sum, job: UserJob) => sum + job.totalPositions, 0);
        }
      )
    );
  }

  public newUserJob() {
    this.projectsService.newUserJobForm();
  }

  public closeUserJobForm() {
    this.projectsService.closeUserJobForm();
  }

  public editUserJob(userJob: UserJob) {
    this.projectsService.editUserJob(userJob);
  }

  public saveUserJob(userJob: UserJob) {
    this.projectsService.saveUserJob(this.projectId, userJob, this.state.editingUserJobId);
  }

  public publishUserJob(userJob: UserJob) {
    this.projectsService.publishUserJob(userJob.id);
  }
  public cancelUserJob(userJob: UserJob) {
    this.projectsService.cancelUserJob(userJob.id);
  }

  public ngOnInit(): void {
    this.projectsService.refreshUserJobForm();
  }

}
