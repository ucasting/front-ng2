import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../../../services/projects.service';

@Component({
  selector: 'uc-projects',
  templateUrl: 'projects.component.html',
})
export class ProjectsComponent implements OnInit {
  constructor(public projectsService: ProjectsService) {

  }

  ngOnInit() {
    this.projectsService.loadMyCompanyProjects();
  }

}
