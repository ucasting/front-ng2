import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Observable, Subscription} from 'rxjs/Rx';
import {FormControl, FormGroup, FormBuilder} from '@angular/forms';
import {UsersService} from '../../../../services/users.service';
import {EntitiesService} from '../../../../services/entities.service';
import {User} from '../../../../models/user.model';
import {UsersState} from '../../../../reducers/users.reducer';
import {user} from '../../../../models/schemas';
import {PageChangedEvent} from 'ng2-bootstrap/pagination/pagination.component';
import {Modal} from 'angular2-modal/plugins/bootstrap';
import {PageService} from '../../../../services/page.service';
import {UserJobsService} from '../../../../services/user-jobs.service';
import {
  InviteCandidateModalComponent
} from '../../../shared/invite-candidate-modal/invite-candidate-modal.component';
import {MdDialog} from '@angular/material';

@Component({
  selector:      'uc-search-candidates',
  templateUrl:   'search-candidates.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class SearchCandidatesComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription[] = [];
  public entitiesService: EntitiesService;
  public usersService: UsersService;
  public filterForm: FormGroup;
  private formBuilder: FormBuilder;
  public usersState$: Observable<UsersState>;
  public candidates$: Observable<User[]>;
  public pages$: Observable<number>;
  public currentPage$: Observable<number>;
  public loading$: Observable<boolean>;

  public currentPage: number;

  public interestOptions: any[];
  public genderOptions: any[];
  public hairColorOptions: any[];
  public hairLengthOptions: any[];
  public hairTypeOptions: any[];
  public skinOptions: any[];
  public physicalTraitOptions: any[];
  public particularityOptions: any[];
  public eyeColorOptions: any[];

  constructor(
              entitiesService: EntitiesService,
              usersService: UsersService,
              formBuilder: FormBuilder,
              private pageService: PageService,
              private userJobsService: UserJobsService,
              private dialog: MdDialog
  ) {
    this.entitiesService = entitiesService;
    this.usersService = usersService;
    this.formBuilder = formBuilder;
    this.filterForm = this.formBuilder.group({
      itemsPerPage:    [9],
      name:            [null],
      type:            ['candidate'],
      interests:       [[]],
      idioms:          [[]],
      skins:           [[]],
      physicalTraits:  [[]],
      particularities: [[]],
      eyeColors:       [[]],
      hairTypes:       [[]],
      hairColors:      [[]],
      hairLengths:     [[]],
      genders:         [[]],
      minAge:          [null],
      maxAge:          [null],
      approved:        [true],
    });


    this.filterForm.valueChanges
      .distinctUntilChanged()
      .subscribe((filters) => {
        this.usersService.changeCandidatesFilters(filters);
      });

    this.usersState$ = this.usersService.usersState$;
    this.loading$ = this.usersState$.map(state => state.loadingCandidates);

    this.candidates$ =
      this.usersService.candidatesIds$
        .map((ids) => this.entitiesService.denormalizeIds(ids, user, 'users'));

    this.pages$ = this.usersService.candidatesPages$;
    this.currentPage$ = this.usersService.currentCandidatesPage$;

    this.subscriptions.push(
      this.currentPage$.subscribe((page: number) => {
        this.currentPage = page;
      }),
      this.entitiesService.jobTypeArray$.take(1).subscribe(jobTypes => this.interestOptions = jobTypes),
      this.entitiesService.genderArray$.take(1).subscribe(genders => this.genderOptions = genders),
      this.entitiesService.eyeColorArray$.take(1).subscribe(eyeColors => this.eyeColorOptions = eyeColors),
      this.entitiesService.hairColorArray$.take(1).subscribe(hairColors => this.hairColorOptions = hairColors),
      this.entitiesService.hairLengthArray$.take(1).subscribe(hairLengths => this.hairLengthOptions = hairLengths),
      this.entitiesService.hairTypeArray$.take(1).subscribe(hairTypes => this.hairTypeOptions = hairTypes),
      this.entitiesService.skinArray$.take(1).subscribe(skins => this.skinOptions = skins),
      this.entitiesService.particularityArray$.take(1).subscribe(particularities => this.particularityOptions = particularities),
      this.entitiesService.physicalTraitArray$.take(1).subscribe(physicalTraits => this.physicalTraitOptions = physicalTraits),
    );
  }

  public selected(value: any, controlName: string): void {
    const control: FormControl = (this.filterForm.controls[controlName] as FormControl);
    const newValue = control.value.concat(value.id);
    control.patchValue(newValue, {emitEvent: true});
  }

  public removed(value: any, controlName: string): void {
    const control: FormControl = (this.filterForm.controls[controlName] as FormControl);
    control.patchValue((control.value as number[]).filter(id => id !== value.id));
  }

  public pageChanged(pageChanged: PageChangedEvent): void {
    this.usersService.changeCandidatesPage(pageChanged.page);
  }

  public selectCandidate(candidate: User) {
    this.userJobsService.selectCandidateLegacy(candidate);
  }

  public inviteCandidate(candidate: User) {
    this.dialog.open(InviteCandidateModalComponent).componentInstance.candidate = candidate;
  }

  ngOnInit() {
    this.pageService.changeTitle('Busca de Candidatos');
    this.usersService.changeCandidatesFilters(this.filterForm.value);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

}
