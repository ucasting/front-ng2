import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../../../services/projects.service';
import {HasSubscriptions} from '../../../../utils/has-subscriptions.class';
import {MyCompanyProjectsState} from '../../../../reducers/my-company-projects.reducer';
import {Project} from '../../../../models/project.model';
import {PageService} from '../../../../services/page.service';
import {go} from '@ngrx/router-store';
import {Store} from '@ngrx/store';

@Component({
  selector: 'uc-project-list',
  templateUrl: 'project-list.component.html',
  styles: [`
    .brick {
        width: 50%;
        padding: 8px;
    }
    @media screen and (min-width: 768px){
        .brick { width: 33.333% }
    }
`
  ]
})
export class ProjectListComponent extends HasSubscriptions implements OnInit {
  public state: MyCompanyProjectsState;

  constructor(public projectsService: ProjectsService,
              public store: Store<any>,
              public pageService: PageService) {
    super();
    this.pageService.changeTitle('Trabalhos');
    this.addSubscription(this.projectsService.myCompanyProjectsState$.subscribe(
      (state: MyCompanyProjectsState) => this.state = state
    ));
  }

  ngOnInit() {
    this.projectsService.loadMyCompanyProjects();
  }

  public selectProject(project: Project) {
    this.store.dispatch(go(['main/hirer/projects', project.id]));
  }

  getProjectId(index, project) {
    return project.id;
  }

}
