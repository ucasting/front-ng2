import {Component, OnInit, ChangeDetectionStrategy, OnDestroy} from '@angular/core';
import {User} from '../../../../models/user.model';
import {AccountService} from '../../../../services/account.service';
import {Subscription} from 'rxjs/Rx';
import 'rxjs/add/observable/of';
import {EntitiesService} from '../../../../services/entities.service';
import * as schema from '../../../../models/schemas';
import {PageService} from '../../../../services/page.service';

@Component({

  selector: 'uc-view-my-profile',
  templateUrl: 'view-my-profile.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewMyProfileComponent implements OnInit, OnDestroy {


  public user: User;
  private userSubscription: Subscription;

  constructor(private accountService: AccountService,
              private entitiesService: EntitiesService,
              private pageService: PageService) {
    this.userSubscription = this.accountService.getUser().subscribe((user) => {
      this.user = this.entitiesService.denormalizeEntity(user, schema.user);
    });
  }

  ngOnInit() {
    this.pageService.changeTitle('Meu Perfil');
  }

  ngOnDestroy(): any {
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

}
