// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { ViewMyProfileComponent } from './view-my-profile.component';
//
// describe('Component: ViewMyProfile', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [ViewMyProfileComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([ViewMyProfileComponent],
//       (component: ViewMyProfileComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(ViewMyProfileComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(ViewMyProfileComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-view-my-profile></uc-view-my-profile>
//   `,
//   directives: [ViewMyProfileComponent]
// })
// class ViewMyProfileComponentTestController {
// }
//
