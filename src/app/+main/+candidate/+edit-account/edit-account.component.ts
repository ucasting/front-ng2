import {Component, OnInit, Output, EventEmitter, OnDestroy} from '@angular/core';
import {Observable, Subscription} from 'rxjs/Rx';
import {FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {FileUploader} from 'ng2-file-upload/ng2-file-upload';
import {User} from '../../../../models/user.model';
import {AccountService} from '../../../../services/account.service';
import {OptionsService} from '../../../../services/options.service';
import {EntitiesService} from '../../../../services/entities.service';
import * as schemas from '../../../../models/schemas';
import {getIdOrNull, STATES} from '../../../../utils/helpers';
import {Profile} from '../../../../models/profile.model';
import {Course} from '../../../../models/course.model';
import {Experience} from '../../../../models/experience.model';
import {ProfileIdiom} from '../../../../models/profile-idiom.model';
import {Particularity} from '../../../../models/particularity.model';
import {PhysicalTrait} from '../../../../models/physical-trait.model';
import {JobType} from '../../../../models/job-type.model';
import * as moment from 'moment';
import {PageService} from '../../../../services/page.service';

@Component({
  selector: 'uc-edit-account',
  templateUrl: 'edit-account.component.html',
})
export class EditAccountComponent implements OnInit, OnDestroy {


  private currentStep = 0;
  private userData: any;
  private user$: Observable<User>;
  private user: User;
  public avatarUploader: FileUploader;
  public coverUploader: FileUploader;
  public currentTab = 0;

  @Output() action = new EventEmitter();
  public states: string[];
  private updating$: Observable<boolean>;

  public dateErrorDefs: any = {required: 'Este campo é obrigatório.', pattern: 'Formato: dd/mm/yyyy'};

  // user form group
  private accountForm: FormGroup;
  private firstName: FormControl;
  private lastName: FormControl;
  private gender: FormControl;
  private email: FormControl;
  private username: FormControl;
  private slug: FormControl;
  private occupation: FormControl;
  private rg: FormControl;
  private cpf: FormControl;
  private phone: FormControl;
  private cellphone: FormControl;

  // password group
  private plainPassword: FormGroup;
  private password: FormControl;
  private confirm: FormControl;

  // profile group
  private profile: FormGroup;
  private drt: FormControl;
  private cnpj: FormControl;
  private description: FormControl;
  private skin: FormControl;
  private eyeColor: FormControl;
  private hairColor: FormControl;
  private hairLength: FormControl;
  private hairType: FormControl;
  private birthdate: FormControl;
  private address: FormControl;
  private city: FormControl;
  private state: FormControl;
  private zipcode: FormControl;
  private height: FormControl;
  private weight: FormControl;
  private waist: FormControl;
  private bust: FormControl;
  private hip: FormControl;
  private thorax: FormControl;
  private shoulder: FormControl;
  private manequim: FormControl;
  private shoesSize: FormControl;
  private hairAllowCut: FormControl;
  private hairAllowChangeColor: FormControl;
  private hairAllowStraighten: FormControl;
  private hasBeard: FormControl;
  private smoker: FormControl;
  private hashtags: FormArray;
  private idioms: FormArray;
  private courses: FormArray;
  private experiences: FormArray;

  private particularities: FormControl;
  private physicalTraits: FormControl;
  private typeInterests: FormControl;

  private jobTypes$: Observable<any>;
  private skins$: Observable<any>;
  private hairColors$: Observable<any>;
  private hairLengths$: Observable<any>;
  private hairTypes$: Observable<any>;
  private eyeColors$: Observable<any>;
  private genders$: Observable<any>;
  private idioms$: Observable<any>;
  private proficiencies$: Observable<any>;
  private particularities$: Observable<any>;
  private physicalTraits$: Observable<any>;

  public jobTypesOptions: any[];
  public skinsOptions: any[];
  public hairColorsOptions: any[];
  public hairLengthsOptions: any[];
  public hairTypesOptions: any[];
  public eyeColorsOptions: any[];
  public gendersOptions: any[];
  public idiomsOptions: any[];
  public proficienciesOptions: any[];
  public particularitiesOptions: any[];
  public physicalTraitsOptions: any[];

  private saved$: Observable<boolean>;
  private serverErrors: any;
  private serverErrorMessage$: Observable<string>;

  private validationErrorSubscription: Subscription;
  private autoSaveSubscription: Subscription;

  private subscriptions: Subscription[] = [];

  public datePattern = '(((0[1-9]|[12][0-9]|3[01])\/(0[13578]|1[02])|(0[1-9]|[12][0-9]|30)\/' +
    '(0[469]|11)|(0[1-9]|1\d|2[0-8])\/02)\/\d{4}|29\/02\/' +
    '(\d{2}(0[48]|[2468][048]|[13579][26])|([02468][048]|[1359][26])00))';


  constructor(private accountService: AccountService,
              private optionsService: OptionsService,
              private entitiesService: EntitiesService,
              private pageService: PageService,
              private fb: FormBuilder) {

    this.saved$ = this.accountService.getHasSaved();

    this.serverErrorMessage$ = this.accountService.getErrorMessage();
    this.validationErrorSubscription = this.accountService.getValidationErrors().subscribe(
      (errors) => {
        this.serverErrors = errors;
      }
    );

    this.jobTypes$ = this.entitiesService.jobTypeArray$;
    this.skins$ = this.entitiesService.skinArray$;
    this.hairColors$ = this.entitiesService.hairColorArray$;
    this.hairLengths$ = this.entitiesService.hairLengthArray$;
    this.hairTypes$ = this.entitiesService.hairTypeArray$;
    this.eyeColors$ = this.entitiesService.eyeColorArray$;
    this.genders$ = this.entitiesService.genderArray$;
    this.idioms$ = this.entitiesService.idiomArray$;
    this.proficiencies$ = this.entitiesService.proficiencyArray$;
    this.particularities$ = this.entitiesService.particularityArray$;
    this.physicalTraits$ = this.entitiesService.physicalTraitArray$;

    // mount password group
    this.password = new FormControl(null);
    this.confirm = new FormControl(null);
    this.plainPassword = this.fb.group({
      password: this.password,
      confirm: this.confirm
    });

    // mount profile group
    this.gender = new FormControl(null, Validators.required);
    this.drt = new FormControl(null);
    this.cnpj = new FormControl(null);
    this.description = new FormControl(null);
    this.birthdate = new FormControl(null);
    this.address = new FormControl(null);
    this.city = new FormControl(null);
    this.state = new FormControl(null);
    this.zipcode = new FormControl(null, Validators.pattern('[0-9]{5}-[0-9]{3}'));
    this.skin = new FormControl(null);
    this.eyeColor = new FormControl(null);
    this.hairColor = new FormControl(null);
    this.hairLength = new FormControl(null);
    this.hairType = new FormControl(null);
    this.hashtags = new FormArray([]);
    this.particularities = new FormControl();
    this.physicalTraits = new FormControl();
    this.idioms = new FormArray([]);
    this.courses = new FormArray([]);
    this.experiences = new FormArray([]);
    this.typeInterests = new FormControl();
    this.height = new FormControl(null, Validators.pattern('([1-2])([\.,][0-9]+)?'));
    this.weight = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.waist = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.bust = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.hip = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.thorax = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.shoulder = new FormControl(null, Validators.pattern('[0-9]{2,3}'));
    this.manequim = new FormControl(null, Validators.pattern('[0-9]{2}'));
    this.shoesSize = new FormControl(null, Validators.pattern('[0-9]{2}'));
    this.hairAllowCut = new FormControl(null);
    this.hairAllowChangeColor = new FormControl(null);
    this.hairAllowStraighten = new FormControl(null);
    this.hasBeard = new FormControl(null);
    this.smoker = new FormControl(null);
    this.profile = this.fb.group({
      drt: this.drt,
      cnpj: this.cnpj,
      description: this.description,
      birthdate: this.birthdate,
      address: this.address,
      gender: this.gender,
      city: this.city,
      state: this.state,
      zipcode: this.zipcode,
      hashtags: this.hashtags,
      idioms: this.idioms,
      courses: this.courses,
      experiences: this.experiences,

      height: this.height,
      weight: this.weight,
      waist: this.waist,
      bust: this.bust,
      hip: this.hip,
      thorax: this.thorax,
      shoulder: this.shoulder,
      manequim: this.manequim,
      shoesSize: this.shoesSize,
      skin: this.skin,
      eyeColor: this.eyeColor,
      hairColor: this.hairColor,
      hairLength: this.hairLength,
      hairType: this.hairType,
      hairAllowCut: this.hairAllowCut,
      hairAllowChangeColor: this.hairAllowChangeColor,
      hairAllowStraighten: this.hairAllowStraighten,
      hasBeard: this.hasBeard,
      smoker: this.smoker,
      particularities: this.particularities,
      typeInterests: this.typeInterests,
      physicalTraits: this.physicalTraits,
    });

    // mount user group
    this.firstName = new FormControl(null, Validators.required);
    this.lastName = new FormControl(null, Validators.required);
    this.email = new FormControl(null, Validators.required);
    this.username = new FormControl(null, Validators.required);
    this.slug = new FormControl('', Validators.required);
    this.occupation = new FormControl(null);
    this.rg = new FormControl(null);
    this.cpf = new FormControl(null);
    this.phone = new FormControl(null);
    this.cellphone = new FormControl(null);
    this.accountForm = this.fb.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      username: this.username,
      slug: this.slug,
      occupation: this.occupation,
      rg: this.rg,
      cpf: this.cpf,
      phone: this.phone,
      cellphone: this.cellphone,
      plainPassword: this.plainPassword,
      profile: this.profile,
    });

    this.subscriptions.push(
      this.proficiencies$.first().subscribe((options) => {
        this.proficienciesOptions = options;
      }),
      this.idioms$.first().subscribe((options) => {
        this.idiomsOptions = options;
      }),
      this.particularities$.first().subscribe((options) => {
        this.particularitiesOptions = options;
      }),
      this.physicalTraits$.first().subscribe((options) => {
        this.physicalTraitsOptions = options;
      }),
      this.jobTypes$.first().subscribe((options) => {
        this.jobTypesOptions = options;
      })
    );

    this.user$ = this.accountService.getUser();

    this.user$
      .first()
      .subscribe(
        (user) => {
          this.user = user;
          this.userData = _.cloneDeep(this.user);
          const denormalizedUser = this.entitiesService.denormalizeEntity(user, schemas.user);
          const profileData: Profile = denormalizedUser.profile as Profile;

          this.firstName.patchValue(user.firstName, {onlySelf: false, emitEvent: true});
          this.lastName.patchValue(user.lastName, {onlySelf: false, emitEvent: true});
          this.email.patchValue(user.email, {onlySelf: false, emitEvent: true});
          this.username.patchValue(user.username, {onlySelf: false, emitEvent: true});
          this.slug.patchValue(user.slug, {onlySelf: false, emitEvent: true});

          this.occupation.patchValue(user.occupation, {onlySelf: false, emitEvent: true});
          this.rg.patchValue(user.rg, {onlySelf: false, emitEvent: true});
          this.cpf.patchValue(user.cpf, {onlySelf: false, emitEvent: true});
          this.phone.patchValue(user.phone, {onlySelf: false, emitEvent: true});
          this.cellphone.patchValue(user.cellphone, {onlySelf: false, emitEvent: true});

          // profile values
          this.gender.patchValue(getIdOrNull(profileData.gender), {onlySelf: false, emitEvent: true});
          this.skin.patchValue(getIdOrNull(profileData.skin), {onlySelf: false, emitEvent: true});
          this.eyeColor.patchValue(getIdOrNull(profileData.eyeColor), {onlySelf: false, emitEvent: true});
          this.hairColor.patchValue(getIdOrNull(profileData.hairColor), {onlySelf: false, emitEvent: true});
          this.hairLength.patchValue(getIdOrNull(profileData.hairLength), {onlySelf: false, emitEvent: true});
          this.hairType.patchValue(getIdOrNull(profileData.hairType), {onlySelf: false, emitEvent: true});
          this.cnpj.patchValue(profileData.cnpj, {onlySelf: false, emitEvent: true});
          this.state.patchValue(profileData.state, {onlySelf: false, emitEvent: true});
          this.city.patchValue(profileData.city, {onlySelf: false, emitEvent: true});
          this.zipcode.patchValue(profileData.zipcode, {onlySelf: false, emitEvent: true});
          this.address.patchValue(profileData.address, {onlySelf: false, emitEvent: true});
          this.description.patchValue(profileData.description, {onlySelf: false, emitEvent: true});
          if (profileData.birthdate) {
            this.birthdate.patchValue(moment.utc(profileData.birthdate).format('DD/MM/YYYY'), {
              onlySelf: false,
              emitEvent: true
            });
          }
          this.height.patchValue(profileData.height, {onlySelf: false, emitEvent: true});
          this.weight.patchValue(profileData.weight, {onlySelf: false, emitEvent: true});
          this.waist.patchValue(profileData.waist, {onlySelf: false, emitEvent: true});
          this.bust.patchValue(profileData.bust, {onlySelf: false, emitEvent: true});
          this.hip.patchValue(profileData.hip, {onlySelf: false, emitEvent: true});
          this.thorax.patchValue(profileData.thorax, {onlySelf: false, emitEvent: true});
          this.shoulder.patchValue(profileData.shoulder, {onlySelf: false, emitEvent: true});
          this.manequim.patchValue(profileData.manequim, {onlySelf: false, emitEvent: true});
          this.shoesSize.patchValue(profileData.shoesSize, {onlySelf: false, emitEvent: true});
          this.hairAllowCut.patchValue(profileData.hairAllowCut, {onlySelf: false, emitEvent: true});
          this.hairAllowChangeColor.patchValue(profileData.hairAllowChangeColor, {onlySelf: false, emitEvent: true});
          this.hairAllowStraighten.patchValue(profileData.hairAllowStraighten, {onlySelf: false, emitEvent: true});
          this.hasBeard.patchValue(profileData.hasBeard, {onlySelf: false, emitEvent: true});
          this.smoker.patchValue(profileData.smoker, {onlySelf: false, emitEvent: true});

          profileData.courses.forEach((course: Course) => {
            const courseForm = this.fb.group({
              name: [course.name, Validators.required],
              entity: [course.entity, Validators.required],
              start: [course.start ? moment.utc(course.start).format('DD/MM/YYYY') : null,
                [
                  Validators.required,
                ]
              ],
              end: [course.end ? moment.utc(course.end).format('DD/MM/YYYY') : null,
                [
                  Validators.required,
                ]
              ]
            });
            this.courses.push(courseForm);
          });

          profileData.experiences.forEach((experience: Experience) => {
            const experienceForm = this.fb.group({
              name: [experience.name, Validators.required],
              description: [experience.description, Validators.required],
              company: [experience.company, Validators.required],
              type: [experience.type ? experience.type.id : '', Validators.required],
              start: [moment.utc(experience.start).format('DD/MM/YYYY'),
                [
                  Validators.required,

                ]
              ]
            });
            this.experiences.push(experienceForm);
          });

          profileData.idioms.forEach((profileIdiom: ProfileIdiom) => {
            const idiomForm = this.fb.group({
              idiom: [profileIdiom.idiom.id, Validators.required],
              proficiency: [profileIdiom.proficiency.id, Validators.required]
            });
            this.idioms.push(idiomForm);
          });
          const particularitiesValue = [];
          profileData.particularities.forEach((particularity: Particularity) => {
            particularitiesValue.push(particularity.id);
          });
          this.particularities.patchValue(particularitiesValue);

          const physicalTraitsValue = [];
          profileData.physicalTraits.forEach((physicalTrait: PhysicalTrait) => {
            physicalTraitsValue.push(physicalTrait.id);
          });
          this.physicalTraits.patchValue(physicalTraitsValue);

          const typeInterestsValue = [];
          profileData.typeInterests.forEach((jobType: JobType) => {
            typeInterestsValue.push(jobType.id);
          });
          this.typeInterests.patchValue(typeInterestsValue);

          this.userData.plainPassword = {
            password: null,
            confirm: null
          };
        }
      );

    this.avatarUploader = this.accountService.getAvatarUploader();
    this.coverUploader = this.accountService.getCoverUploader();

    this.updating$ = this.accountService.getUpdating();

    this.states = STATES;


    this.autoSaveSubscription = this.accountForm.statusChanges
      .do((status) => console.log(status, this.accountForm.errors))
      .debounceTime(5000)
      // .filter(status => status === "VALID")
      .subscribe(() => {
        // only auto saves if it has been changed
        if (this.accountForm.dirty) {
          this.saveAccount();
        }
      });


  }

  ngOnInit() {
    this.pageService.changeTitle('Editar Perfil');
  }

  goToStep(step: number) {
    this.currentStep = step;
  }

  goToTab(tab: number) {
    this.currentTab = tab;
  }

  saveAccount(): void {
    this.accountService.editAccount(this.accountForm.value);
  }

  addHashtag() {
    const hashtag: FormControl = new FormControl(null);
    this.hashtags.push(hashtag);
  }

  toggleParticularity(object) {
    this.toggleArray(object, this.userData.profile.particularities);
  }

  toggleTrait(object) {
    this.toggleArray(object, this.userData.profile.physicalTraits$);
  }

  toggleInterest(object) {
    this.toggleArray(object, this.userData.profile.typeInterests);
  }

  // Adds or removes an object with an id from the array
  private toggleArray(object: {id: any}, array: any[]) {
    const index = _.findIndex(array, {id: object.id});
    if (index === -1) {
      array.push(object);
    } else {
      array.splice(index, 1);
    }
  }

  containsParticularity(object: {id: any}) {
    return (_.findIndex(this.userData.profile.particularities, {id: object.id}) !== -1);
  }

  containsTrait(object: {id: any}) {
    return (_.findIndex(this.userData.profile.physicalTraits$, {id: object.id}) !== -1);
  }

  containsInterest(object: {id: any}) {
    return (_.findIndex(this.userData.profile.typeInterests, {id: object.id}) !== -1);
  }


  addCourseForm() {
    const courseForm = this.fb.group({
      name: ['', Validators.required],
      entity: ['', Validators.required],
      start: ['',
        [
          Validators.required,
        ]
      ],
      end: ['',
        [
          Validators.required,
        ]
      ]
    });
    this.courses.push(courseForm);
  }

  addExperienceForm() {
    const experienceForm = this.fb.group({
      name: ['', Validators.required],
      description: [''],
      company: ['', Validators.required],
      type: ['', Validators.required],
      start: ['',
        [
          Validators.required,

        ]
      ]
    });
    this.experiences.push(experienceForm);
  }

  addIdiomForm() {
    const idiomForm = this.fb.group({
      idiom: ['', Validators.required],
      proficiency: ['', Validators.required],
    });
    this.idioms.push(idiomForm);
  }

  removeIdiomForm(index: number) {
    this.idioms.removeAt(index);
  }

  removeExperienceForm(index: number) {
    this.experiences.removeAt(index);
  }

  removeCourseForm(index: number) {
    this.courses.removeAt(index);
  }

  ngOnDestroy(): void {
    if (this.validationErrorSubscription) {
      this.validationErrorSubscription.unsubscribe();
    }
    if (this.autoSaveSubscription) {
      this.autoSaveSubscription.unsubscribe();
    }
    this.subscriptions.forEach(
      (subscription) => {
        subscription.unsubscribe();
      }
    );
  }
}
