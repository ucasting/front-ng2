import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ProfileService} from '../../../../../services/profile.service';
import {Gallery} from '../../../../../models/gallery.model';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import {FileUploader} from 'ng2-file-upload/ng2-file-upload';
import {AuthenticationService} from '../../../../../services/authentication.service';
import {environment} from '../../../../../environments/environment';
import {GalleryMedia} from '../../../../../models/gallery-media.model';
import {Subscription, Observable, Subject} from 'rxjs/Rx';
import {EntitiesService} from '../../../../../services/entities.service';

@Component({
  selector: 'uc-edit-portfolio',
  templateUrl: 'edit-portfolio.component.html',
})
export class EditPortfolioComponent implements OnInit, OnDestroy {

  currentAlbumId$: Subject<string> = new Subject<string>();
  currentAlbum$: Observable<Gallery>;
  currentAlbum: Gallery;

  albumGroup: FormGroup;

  imageUploader: FileUploader;

  private subscriptions: Subscription[] = [];

  @Input() albums: Gallery[];
  private currentAlbumId: string;

  constructor(private profileService: ProfileService,
              private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private entitiesService: EntitiesService) {

    this.currentAlbumId$.publishLast().refCount();
    this.subscriptions.push(
      this.currentAlbumId$.subscribe((id) => {
        this.currentAlbumId = id;
        if (id === null) {
          this.imageUploader.cancelAll();
        }
      })
    );
    this.currentAlbum$ = this.currentAlbumId$.map((id) => this.entitiesService.getEntity('galleries', id));
    this.albumGroup = this.formBuilder.group({
      name: ['', Validators.required]
    });
    this.imageUploader = new FileUploader({
      // allowedFileType: ['jpg', 'jpeg', 'png'],
      autoUpload: true,
      isHTML5: true,
      removeAfterUpload: true
    });
    this.authService.getToken$().subscribe(
      token => {
        this.imageUploader.authToken = 'Bearer ' + token;
      }
    );

    this.imageUploader.onSuccessItem = (item, response, status, header) => {
      this.profileService.addImageSuccess(JSON.parse(response));
    };

    this.imageUploader.onErrorItem = (item, response, status, header) => {
      this.profileService.addImageFail(JSON.parse(response));
    };

    this.subscriptions.push(
      this.albumGroup.statusChanges
        .debounceTime(1000)
        .filter(status => status === 'VALID')
        .subscribe(() => {
          // only auto saves if it has been changed
          if (this.albumGroup.dirty) {
            this.saveAlbum(this.currentAlbumId);
          }
        })
    );

    this.subscriptions.push(
      this.currentAlbum$.subscribe((album) => {
        this.currentAlbum = album;
        console.log(album);
        if (album) {
          ((this.albumGroup.controls as any).name as FormControl).patchValue(album.name);
          this.imageUploader.options.url = `${environment.APIPath}my/profile/album/${album.id}/image`;
        }
      })
    );
  }

  addAlbum() {
    this.profileService.addAlbum();
  }

  deleteAlbum(id: string) {
    this.profileService.removeAlbum(id);
  }

  deleteGalleryMedia(galleryMedia) {
    this.profileService.deleteGalleryMedia(galleryMedia);
  }

  viewImage(galleryMedia: GalleryMedia) {
    console.log('view image');
  }

  saveAlbum(id: string) {
    if (id) {
      this.profileService.editAlbum(id, this.albumGroup.value);
    }
  }

  ngOnInit() {

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => {
      subscription.unsubscribe();
    });
  }

}
