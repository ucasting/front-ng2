// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { EditPortfolioComponent } from './edit-portfolio.component';
//
// describe('Component: EditPortfolio', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [EditPortfolioComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([EditPortfolioComponent],
//       (component: EditPortfolioComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(EditPortfolioComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(EditPortfolioComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-edit-portfolio></uc-edit-portfolio>
//   `,
//   directives: [EditPortfolioComponent]
// })
// class EditPortfolioComponentTestController {
// }
//
