import {NgModule} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import {HttpModule, RequestOptions, Http} from '@angular/http';
import {authHttpServiceFactory} from '../services/auth-http-service.factory';
import {AccountActions} from '../actions/account-actions';
import {CompanyJobsActions} from '../actions/company-jobs-actions';
import {NotificationsActions} from '../actions/notifications-actions';
import {AuthActions} from '../actions/auth-actions';
import {PageActions} from '../actions/page-actions';
import {ProjectsActions} from '../actions/projects-actions';
import {OptionsActions} from '../actions/options-actions';
import {ProfileActions} from '../actions/profile-actions';
import {UserJobsActions} from '../actions/user-jobs-actions';
import {UsersActions} from '../actions/users-actions';
import {AuthGuard} from '../guards/auth-guard';
import {CandidateGuard} from '../guards/candidate-guard';
import {CompanyAdminGuard} from '../guards/company-admin-guard';
import {CompanyUserGuard} from '../guards/company-user-guard';
import {HirerGuard} from '../guards/hirer-guard';
import {AuthenticationService} from '../services/authentication.service';
import {AuthorizationService} from '../services/authorization.service';
import {AccountService} from '../services/account.service';
import {OptionsService} from '../services/options.service';
import {ProfileService} from '../services/profile.service';
import {EntitiesService} from '../services/entities.service';
import {NotificationsService} from '../services/notifications.service';
import {UsersService} from '../services/users.service';
import {UsersApiService} from '../services/users-api.service';
import {UserJobsService} from '../services/user-jobs.service';
import {PageService} from '../services/page.service';
import {UserJobsApiService} from '../services/user-jobs-api.service';
import {MyCompanyJobsService} from '../services/my-company-jobs.service';
import {ProjectsService} from '../services/projects.service';
import {ProjectsApiService} from '../services/projects-api.service';

@NgModule({
  imports: [HttpModule],
  providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AuthenticationService,
    AuthorizationService,
    AccountService,
    OptionsService,
    ProfileService,
    EntitiesService,
    NotificationsService,
    UsersService,
    UsersApiService,
    PageService,
    UserJobsService,
    UserJobsApiService,
    MyCompanyJobsService,
    ProjectsService,
    ProjectsApiService,
    AccountActions,
    CompanyJobsActions,
    NotificationsActions,
    AuthActions,
    PageActions,
    OptionsActions,
    ProjectsActions,
    ProfileActions,
    UserJobsActions,
    UsersActions,
    AuthGuard,
    CandidateGuard,
    CompanyAdminGuard,
    CompanyUserGuard,
    HirerGuard,
  ]
})
export class CoreModule {
}
