import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Overlay} from 'angular2-modal';
import {Modal} from 'angular2-modal/plugins/bootstrap';

@Component({
  selector: 'uc-app',
  templateUrl: 'ucasting-app.component.html',
  providers: [Modal]
})
export class UcastingAppComponent implements OnInit {

  constructor(public overlay: Overlay, public viewContainerRef: ViewContainerRef) {
    overlay.defaultViewContainer = this.viewContainerRef;
  }

  ngOnInit(): void {
  }


}
