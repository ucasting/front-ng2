import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {Observable} from 'rxjs/Rx';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {PageService} from '../../../services/page.service';

@Component({
  selector: 'uc-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;

  hasError$: Observable<boolean>;
  authChecking$: Observable<boolean>;

  constructor(private authService: AuthenticationService,
              private pageService: PageService,
              private formBuilder: FormBuilder) {
    this.hasError$ = this.authService.getHasError();
    this.authChecking$ = this.authService.getAuthChecking();

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.pageService.changeTitle('Login');
  }

  onLogin() {
    let values = this.loginForm.value;
    this.authService.login(values.username, values.password);
  }

}
