import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'uc-security',
  template: '<router-outlet></router-outlet>',
})
export class SecurityComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
