import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {PageService} from '../../../services/page.service';

@Component({
  selector: 'uc-logout',
  template: '',
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthenticationService,
              private pageService: PageService) {
  }

  ngOnInit() {
    this.authService.logout();
    this.pageService.changeTitle(null);
  }

}
