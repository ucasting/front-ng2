// import {
//   beforeEach,
//   beforeEachProviders,
//   describe,
//   expect,
//   it,
//   inject,
// } from '@angular/core/testing';
// import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
// import { Component } from '@angular/core';
// import { By } from '@angular/platform-browser';
// import { NotAuthorizedComponent } from './not-authorized.component';
//
// describe('Component: NotAuthorized', () => {
//   let builder: TestComponentBuilder;
//
//   beforeEachProviders(() => [NotAuthorizedComponent]);
//   beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
//     builder = tcb;
//   }));
//
//   it('should inject the component', inject([NotAuthorizedComponent],
//       (component: NotAuthorizedComponent) => {
//     expect(component).toBeTruthy();
//   }));
//
//   it('should create the component', inject([], () => {
//     return builder.createAsync(NotAuthorizedComponentTestController)
//       .then((fixture: ComponentFixture<any>) => {
//         let query = fixture.debugElement.query(By.directive(NotAuthorizedComponent));
//         expect(query).toBeTruthy();
//         expect(query.componentInstance).toBeTruthy();
//       });
//   }));
// });
//
// @Component({
//   selector: 'test',
//   template: `
//     <uc-not-authorized></uc-not-authorized>
//   `,
//   directives: [NotAuthorizedComponent]
// })
// class NotAuthorizedComponentTestController {
// }
//
