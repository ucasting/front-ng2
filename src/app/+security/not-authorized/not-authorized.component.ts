import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'uc-not-authorized',
  template: '<p>Você não tem permissão para acessar esta página.</p>',
})
export class NotAuthorizedComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

}
