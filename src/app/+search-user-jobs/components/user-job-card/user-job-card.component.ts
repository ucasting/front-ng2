import {Component, OnInit, Input, EventEmitter, ChangeDetectionStrategy, Output} from '@angular/core';
import {UserJob} from '../../../../models/user-job.model';
import {EntitiesService} from '../../../../services/entities.service';
import {ProjectLocation} from '../../../../models/project-location.model';

@Component({
  selector:        'uc-user-job-card',
  templateUrl:     'user-job-card.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles:          [
    `
    .divider {
      margin-bottom: 12px;
      width: 100%;
      height: 1px;
    }
  `
  ]
})
export class UserJobCardComponent implements OnInit {

  @Input() userJob: UserJob;
  @Input() expanded = false;
  @Input() applyVisible = true;
  @Input() applying = false;
  @Input() cancelling = false;
  @Input() inSelection = false;

  location: ProjectLocation;

  @Output() expand: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() shrink: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() cancel: EventEmitter<UserJob> = new EventEmitter<UserJob>();
  @Output() apply: EventEmitter<UserJob> = new EventEmitter<UserJob>();

  showBody = false;


  constructor(private entitiesService: EntitiesService) {

  }

  ngOnInit() {
    this.location = this.entitiesService.getEntity('projectLocations', this.userJob.location as string);
    const job = this.userJob;
    if (job.hasBeard ||
      job.minAge ||
      job.maxAge ||
      job.eyeColors.length > 0 ||
      job.hairColors.length > 0 ||
      job.hairLengths.length > 0 ||
      job.hairTypes.length > 0 ||
      job.particularities.length > 0 ||
      job.physicalTraits.length > 0 ||
      job.skins.length > 0) {
      this.showBody = true;
    }
  }

}
