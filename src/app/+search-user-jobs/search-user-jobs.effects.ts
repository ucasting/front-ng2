import {UserJobsApiService} from '../../services/user-jobs-api.service';
import {Effect, Actions} from '@ngrx/effects';
import {SearchUserJobsActions} from './search-user-jobs.actions';
import {Observable} from 'rxjs';
import {normalize, arrayOf} from 'normalizr';
import {userJob} from '../../models/schemas';
import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../reducers/index';
import {NotificationsService} from '../../services/notifications.service';

@Injectable()
export class SearchUserJobsEffects {

  @Effect() searchUserJobs$ = this.actions$
    .ofType(SearchUserJobsActions.SEARCH_USER_JOBS)
    .switchMap(action => this.api.searchUserJobs(action.payload)
      .retry(2)
      .map((res) => {
        let result = res.json();
        let normalized = normalize(result.items, arrayOf(userJob));
        result.entities = normalized.entities;
        result.ids = normalized.result;
        return SearchUserJobsActions.searchUserJobsSuccess(result);
      })
      .catch((res) => Observable.of(SearchUserJobsActions.searchUserJobsFail(res.json())))
    );

  @Effect() searchCompatible$ = this.actions$
    .ofType(SearchUserJobsActions.SEARCH_COMPATIBLE)
    .switchMap(action => this.api.searchCompatibleUserJobs(action.payload)
      .retry(2)
      .map((res) => {
        let result = res.json();
        let normalized = normalize(result.items, arrayOf(userJob));
        result.entities = normalized.entities;
        result.ids = normalized.result;
        return SearchUserJobsActions.searchCompatibleSuccess(result);
      })
      .catch((res) => Observable.of(SearchUserJobsActions.searchCompatibleFail(res.json())))
    );

  @Effect() applyForJob$ = this.actions$
    .ofType(SearchUserJobsActions.APPLY_FOR_JOB)
    .switchMap(action => this.api.applyForJob(action.payload)
      .map((res) => {
        let result = res.json();
        let normalized = normalize(result, userJob);
        return SearchUserJobsActions.applyForJobSuccess(normalized);
      })
      .catch((res) => {
        return Observable.of(SearchUserJobsActions.applyForJobFail(res.json(), action.payload));
      })
      .retry(2)
    );

  @Effect() applyForJobFail$ = this.actions$
    .ofType(SearchUserJobsActions.APPLY_FOR_JOB_FAIL)
    .do(action => {
      this.notificationsService.addDanger('Erro se candidatando à job', action.payload.error.message);
    })
    .filter(() => false);

  @Effect() cancelApplication$ = this.actions$
    .ofType(SearchUserJobsActions.CANCEL_APPLICATION)
    .switchMap(action => this.api.cancelApplicationForJob(action.payload)
      .map(res => {
        let result = res.json();
        let normalized = normalize(result, userJob);
        return SearchUserJobsActions.cancelApplicationSuccess(action.payload, normalized);
      })
      .catch(res => {
        return Observable.of(SearchUserJobsActions.cancelApplicationFail(action.payload, res.json()));
      })
      .retry(2)
    );

  @Effect() cancelApplicationSuccess$ = this.actions$
    .ofType(SearchUserJobsActions.CANCEL_APPLICATION_SUCCESS)
    .do(action => {
      this.notificationsService.addSuccess('Candidatura cancelada', 'Sua candidatura foi cancelada com sucesso.');
    })
    .filter(() => false);

  @Effect() cancelApplicationFail$ = this.actions$
    .ofType(SearchUserJobsActions.CANCEL_APPLICATION_FAIL)
    .do(action => {
      this.notificationsService.addDanger('Erro cancelando candidatura', action.payload.error.message);
    })
    .filter(() => false);

  constructor(private api: UserJobsApiService,
              private actions$: Actions,
              private store$: Store<AppState>,
              private notificationsService: NotificationsService) {
  }

}
