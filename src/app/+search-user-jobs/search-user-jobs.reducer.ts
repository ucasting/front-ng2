import {Action} from '@ngrx/store';
import {SearchUserJobsActions} from './search-user-jobs.actions';
import {NormalizedResponse} from '../../models/schemas';
import {PaginatedResponse} from '../../models/paginated-response.interface';
import * as lodash from 'lodash';

export interface SearchUserJobsState {
  loadingAll: boolean;
  loadingCompatible: boolean;
  allPagesLoaded: Array<number>;
  allPagesTotal: number;
  compatiblePagesLoaded: Array<number>;
  compatiblePagesTotal: number;
  filters: any;
  compatibleUserJobs: Array<any>;
  allUserJobs: Array<any>;
  allCount: number;
  compatibleCount: number;
  applyingForJobs: Array<any>;
  expandedAllJob: any;
  expandedCompatibleJob: any;
  cancellingApplicationsForJobs: Array<any>;
}

export const initialSearchUserJobsState: SearchUserJobsState = {
  loadingAll: false,
  loadingCompatible: false,
  allPagesLoaded: [],
  allPagesTotal: null,
  compatiblePagesLoaded: [],
  compatiblePagesTotal: null,
  filters: {},
  compatibleUserJobs: [],
  allUserJobs: [],
  allCount: null,
  compatibleCount: null,
  applyingForJobs: [],
  expandedAllJob: null,
  expandedCompatibleJob: null,
  cancellingApplicationsForJobs: [],
};

export function searchUserJobsReducer(state: SearchUserJobsState = initialSearchUserJobsState,
                                      action: Action): SearchUserJobsState {
  switch (action.type) {
    case SearchUserJobsActions.SEARCH_USER_JOBS:
      return Object.assign({}, state, {
        loadingAll: true
      });
    case SearchUserJobsActions.SEARCH_USER_JOBS_SUCCESS: {
      let payload: PaginatedResponse = action.payload;
      return Object.assign({}, state, {
        loadingAll: false,
        allCount: payload.total,
        allUserJobs: lodash.union(state.allUserJobs, payload.ids),
        allPagesTotal: payload.pages,
        allPagesLoaded: lodash.union(state.allPagesLoaded, [payload.currentPage]),
      });
    }
    case SearchUserJobsActions.SEARCH_USER_JOBS_FAIL:
      return Object.assign({}, state, {
        loadingAll: false
      });
    case SearchUserJobsActions.SEARCH_COMPATIBLE:
      return Object.assign({}, state, {
        loadingCompatible: true
      });
    case SearchUserJobsActions.SEARCH_COMPATIBLE_SUCCESS: {
      let payload: PaginatedResponse = action.payload;
      return Object.assign({}, state, {
        loadingCompatible: false,
        compatibleCount: payload.total,
        compatibleUserJobs: lodash.union(state.compatibleUserJobs, payload.ids),
        compatiblePagesTotal: payload.pages,
        compatiblePagesLoaded: lodash.union(state.compatiblePagesLoaded, [payload.currentPage]),
      });
    }
    case SearchUserJobsActions.SEARCH_COMPATIBLE_FAIL:
      return Object.assign({}, state, {
        loadingCompatible: false
      });
    case SearchUserJobsActions.APPLY_FOR_JOB: {
      let indexOfJob = state.applyingForJobs.indexOf(action.payload);
      // user is already applying for job, ignore
      if (indexOfJob !== -1) {
        return state;
      }
      let applying = [...state.applyingForJobs];
      applying.push(action.payload);
      return Object.assign({}, state, {
        applyingForJobs: applying
      });
    }
    case SearchUserJobsActions.CANCEL_APPLICATION: {
      let indexOfJob = state.cancellingApplicationsForJobs.indexOf(action.payload);
      // user is already applying for job, ignore
      if (indexOfJob !== -1) {
        return state;
      }
      let cancelling = [...state.applyingForJobs];
      cancelling.push(action.payload);
      return Object.assign({}, state, {
        cancellingApplicationsForJobs: cancelling
      });
    }
    case SearchUserJobsActions.APPLY_FOR_JOB_SUCCESS:
    case SearchUserJobsActions.APPLY_FOR_JOB_FAIL: {
      let indexOfJob = state.applyingForJobs.indexOf(action.payload.jobId);
      if (indexOfJob === -1) {
        return state;
      }
      let applying = state.applyingForJobs
        .slice(0, indexOfJob)
        .concat(state.applyingForJobs.slice(indexOfJob + 1));
      return Object.assign({}, state, {
        applyingForJobs: applying
      });
    }
    case SearchUserJobsActions.CANCEL_APPLICATION_SUCCESS:
    case SearchUserJobsActions.CANCEL_APPLICATION_FAIL: {
      let indexOfJob = state.cancellingApplicationsForJobs.indexOf(action.payload.jobId);
      if (indexOfJob === -1) {
        return state;
      }
      let cancelling = state.cancellingApplicationsForJobs
        .slice(0, indexOfJob)
        .concat(state.cancellingApplicationsForJobs.slice(indexOfJob + 1));
      return Object.assign({}, state, {
        cancellingApplicationsForJobs: cancelling
      });
    }
    case SearchUserJobsActions.EXPAND_ALL_JOB: {
      return Object.assign({}, state, {
        expandedAllJob: action.payload
      });
    }
    case SearchUserJobsActions.EXPAND_COMPATIBLE_JOB: {
      return Object.assign({}, state, {
        expandedCompatibleJob: action.payload
      });
    }
    case SearchUserJobsActions.SHRINK_ALL_JOB: {
      return Object.assign({}, state, {
        expandedAllJob: null
      });
    }
    case SearchUserJobsActions.SHRINK_COMPATIBLE_JOB: {
      return Object.assign({}, state, {
        expandedCompatibleJob: null
      });
    }
    default:
      return state;
  }
}
