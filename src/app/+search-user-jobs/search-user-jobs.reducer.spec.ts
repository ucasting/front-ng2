import {SearchUserJobsActions} from './search-user-jobs.actions';
import {searchUserJobsReducer} from './search-user-jobs.reducer';
import {initialSearchUserJobsState} from './search-user-jobs.reducer';

describe('Search user jobs reducer', () => {
  it('should display loading when loading jobs', () => {
    const state = searchUserJobsReducer(initialSearchUserJobsState, {type: SearchUserJobsActions.SEARCH_USER_JOBS, payload: {}});
    expect(state.loadingAll).toBe(true);
  });
});
