import {Action} from '@ngrx/store';
import {NormalizedResponse} from '../../models/schemas';
import {ApiError} from '../../models/api-error.interface';
import {PaginatedResponse} from '../../models/paginated-response.interface';
export class SearchUserJobsActions {
  static SEARCH_USER_JOBS = '[SearchUserJobs] Search user jobs';
  static SEARCH_USER_JOBS_SUCCESS = '[SearchUserJobs] Search user jobs success';
  static SEARCH_USER_JOBS_FAIL = '[SearchUserJobs] Search user jobs fail';
  static SEARCH_COMPATIBLE = '[SearchUserJobs] Search compatible user jobs';
  static SEARCH_COMPATIBLE_SUCCESS = '[SearchUserJobs] Search compatible user jobs success';
  static SEARCH_COMPATIBLE_FAIL = '[SearchUserJobs] Search compatible user jobs fail';
  static APPLY_FOR_JOB = '[SearchUserJobs] Apply for job start';
  static APPLY_FOR_JOB_SUCCESS = '[SearchUserJobs] Apply for job success';
  static APPLY_FOR_JOB_FAIL = '[SearchUserJobs] Apply for job fail';
  static EXPAND_ALL_JOB = '[SearchUserJobs] Expand job in all tab';
  static EXPAND_COMPATIBLE_JOB = '[SearchUserJobs] Expand job in compatible tab';
  static SHRINK_ALL_JOB = '[SearchUserJobs] Shrink job in all tab';
  static SHRINK_COMPATIBLE_JOB = '[SearchUserJobs] Shrink job in compatible tab';
  static CANCEL_APPLICATION = '[SearchUserJobs] Cancel application';
  static CANCEL_APPLICATION_SUCCESS = '[SearchUserJobs] Cancel application success';
  static CANCEL_APPLICATION_FAIL = '[SearchUserJobs] Cancel application fail';

  static searchUserJobs(query: any = {page: 1}): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_USER_JOBS,
      payload: Object.assign(query, {published: true, cancelled: false, finished: false, active: true, approved: true})
    };
  }

  static searchUserJobsSuccess(result: PaginatedResponse): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_USER_JOBS_SUCCESS,
      payload: result
    };
  }

  static searchUserJobsFail(error: any): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_USER_JOBS_FAIL,
      payload: error
    };
  }

  static searchCompatible(query: any = {page: 1}): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_COMPATIBLE,
      payload: Object.assign(query, {published: true, cancelled: false, finished: false, active: true, approved: true})
    };
  }

  static searchCompatibleSuccess(result: PaginatedResponse): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_COMPATIBLE_SUCCESS,
      payload: result
    };
  }

  static searchCompatibleFail(error: any): Action {
    return {
      type:    SearchUserJobsActions.SEARCH_COMPATIBLE_FAIL,
      payload: error
    };
  }

  static applyForJob(jobId: any): Action {
    return {
      type:    SearchUserJobsActions.APPLY_FOR_JOB,
      payload: jobId
    };
  }

  static applyForJobSuccess(normalizedRes: NormalizedResponse): Action {
    return {
      type:    SearchUserJobsActions.APPLY_FOR_JOB_SUCCESS,
      payload: Object.assign(normalizedRes, {jobId: normalizedRes.result})
    };
  }

  static applyForJobFail(error: ApiError, jobId): Action {
    return {
      type:    SearchUserJobsActions.APPLY_FOR_JOB_FAIL,
      payload: {error, jobId}
    };
  }

  static expandAllJob(jobId): Action {
    return {
      type:    SearchUserJobsActions.EXPAND_ALL_JOB,
      payload: jobId
    };
  }

  static expandCompatibleJob(jobId): Action {
    return {
      type:    SearchUserJobsActions.EXPAND_COMPATIBLE_JOB,
      payload: jobId
    };
  }

  static shrinkAllJob(jobId): Action {
    return {
      type:    SearchUserJobsActions.SHRINK_ALL_JOB,
      payload: jobId
    };
  }

  static shrinkCompatibleJob(jobId): Action {
    return {
      type:    SearchUserJobsActions.SHRINK_COMPATIBLE_JOB,
      payload: jobId
    };
  }

  static cancelApplication(jobId): Action {
    return {
      type:    SearchUserJobsActions.CANCEL_APPLICATION,
      payload: jobId
    };
  }

  static cancelApplicationSuccess(jobId: number, response: NormalizedResponse): Action {
    return {
      type:    SearchUserJobsActions.CANCEL_APPLICATION_SUCCESS,
      payload: {jobId, response}
    };
  }

  static cancelApplicationFail(jobId: number, error: ApiError): Action {
    return {
      type:    SearchUserJobsActions.CANCEL_APPLICATION_FAIL,
      payload: {jobId, error}
    };
  }


}
