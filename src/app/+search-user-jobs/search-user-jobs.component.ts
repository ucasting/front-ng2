import {Component, OnInit, AfterViewInit, QueryList, ViewChildren} from '@angular/core';
import {AppState} from '../../reducers/index';
import {Store} from '@ngrx/store';
import {SearchUserJobsActions} from './search-user-jobs.actions';
import {Observable} from 'rxjs/Observable';
import {SearchUserJobsState} from './search-user-jobs.reducer';
import {EntitiesState} from '../../reducers/entities.reducer';
import {UserJob} from '../../models/user-job.model';
import {HasSubscriptions} from '../../utils/has-subscriptions.class';
import {AngularMasonry} from 'angular2-masonry/src/masonry';
import {AccountService} from '../../services/account.service';
import {UserSelection} from '../../models/user-selection.model';
import {EntitiesService} from '../../services/entities.service';
import {PageService} from '../../services/page.service';

@Component({
  selector: 'uc-search-user-jobs',
  templateUrl: 'search-user-jobs.component.html',
  styles: [`
        .fade-in {
    -webkit-animation: fadein 0.8s; /* Safari, Chrome and Opera > 12.1 */
            animation: fadein 0.8s;
        }
        @keyframes fadein {
          0% { opacity: 0; }
          50% { opacity: 0; }
          100% { opacity: 1; }
        }
        @-webkit-keyframes fadein {
           0% { opacity: 0; }
          50% { opacity: 0; }
          100% { opacity: 1; }
        }
    `]
})
export class SearchUserJobsComponent extends HasSubscriptions implements OnInit, AfterViewInit {


  public currentTab = 0;
  private state$: Observable<SearchUserJobsState>;
  private entitiesState$: Observable<EntitiesState>;

  public state: SearchUserJobsState;
  private userId;

  @ViewChildren(AngularMasonry) masonries: QueryList<AngularMasonry>;

  constructor(private store: Store<AppState>,
              private accountService: AccountService,
              private entitiesService: EntitiesService,
              private pageService: PageService) {
    super();
    this.state$ = this.store.select((state: AppState) => state.searchUserJobs);
    this.entitiesState$ = this.store.select((state) => state.entities);
    accountService.getUserId().take(1).subscribe(id => this.userId = id);

    this.addSubscription(
      this.state$.subscribe((state: SearchUserJobsState) => {
        this.state = state;
      })
    );

  }

  ngOnInit() {
    this.pageService.changeTitle('Vagas');
    this.store.dispatch(SearchUserJobsActions.searchUserJobs());
    this.store.dispatch(SearchUserJobsActions.searchCompatible());
  }

  ngAfterViewInit(): void {

  }

  setTab(index: number) {
    this.currentTab = index;
  }

  expandCompatible(job: UserJob) {
    this.store.dispatch(SearchUserJobsActions.expandCompatibleJob(job.id));
    this.masonries.first.layout();
  }

  expandAll(job: UserJob) {
    this.store.dispatch(SearchUserJobsActions.expandAllJob(job.id));
    this.masonries.last.layout();
  }

  shrinkCompatible(job: UserJob) {
    this.store.dispatch(SearchUserJobsActions.shrinkCompatibleJob(job.id));
    this.masonries.first.layout();
  }

  shrinkAll(job: UserJob) {
    this.store.dispatch(SearchUserJobsActions.shrinkAllJob(job.id));
    this.masonries.last.layout();
  }

  apply(job: UserJob) {
    this.store.dispatch(SearchUserJobsActions.applyForJob(job.id));
  }

  isApplying(job): boolean {
    return this.state.applyingForJobs.indexOf(job.id) !== -1;
  }

  isCancelling(job): boolean {
    return this.state.cancellingApplicationsForJobs.indexOf(job.id) !== -1;
  }

  isExpandedCompatible(job): boolean {
    return this.state.expandedCompatibleJob === job.id;
  }

  isExpandedAll(job): boolean {
    return this.state.expandedAllJob === job.id;
  }

  cancelApplication(job) {
    this.store.dispatch(SearchUserJobsActions.cancelApplication(job.id));
  }

  isInSelection(job: UserJob): boolean {
    const selections: Array<UserSelection> =
      this.entitiesService.getEntities('userSelections', job.selections as Array<string>);
    const found = selections
        .find((selection: UserSelection) => {
          if (selection.user === this.userId) {
            return true;
          }
          return false;
        })
      ;
    return !!found;
  }

  getJobId(index, job) {
    return job.id;
  }

  onScroll() {
    if (this.currentTab === 0) {
      const pagesLoaded = this.state.compatiblePagesLoaded;
      const pagesTotal = this.state.compatiblePagesTotal;
      if (pagesLoaded.length < pagesTotal) {
        this.store.dispatch(SearchUserJobsActions.searchCompatible({page: pagesLoaded.length + 1}));
      }
    } else {
      const pagesLoaded = this.state.allPagesLoaded;
      const pagesTotal = this.state.allPagesTotal;
      if (pagesLoaded.length < pagesTotal) {
        this.store.dispatch(SearchUserJobsActions.searchUserJobs({page: pagesLoaded.length + 1}));
      }
    }

  }

}
