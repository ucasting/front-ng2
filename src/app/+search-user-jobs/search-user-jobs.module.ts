import {NgModule} from '@angular/core';
import {SearchUserJobsRoutes} from './search-user-jobs-routes';
import {CommonModule} from '@angular/common';
import {UserJobCardComponent} from './components/user-job-card/user-job-card.component';
import {SearchUserJobsComponent} from './search-user-jobs.component';
import {SharedModule} from '../shared.module';

@NgModule({
  declarations: [
    SearchUserJobsComponent,
    UserJobCardComponent
  ],
  imports: [
    CommonModule,
    SearchUserJobsRoutes,
    SharedModule,
    // EffectsModule.run(SearchUserJobsEffects),
  ]
})
export class SearchUserJobsModule {
}
