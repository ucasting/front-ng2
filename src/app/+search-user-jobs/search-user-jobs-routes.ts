import {RouterModule, Routes} from '@angular/router';
import {SearchUserJobsComponent} from './search-user-jobs.component';

export const searchUserJobsRoutes: Routes = [
  {path: '', component: SearchUserJobsComponent, pathMatch: 'full'}
];


export const SearchUserJobsRoutes = RouterModule.forChild(searchUserJobsRoutes);
