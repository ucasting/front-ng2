import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {Store} from '@ngrx/store';
import {go} from '@ngrx/router-store';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthenticationService, private store: Store<any>) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    if (this.authService.hasValidToken()) {
      return Observable.of(true);
    }
    this.store.dispatch(go(['/security/login']));
    return Observable.of(false);
  }
}
