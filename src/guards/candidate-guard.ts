import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {ROLE_CANDIDATE} from '../models/roles.constants';
import {AccountService} from '../services/account.service';
import {NotificationsService} from '../services/notifications.service';

@Injectable()
export class CandidateGuard implements CanActivate {
  constructor(private accountService: AccountService, private notificationsService: NotificationsService) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    if (this.accountService.hasRole(ROLE_CANDIDATE)) {
      return Observable.of(true);
    }
    this.notificationsService.addWarning('Acesso negado', 'Você não tem permissão para acessar essa página.');
    return Observable.of(false);
  }
}
