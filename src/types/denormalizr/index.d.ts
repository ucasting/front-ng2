declare module 'denormalizr' {
  export function denormalize(entity, entities, entitySchema): any;
}

