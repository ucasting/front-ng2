import {Media} from './media.model';

export interface Company {
  id?: string|number;
  name: string;
  logo: Media;
}
