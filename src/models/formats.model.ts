export interface Formats {
  small: string;
  medium: string;
  big: string;
  hd?: string;
}
