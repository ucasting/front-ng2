export interface Hashtag {
    id: string|number;
    name: string;
}
