import {SelectionStatus} from './selection-status.model';
export interface Selection {
  id: string|number;
  status: any|SelectionStatus;
  appliedAt: any;
  invitedAt: any;
  rejectedAt: any;
  selectedAt: any;
  approvedAt: any;
}
