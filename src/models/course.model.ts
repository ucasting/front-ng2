export interface Course {
    id: string|number;
    name: string;
    entity: string;
    start: any;
    end: any;
}
