import {FormErrors} from './form-errors.interface';

export interface FormErrorResponse {
  code: number;
  errors: FormErrors;
  message: string;
}
