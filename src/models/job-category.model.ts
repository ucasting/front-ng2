export interface JobCategory {
    id: string|number;
    name: string;
}
