import {GalleryMedia} from './gallery-media.model';
export class Gallery {
  id: string|number;
  name: string;
  main: boolean;
  medias: Array<any|GalleryMedia>;
}
