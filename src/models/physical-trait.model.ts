export interface PhysicalTrait {
    id: string|number;
    name: string;
}
