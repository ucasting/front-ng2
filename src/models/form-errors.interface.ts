export interface FormErrors {
  errors?: string[];
  children?: Fields;
}

export interface Fields {
  [fieldName: string]: FormErrors;
}
