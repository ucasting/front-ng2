export interface ApiError {
  message: string;
  statusCode: number;
  errorKey: string;
}
