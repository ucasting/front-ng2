export interface PaginatedResponse {
  ids?: Array<number>;
  entities?: any[];
  items: any[];
  currentPage: number;
  maxPerPage: number;
  total: number;
  pageItemsCount: number;
  pages: number;
}
