export interface Benefit {
    id: string|number;
    name: string;
}
