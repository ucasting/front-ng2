import {User} from './user.model';
import {UserJob} from './user-job.model';
import {Selection} from './selection.model'
export interface UserSelection extends Selection {
  id: string|number;
  user: number|User;
  userJob: number|UserJob;
}
