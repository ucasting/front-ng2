import {Formats} from './formats.model';
export interface Media {
  id: string|number;
  name: string;
  formats: Formats;
}
