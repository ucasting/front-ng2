import {Idiom} from './idiom.model';
export interface ProfileIdiom {
    id: string|number;
    idiom: any|Idiom;
    proficiency: any;
}
