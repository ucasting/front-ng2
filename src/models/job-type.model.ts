import {JobCategory} from './job-category.model';
export interface JobType {
    id: string|number;
    name: string;
    category: any|JobCategory;
    color: string;
}
