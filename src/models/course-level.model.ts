export interface CourseLevel {
    id: string|number;
    name: string;
}
