export interface ProjectLocation {
    id: string|number;
    name: string;
    address: string;
    district: string;
    state: string;
    city: string;
    zipcode: string;
    latitude: number;
    longitude: number;
}
