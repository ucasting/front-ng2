export interface Experience {
  id: string|number;
  name: string;
  description: string;
  company: string;
  type: any;
  start: any;
}
