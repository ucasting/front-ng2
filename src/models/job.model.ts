import {JobType} from './job-type.model';
import {User} from './user.model';
import {Company} from './company.model';
import {Project} from './project.model';
export interface Job {
    id?: string|number;
    name: string;
    description: string;
    observations: string;
    requirements: string;
    jobType: any|JobType;
    start: string|Date;
    end: string|Date;
    user: User;
    company: Company;
    project?: any|Project;
    published: boolean;
    cancelled: boolean;
    approved: boolean;
    createdAt: any;
    publishedAt: any;
    cancelledAt: boolean;
    approvedAt: boolean;
}
