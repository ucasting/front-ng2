import {Group} from './group.model';
import {Media} from './media.model';
import {Profile} from './profile.model';
export interface User {
  id?: string|number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  rg: string;
  cpf: string;
  phone: string;
  cellphone: string;
  occupation: string;
  slug: string;
  gender: string;
  avatar: any|Media;
  cover: any;
  group: any|Group;
  profile: any|Profile;
  roles?: string[];
}
