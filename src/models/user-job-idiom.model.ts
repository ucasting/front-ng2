import {Idiom} from './idiom.model';
export interface UserJobIdiom {
    id: string|number;
    idiom: any|Idiom;
    proficiency: any;
}
