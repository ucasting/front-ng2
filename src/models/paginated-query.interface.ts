export interface PaginatedQuery {
  page?: number;
  itemsPerPage?: number;
}
