export class Gender {
  id: string|number;
  name: string;
  maleFeatures: boolean;
  femaleFeatures: boolean;
}
