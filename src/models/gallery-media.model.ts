import {Media} from './media.model';
export interface GalleryMedia {
  id: string|number;
  position: number;
  media: any|Media;
}
