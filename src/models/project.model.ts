import {User} from './user.model';
import {Company} from './company.model';
import {UserJob} from './user-job.model';
import {CompanyJob} from './company-job.model';
import {ProjectLocation} from './project-location.model';
import {JobCategory} from './job-category.model';
export interface Project {
    id?: string|number;
    name: string;
    description: string;
    clientName: string;
    budget: number;
    totalPositions: number;
    user: User;
    company: Company;
    jobCategory: JobCategory;
    locations: Array<string|number|ProjectLocation>;
    userJobs: Array<string|number|UserJob>;
    companyJobs: Array<string|number|CompanyJob>;
}
