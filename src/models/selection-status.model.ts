export interface SelectionStatus {
    id: string|number;
    name: string;
}
