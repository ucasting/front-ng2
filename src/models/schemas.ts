import {Schema, arrayOf} from 'normalizr';
import {JobType} from './job-type.model';
import {JobCategory} from './job-category.model';
import {User} from './user.model';
import {Profile} from './profile.model';
import {Particularity} from './particularity.model';
import {PhysicalTrait} from './physical-trait.model';
import {Idiom} from './idiom.model';
import {ProfileIdiom} from './profile-idiom.model';
import {Gallery} from './gallery.model';
import {GalleryMedia} from './gallery-media.model';
import {HairLength} from './hair-length.model';
import {Media} from './media.model';
import {Skin} from './skin.model';
import {HairColor} from './hair-color.model';
import {HairType} from './hair-type.model';
import {Company} from './company.model';
import {SelectionStatus} from './selection-status.model';
import {RemunerationType} from './remuneration-type.model';
import {Benefit} from './benefit.model';
import {CourseLevel} from './course-level.model';
import {UserJob} from './user-job.model';
import {UserJobIdiom} from './user-job-idiom.model';
import {UserJobCourse} from './user-job-course.model';
import {CompanyJob} from './company-job.model';
import {Project} from './project.model';
import {ProjectLocation} from './project-location.model';

// jobs
export const jobType: Schema = new Schema('jobTypes');
export const jobCategory: Schema = new Schema('jobCategories');
export const project: Schema = new Schema('projects');
export const projectLocation: Schema = new Schema('projectLocations');
export const userJob: Schema = new Schema('userJobs');
export const companyJob: Schema = new Schema('companyJobs');
export const selectionStatus: Schema = new Schema('selectionStatuses');
export const userSelection: Schema = new Schema('userSelections');
export const companySelection: Schema = new Schema('companySelections');
export const userJobIdiom: Schema = new Schema('userJobIdioms');
export const userJobCourse: Schema = new Schema('userJobCourses');
export const remunerationType: Schema = new Schema('remunerationTypes');
export const benefit: Schema = new Schema('benefits');

// user and profile
export const user: Schema = new Schema('users');
export const company: Schema = new Schema('companies');
export const profile: Schema = new Schema('profiles');
export const particularity: Schema = new Schema('particularities');
export const physicalTrait: Schema = new Schema('physicalTraits');
export const gallery: Schema = new Schema('galleries');
export const galleryMedia: Schema = new Schema('galleryMedias');
export const media: Schema = new Schema('medias');
export const idiom: Schema = new Schema('idioms');
export const hashtag: Schema = new Schema('hashtags');
export const profileIdiom: Schema = new Schema('profileIdioms');
export const hairType: Schema = new Schema('hairTypes');
export const hairLength: Schema = new Schema('hairLengths');
export const hairColor: Schema = new Schema('hairColors');
export const skin: Schema = new Schema('skins');
export const eyeColor: Schema = new Schema('eyeColors');
export const gender: Schema = new Schema('genders');
export const proficiency: Schema = new Schema('proficiencies');
export const courseLevel: Schema = new Schema('courseLevels');
// just for normalization purposes
export const option: Schema = new Schema('options');


export interface NormalizedResponse {
  result: number|number[];
  entities?: {
    jobTypes?: JobType[],
    jobCategories?: JobCategory[],
    users?: User[],
    profiles?: Profile[],
    particularities?: Particularity[],
    physicalTraits?: PhysicalTrait[],
    idioms?: Idiom[],
    profileIdioms?: ProfileIdiom[],
    galleries?: Gallery[],
    galleryMedias?: GalleryMedia[],
    medias?: Media[],
    skins?: Skin[],
    hairLengths?: HairLength[],
    hairColors?: HairColor[],
    hairTypes?: HairType[],
    companies?: Company[],
    selectionStatuses?: SelectionStatus[],
    remunerationTypes?: RemunerationType[],
    benefits?: Benefit[],
    courseLevels?: CourseLevel[],
    userJobs?: UserJob[],
    userJobIdioms?: UserJobIdiom[],
    userJobCourses?: UserJobCourse[],
    companyJobs?: CompanyJob[],
    projects?: Project[],
    projectLocations?: ProjectLocation[],
    // courses?: Course[],
    // experiences?: Experience[],
  };
}

company.define({
  users: arrayOf(user),
  userJobs: arrayOf(userJob),
  companyJobs: arrayOf(companyJob),
  logo: media
});

user.define({
  profile: profile,
  gender: gender,
  avatar: media,
  cover: media,
  company: company
});

jobType.define({
  category: jobCategory
});

galleryMedia.define({
  media: media
});

gallery.define({
  medias: arrayOf(galleryMedia)
});

profileIdiom.define({
  idiom: idiom,
  proficiency: proficiency
});

userJobIdiom.define({
  idiom: idiom,
  proficiency: proficiency
});

userJobCourse.define({
  level: courseLevel
});

profile.define({
  typeInterests: arrayOf(jobType),
  particularities: arrayOf(particularity),
  physicalTraits: arrayOf(physicalTrait),
  hashtags: arrayOf(hashtag),
  hairType: hairType,
  hairLength: hairLength,
  hairColor: hairColor,
  skin: skin,
  eyeColor: eyeColor,
  idioms: arrayOf(profileIdiom),
  albums: arrayOf(gallery),
  videos: gallery,
  gender: gender,
  // courses: arrayOf(course),
  // experiences: arrayOf(experience),
});

option.define({
  jobTypes: arrayOf(jobType),
  jobCategories: arrayOf(jobCategory),
  particularities: arrayOf(particularity),
  physicalTraits: arrayOf(physicalTrait),
  idioms: arrayOf(idiom),
  proficiencies: arrayOf(proficiency),
  hairTypes: arrayOf(hairType),
  hairLengths: arrayOf(hairLength),
  hairColors: arrayOf(hairColor),
  skins: arrayOf(skin),
  eyeColors: arrayOf(eyeColor),
  genders: arrayOf(gender),
  hashtags: arrayOf(hashtag),
  selectionStatuses: arrayOf(selectionStatus),
  remunerationTypes: arrayOf(remunerationType),
  benefits: arrayOf(benefit),
  courseLevels: arrayOf(courseLevel),
});

userJob.define({
  user: user,
  company: company,
  jobType: jobType,
  remunerationType: remunerationType,
  location: projectLocation,
  project: project,
  courses: arrayOf(userJobCourse),
  idioms: arrayOf(userJobIdiom),
  eyeColors: arrayOf(eyeColor),
  genders: arrayOf(gender),
  skins: arrayOf(skin),
  hairColors: arrayOf(hairColor),
  hairLengths: arrayOf(hairLength),
  hairTypes: arrayOf(hairType),
  benefits: arrayOf(benefit),
  particularities: arrayOf(particularity),
  physicalTraits: arrayOf(physicalTrait),
  selections: arrayOf(userSelection)
});

companyJob.define({
  user: user,
  company: company,
  location: projectLocation,
  selections: arrayOf(companySelection)
});

project.define({
  user: user,
  company: company,
  jobCategory: jobCategory,
  userJobs: arrayOf(userJob),
  companyJobs: arrayOf(companyJob),
  locations: arrayOf(projectLocation)
});

userSelection.define({
  status: selectionStatus,
  user: user,
  userJob: userJob
});

companySelection.define({
  status: selectionStatus,
  company: company,
  companyJob: companyJob
});



