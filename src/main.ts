import './polyfills.ts';
// imports rxjs operators
import 'rxjs/add/operator/bufferTime';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/publish';
import 'rxjs/add/operator/publishLast';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/throttleTime';
import '@ngrx/core/add/operator/select';

import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {environment} from './environments/environment';
import {UcastingAppModule} from './app/ucasting-app.module';
// import {Ng2BootstrapConfig, Ng2BootstrapTheme} from 'ng2-bootstrap';


// Ng2BootstrapConfig.theme = Ng2BootstrapTheme.BS4;

if (environment.production) {
  enableProdMode();
}

// platformBrowserDynamic().bootstrapModule(UcastingAppModule);
platformBrowserDynamic().bootstrapModule(UcastingAppModule);
