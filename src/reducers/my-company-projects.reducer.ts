import {Action} from '@ngrx/store';
import {ProjectsActions} from '../actions/projects-actions';
import {NormalizedResponse} from '../models/schemas';

export interface MyCompanyProjectsState {
  projectsLoaded: boolean;
  loadingProjects: boolean;
  loadingCompanyJobs: boolean;
  savingProject: boolean;
  projectId: number;
  projectError: string;
  projectValidationErrors: any;
  myCompanyProjects: string[];
  myCompanyCompanyJobs: string[];
  myCompanyUserJobs: string[];
  openUserJobForm: boolean;
  editingUserJobId: number;
  publishingUserJobId: number;
  userJobError: any;
  userJobValidationErrors: any;
  savingUserJob: boolean;
}

const initialProjectsState: MyCompanyProjectsState = {
  projectsLoaded: false,
  loadingProjects: false,
  loadingCompanyJobs: false,
  savingProject: false,
  projectId: null,
  projectError: null,
  projectValidationErrors: {},
  myCompanyProjects: [],
  myCompanyCompanyJobs: [],
  myCompanyUserJobs: [],
  openUserJobForm: false,
  editingUserJobId: null,
  publishingUserJobId: null,
  userJobError: null,
  userJobValidationErrors: {},
  savingUserJob: false,
};

export function myCompanyProjectsReducer(state: MyCompanyProjectsState = initialProjectsState,
                                         action: Action): MyCompanyProjectsState {
  switch (action.type) {
    case ProjectsActions.LOAD_MY_COMPANY_PROJECTS: {
      return Object.assign({}, state, {loadingProjects: true});
    }
    case ProjectsActions.LOAD_MY_COMPANY_PROJECTS_SUCCESS: {
      return Object.assign({}, state,
        {
          projectsLoaded: true,
          loadingProjects: false,
          myCompanyProjects: (action.payload as NormalizedResponse).result
        }
      );
    }
    case ProjectsActions.LOAD_MY_COMPANY_PROJECTS_FAIL: {
      return Object.assign({}, state, {loadingProjects: false});
    }
    case ProjectsActions.REFRESH_FORM: {
      return Object.assign({}, state, {
        projectId: null,
        projectError: null,
        projectValidationErrors: {}
      });
    }
    case ProjectsActions.REFRESH_USERJOB_FORM: {
      return Object.assign({}, state, {
        openUserJobForm: false,
        editingUserJobId: null,
        userJobError: null,
        userJobValidationErrors: {}
      });
    }
    case ProjectsActions.CREATE_PROJECT:
      return Object.assign({}, state, {
        projectId: null,
        projectError: null,
        projectValidationErrors: null,
        savingProject: true
      });
    case ProjectsActions.CREATE_PROJECT_SUCCESS: {
      return Object.assign({}, state, {
        projectId: action.payload.result,
        savingProject: false,
        // pushes the new job into the companies's jobs
        myCompanyProjects: [...state.myCompanyProjects, action.payload.result]
      });
    }
    case ProjectsActions.CREATE_PROJECT_FAIL:
      return Object.assign({}, state, {
        projectError: action.payload.message,
        projectValidationErrors: action.payload.errors,
        savingProject: false
      });
    case ProjectsActions.SAVE_PROJECT:
      return Object.assign({}, state, {
        projectError: null,
        projectValidationErrors: null,
        savingProject: true
      });
    case ProjectsActions.SAVE_PROJECT_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        projectId: action.payload.result,
        savingProject: false,
      });
    case ProjectsActions.SAVE_PROJECT_FAIL:
      return Object.assign({}, state, {
        projectError: action.payload.message,
        projectValidationErrors: action.payload.errors,
        savingProject: false
      });
    case ProjectsActions.NEW_USERJOB_FORM:
      return Object.assign({}, state, {openUserJobForm: true, editingUserJobId: null, });
    case ProjectsActions.CLOSE_USERJOB_FORM:
      return Object.assign({}, state, {openUserJobForm: false, editingUserJobId: null});
    case ProjectsActions.CREATE_USERJOB:
      return Object.assign({}, state, {
        editingUserJobId: null,
        userJobError: null,
        userJobValidationErrors: null,
        savingUserJob: true
      });
    case ProjectsActions.CREATE_USERJOB_SUCCESS: {
      return Object.assign({}, state, {
        editingUserJobId: action.payload.result,
        savingUserJob: false,
        // pushes the new job into the companies's jobs
        myCompanyUserJobs: [...state.myCompanyUserJobs, action.payload.result]
      });
    }
    case ProjectsActions.CREATE_USERJOB_FAIL:
      return Object.assign({}, state, {
        userJobError: action.payload.message,
        userJobValidationErrors: action.payload.errors,
        savingUserJob: false
      });
    case ProjectsActions.SAVE_USERJOB:
      return Object.assign({}, state, {
        userJobError: null,
        userJobValidationErrors: null,
        savingUserJob: true
      });
    case ProjectsActions.SAVE_USERJOB_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        userJobId: action.payload.result,
        savingUserJob: false,
      });
    case ProjectsActions.SAVE_USERJOB_FAIL:
      return Object.assign({}, state, {
        userJobError: action.payload.message,
        userJobValidationErrors: action.payload.errors,
        savingUserJob: false
      });
    case ProjectsActions.EDIT_USERJOB:
      return Object.assign({}, state, {
        openUserJobForm: true,
        editingUserJobId: action.payload.id,
        userJobError: null,
        userJobValidationErrors: null,
        savingUserJob: false
      });
    case ProjectsActions.PUBLISH_USERJOB:
      return Object.assign({}, state, {
        publishingUserJobId: action.payload
      });
    case ProjectsActions.PUBLISH_USERJOB_SUCCESS:
      return Object.assign({}, state, {
        publishingUserJobId: null
      });
    case ProjectsActions.PUBLISH_USERJOB_FAIL:
      return Object.assign({}, state, {
        publishingUserJobId: null
      });
    case ProjectsActions.CANCEL_USERJOB:
      return Object.assign({}, state, {
        cancellingUserJobId: action.payload
      });
    case ProjectsActions.CANCEL_USERJOB_SUCCESS:
      return Object.assign({}, state, {
        cancellingUserJobId: null
      });
    case ProjectsActions.CANCEL_USERJOB_FAIL:
      return Object.assign({}, state, {
        cancellingUserJobId: null
      });
    default:
      return state;
  }
}
