import {Action} from '@ngrx/store';
import {AccountActions} from '../actions/account-actions';
import {AuthActions} from '../actions/auth-actions';
import {FormErrors} from '../models/form-errors.interface';

export interface AccountState {
  user: string;
  updating: boolean;
  updatingAvatar: boolean;
  updatingCover: boolean;
  error: string;
  saved: boolean;
  validationErrors: FormErrors;
  avatarErrors: any;
  coverErrors: any;
}
export const initialAccountState: AccountState = {
  user: null,
  updating: false,
  updatingAvatar: false,
  updatingCover: false,
  error: null,
  saved: null,
  avatarErrors: null,
  coverErrors: null,
  validationErrors: null,
};

export function accountReducer(state: AccountState = initialAccountState, action: Action): AccountState {
  switch (action.type) {
    case AuthActions.LOGIN_SUCCESS:
      return Object.assign({}, state, {user: action.payload.userId});
    case AccountActions.EDIT_ACCOUNT:
      return Object.assign({}, state, {error: null, validationErrors: null, updating: true, saved: false});
    case AccountActions.EDIT_ACCOUNT_SUCCESS:
      return Object.assign({}, state, {error: null, validationErrors: null, updating: false, saved: true});
    case AccountActions.EDIT_ACCOUNT_FAIL:
      return Object.assign({}, state, {
        error: action.payload.message,
        validationErrors: action.payload.errors,
        updating: false,
        saved: false
      });
    case AccountActions.EDIT_AVATAR:
      return Object.assign({}, state, {updatingAvatar: true});
    case AccountActions.EDIT_COVER:
      return Object.assign({}, state, {updatingCover: true});
    case AccountActions.EDIT_AVATAR_SUCCESS:
      return Object.assign({}, state, {updatingAvatar: false});
    case AccountActions.EDIT_COVER_SUCCESS:
      return Object.assign({}, state, {updatingCover: false});
    case AccountActions.EDIT_AVATAR_FAIL:
      return Object.assign({}, state, {updatingAvatar: false});
    case AccountActions.EDIT_COVER_FAIL:
      return Object.assign({}, state, {updatingCover: false});
    default:
      return state;
  }
}
