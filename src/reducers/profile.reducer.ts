import {Action} from '@ngrx/store';
import {AuthActions} from '../actions/auth-actions';
import {ProfileActions} from '../actions/profile-actions';
import {EntitiesState} from './entities.reducer';
import {Observable} from 'rxjs/Rx';

export interface ProfileState {
  profile: string;
  addingAlbum: boolean;
  editingAlbum: boolean;
  removingAlbum: boolean;
  addingAlbumImage: boolean;
  editingAlbumImage: boolean;
  removingAlbumImage: boolean;
  addingVideo: boolean;
  editingVideo: boolean;
  removingVideo: boolean;
}

export const initialState: ProfileState = {
  profile: null,
  addingAlbum: false,
  editingAlbum: false,
  removingAlbum: false,
  addingAlbumImage: false,
  editingAlbumImage: false,
  removingAlbumImage: false,
  addingVideo: false,
  editingVideo: false,
  removingVideo: false,
};

export function profileReducer(state: ProfileState = initialState, action: Action): ProfileState {
  switch (action.type) {
    case AuthActions.LOGIN_SUCCESS:
      return Object.assign({}, state, {profile: action.payload.profileId});
    case ProfileActions.ADD_ALBUM:
      return Object.assign({}, state, {addingAlbum: true});
    case ProfileActions.ADD_ALBUM_SUCCESS:
    case ProfileActions.ADD_ALBUM_FAIL:
      return Object.assign({}, state, {addingAlbum: false});
    default:
      return state;
  }
}

export function getProfileEntities() {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.profiles);
}

export function getProfile(id: string) {
  return (state$: Observable<EntitiesState>) => state$
    .select(s => s.entities.profiles[id]);
}

export function getProfiles(ids: string[]) {
  return (state$: Observable<EntitiesState>) => state$
    .let(getProfileEntities())
    .map(entities => ids.map(id => entities[id]));
}
