import {Action} from '@ngrx/store';
import {UserJobsActions} from '../actions/user-jobs-actions';

export interface MyCompanyJobsState {
  loadingUserJobs: boolean;
  loadingCompanyJobs: boolean;
  savingUserJob: boolean;
  userJobId: number;
  userJobError: string;
  userJobValidationErrors: any;
  myCompanyUserJobs: string[];
  myCompanyCompanyJobs: string[];
}

const initialJobsState: MyCompanyJobsState = {
  loadingUserJobs: false,
  loadingCompanyJobs: false,
  savingUserJob: false,
  userJobId: null,
  userJobError: null,
  userJobValidationErrors: {},
  myCompanyUserJobs: [],
  myCompanyCompanyJobs: [],
};

export function myCompanyJobsReducer(state: MyCompanyJobsState = initialJobsState, action: Action): MyCompanyJobsState {
  switch (action.type) {
    case UserJobsActions.REFRESH_FORM: {
      return Object.assign({}, state, {
        loadingUserJobs: false,
        savingUserJob: false,
        userJobId: null,
        userJobError: null,
        userJobValidationErrors: {}
      });
    }
    case UserJobsActions.CREATE_JOB:
      return Object.assign({}, state, {
        userJobId: null,
        userJobError: null,
        userJobValidationErrors: null,
        savingUserJob: true
      });
    case UserJobsActions.CREATE_JOB_SUCCESS: {
      console.log(action.payload);
      return Object.assign({}, state, {
        userJobId: action.payload.result,
        savingUserJob: false,
        // pushes the new job into the companies's jobs
        myCompanyUserJobs: [...state.myCompanyUserJobs, action.payload.result]
      });
    }
    case UserJobsActions.CREATE_JOB_FAIL:
      return Object.assign({}, state, {
        userJobError: action.payload.message,
        userJobValidationErrors: action.payload.errors,
        savingUserJob: false
      });
    case UserJobsActions.SAVE_JOB:
      return Object.assign({}, state, {
        userJobError: null,
        userJobValidationErrors: null,
        savingUserJob: true
      });
    case UserJobsActions.SAVE_JOB_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        userJobId: action.payload.result,
        savingUserJob: false,
      });
    case UserJobsActions.SAVE_JOB_FAIL:
      return Object.assign({}, state, {
        userJobError: action.payload.message,
        userJobValidationErrors: action.payload.errors,
        savingUserJob: false
      });
    default:
      return state;
  }
}
