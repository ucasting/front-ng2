import {Action} from '@ngrx/store';
import {UsersActions} from '../actions/users-actions';
import {PaginatedResponse} from '../models/paginated-response.interface';

export interface UsersState {
  loadingCandidates: boolean;
  candidatesFilters: any;
  candidatesPages: number;
  currentCandidatesPage: number;
  maxCandidatesPerPage: number;
  totalCandidates: number;
  candidatesIds: number[];
}

const initialUsersState: UsersState = {
  loadingCandidates: false,
  candidatesFilters: null,
  candidatesPages: 0,
  currentCandidatesPage: 1,
  maxCandidatesPerPage: 15,
  totalCandidates: 0,
  candidatesIds: [],
};

export function usersReducer(state: UsersState = initialUsersState, action: Action): UsersState {
  switch (action.type) {
    case UsersActions.SEARCH_CANDIDATES:
      return Object.assign({}, state, {
        loadingCandidates: true
      });
    case UsersActions.SEARCH_CANDIDATES_SUCCESS: {
      let payload: PaginatedResponse = action.payload;
      return Object.assign({}, state, {
        candidatesIds: payload.ids,
        candidatesPages: payload.pages,
        currentCandidatesPage: payload.currentPage,
        maxCandidatesPerPage: payload.maxPerPage,
        totalCandidates: payload.total,
        candidatesLoaded: true,
        loadingCandidates: false,
      });
    }
    case UsersActions.SEARCH_CANDIDATES_FAIL:
      return Object.assign({}, state, {
        loadingCandidates: false
      });
    case UsersActions.CHANGE_CANDIDATES_PAGE:
      return Object.assign({}, state, {
        currentCandidatesPage: action.payload
      });
    case UsersActions.CHANGE_CANDIDATES_FILTERS:
      return Object.assign({}, state, {
        candidatesFilters: action.payload,
        currentCandidatesPage: 1
      });
    default:
      return state;
  }
}
