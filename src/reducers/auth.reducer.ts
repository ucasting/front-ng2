import {Action} from '@ngrx/store';
import {AuthActions} from '../actions/auth-actions';

export interface AuthState {
  token: string;
  authChecking: boolean;
  hasError: boolean;
}

const initialAuthState: AuthState = {
  token: null,
  authChecking: false,
  hasError: false
};

export function authReducer(state: AuthState = initialAuthState, action: Action): AuthState {
  switch (action.type) {
    case AuthActions.LOGIN:
      return Object.assign({}, state, {authChecking: true, hasError: false});
    case AuthActions.LOGIN_SUCCESS:
      return {
        token: action.payload.token,
        authChecking: false,
        hasError: false
      };
    case AuthActions.LOGIN_FAIL:
      return {
        token: state.token,
        authChecking: false,
        hasError: true
      };
    case AuthActions.LOGOUT:
      return {
        token: null,
        authChecking: false,
        hasError: false
      };
    default: {
      return state;
    }
  }
}
