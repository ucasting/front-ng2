import {Action} from '@ngrx/store';
import {NotificationsActions} from '../actions/notifications-actions';
import {Notification} from '../models/notification.interface';

export type NotificationsState = Notification[];

const initialNotificationsState: NotificationsState = [];

export function notificationsReducer(state = initialNotificationsState, action: Action) {
  switch (action.type) {
    case NotificationsActions.ADD: {
      const notification: Notification = {
        id: action.payload.id,
        title: action.payload.title,
        message: action.payload.message,
        type: action.payload.type
      };
      return [...state, notification];
    }
    case NotificationsActions.REMOVE: {
      return state.filter((notification: Notification) => notification.id !== action.payload.id);
    }
    default: {
      return state;
    }
  }
}
