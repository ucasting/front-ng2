import {Action} from '@ngrx/store';
import {OptionsActions} from '../actions/options-actions';

export interface OptionsState {
  loading: boolean;
  loaded: boolean;
}

const initialOptionsState: OptionsState = {
  loading: false,
  loaded: false,
};

export function optionsReducer(state: OptionsState = initialOptionsState, action: Action): OptionsState {
  switch (action.type) {
    case OptionsActions.LOAD:
      return Object.assign({}, state, {loading: true});
    case OptionsActions.LOAD_SUCCESS:
      return {loading: false, loaded: true};
    case OptionsActions.LOAD_FAIL:
      return Object.assign({}, state, {loading: false});
    default:
      return state;
  }
}
