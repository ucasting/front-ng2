import {Action} from '@ngrx/store';
import {PageActions} from '../actions/page-actions';

export interface PageState {
  title: string;
  toggledNavigation: boolean;
}

const initialPageState: PageState = {
  title: null,
  toggledNavigation: false
};

export function pageReducer(state: PageState = initialPageState, action: Action): PageState {
  switch (action.type) {
    case PageActions.CHANGE_TITLE:
      return Object.assign({}, state, {title: action.payload});
    case PageActions.TOGGLE_NAVIGATION:
      return Object.assign({}, state, {toggledNavigation: !state.toggledNavigation});
    default:
      return state;
  }
}
