import * as _ from 'lodash';
import {Observable} from 'rxjs';

export interface EntitiesEntries {
  users: any;
  profiles: any;
  physicalTraits: any;
  particularities: any;
  eyeColors: any;
  hairTypes: any;
  hairLengths: any;
  hairColors: any;
  skins: any;
  medias: any;
  galleries: any;
  galleryMedias: any;
  genders: any;
  jobTypes: any;
  jobCategories: any;
  idioms: any;
  groups: any;
  courses: any;
  experiences: any;
  hashtags: any;
  profileIdioms: any;
  proficiencies: any;
  userJobs: any;
  userJobIdioms: any;
  userJobCourses: any;
  companyJobs: any;
  selectionStatuses: any;
  remunerationTypes: any;
  benefits: any;
  courseLevels: any;
  projects: any;
  projectLocations: any;
}

export interface EntitiesIds {
  users: string[];
  profiles: string[];
  physicalTraits: string[];
  particularities: string[];
  eyeColors: string[];
  hairTypes: string[];
  hairLengths: string[];
  hairColors: string[];
  skins: string[];
  medias: string[];
  galleries: string[];
  galleryMedias: string[];
  genders: string[];
  jobTypes: string[];
  jobCategories: string[];
  idioms: string[];
  groups: string[];
  courses: string[];
  experiences: string[];
  hashtags: string[];
  profileIdioms: string[];
  proficiencies: string[];
  userJobs: string[];
  userJobIdioms: string[];
  userJobCourses: string[];
  companyJobs: string[];
  selectionStatuses: string[];
  remunerationTypes: string[];
  benefits: string[];
  courseLevels: string[];
  projects: string[];
  projectLocations: string[];
}

export interface EntitiesState {
  entities: EntitiesEntries;
  ids: EntitiesIds;
}

export const initialEntityCacheState: EntitiesState = {
  entities: {
    users: {},
    profiles: {},
    physicalTraits: {},
    particularities: {},
    eyeColors: {},
    hairTypes: {},
    hairLengths: {},
    hairColors: {},
    skins: {},
    jobTypes: {},
    jobCategories: {},
    galleries: {},
    galleryMedias: {},
    genders: {},
    medias: {},
    idioms: {},
    groups: {},
    courses: {},
    experiences: {},
    hashtags: {},
    profileIdioms: {},
    proficiencies: {},
    userJobs: {},
    userJobIdioms: {},
    userJobCourses: {},
    companyJobs: {},
    selectionStatuses: {},
    remunerationTypes: {},
    benefits: {},
    courseLevels: {},
    projects: {},
    projectLocations: {},
  },
  ids: {
    users: [],
    profiles: [],
    physicalTraits: [],
    particularities: [],
    eyeColors: [],
    hairTypes: [],
    hairLengths: [],
    hairColors: [],
    skins: [],
    jobTypes: [],
    jobCategories: [],
    galleries: [],
    galleryMedias: [],
    genders: [],
    medias: [],
    idioms: [],
    groups: [],
    courses: [],
    experiences: [],
    hashtags: [],
    profileIdioms: [],
    proficiencies: [],
    userJobs: [],
    userJobIdioms: [],
    userJobCourses: [],
    companyJobs: [],
    selectionStatuses: [],
    remunerationTypes: [],
    benefits: [],
    courseLevels: [],
    projects: [],
    projectLocations: [],
  }

};
// This reducer is responsible of merging all the changes retrieved and normalized from the API.
// The entities stored here are the single source of truth for all entities managed in the app.
// All the deletions are handled here too
export function entitiesReducer(state = initialEntityCacheState, action) {
  if (action.payload && action.payload.entities) {
    // const entities = action.payload.entities;

    // let entityTypes = Object.keys(entities);
    // TODO merge IDs as well
    // merges all properties, and overwrites the array collections
    return _.mergeWith({}, state, {entities: action.payload.entities},
      (objValue, srcValue) => {
        if (_.isArray(srcValue)) {
          return srcValue;
        }
      });
  }
  return state;
}
//
// function cloneState() {
//   return newState;
// }

// selectors
export function getUser(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.users[id]);
}
export function getProfile(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.profiles[id]);
}
export function getPhysicalTrait(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.physicalTraits[id]);
}
export function getParticularity(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.particularities[id]);
}
export function getJobType(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.jobTypes[id]);
}
export function getJobCategory(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.jobCategories[id]);
}
export function getMedia(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.medias[id]);
}
export function getGalleryMedia(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.galleryMedias[id]);
}
export function getGallery(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.galleries[id]);
}
export function getIdiom(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.idioms[id]);
}
export function getGroup(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.groups[id]);
}
export function getCourse(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.courses[id]);
}
export function getExperience(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.experiences[id]);
}
export function getHashtag(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.hashtags[id]);
}

export function getProfileIdiom(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.profileIdioms[id]);
}

export function getSkin(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.skins[id]);
}

export function getHairLength(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.hairLengths[id]);
}

export function getHairType(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.hairTypes[id]);
}

export function getHairColor(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.hairColors[id]);
}

export function getEyeColor(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.eyeColors[id]);
}

export function getGender(id: string|number) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => s.entities.genders[id]);
}


export function getGalleries(ids: string[]) {
  return (state$: Observable<EntitiesState>) => state$
    .select((s: EntitiesState) => {
      ids.map((id) => s.entities.galleries[id]);
    });
}



