import {compose} from '@ngrx/core/compose';
import {storeLogger} from 'ngrx-store-logger';
import {storeFreeze} from 'ngrx-store-freeze';
import {combineReducers} from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import * as fromAccount from './account.reducer';
import * as fromOptions from './options.reducer';
import * as fromProfile from './profile.reducer';
import * as fromEntities from './entities.reducer';
import * as fromNotifications from './notifications.reducer';
import * as fromUsers from './users.reducer';
import * as fromPage from './page.reducer';
import * as fromMyCompanyJobs from './my-company-jobs.reducer';
import * as fromSearchUserJobs from '../app/+search-user-jobs/search-user-jobs.reducer';
import * as fromUserJobsSelection from '../app/+user-jobs-selection/user-jobs-selection.reducer';
import * as fromMyCompanyProjects from './my-company-projects.reducer';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';
import {routerReducer, RouterState} from '@ngrx/router-store';


/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */

/**
 * storeLogger is a powerful metareducer that logs out each time we dispatch
 * an action.
 *
 * A metareducer wraps a reducer function and returns a new reducer function
 * with superpowers. They are handy for all sorts of tasks, including
 * logging, undo/redo, and more.
 */

/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */

/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 *
 * More: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */


/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */


/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface AppState {
  router: RouterState;
  auth: fromAuth.AuthState;
  account: fromAccount.AccountState;
  options: fromOptions.OptionsState;
  profile: fromProfile.ProfileState;
  entities: fromEntities.EntitiesState;
  notifications: fromNotifications.NotificationsState;
  users: fromUsers.UsersState;
  page: fromPage.PageState;
  myCompanyJobs: fromMyCompanyJobs.MyCompanyJobsState;
  myCompanyProjects: fromMyCompanyProjects.MyCompanyProjectsState;
  searchUserJobs: fromSearchUserJobs.SearchUserJobsState;
  userJobsSelection: fromUserJobsSelection.UserJobsSelectionState;
}

export const debugStore = () => {
  if (!environment.production) {
    return compose(storeFreeze, storeLogger());
  }
  return (reducer) => {
    return (state, action) => {
      return reducer(state, action);
    };
  };
};


/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */

export function rootReducer(state, reducers) {
  return compose(debugStore(), combineReducers)({
    router: routerReducer,
    auth: fromAuth.authReducer,
    account: fromAccount.accountReducer,
    options: fromOptions.optionsReducer,
    profile: fromProfile.profileReducer,
    entities: fromEntities.entitiesReducer,
    notifications: fromNotifications.notificationsReducer,
    users: fromUsers.usersReducer,
    page: fromPage.pageReducer,
    myCompanyJobs: fromMyCompanyJobs.myCompanyJobsReducer,
    myCompanyProjects: fromMyCompanyProjects.myCompanyProjectsReducer,
    searchUserJobs: fromSearchUserJobs.searchUserJobsReducer,
    userJobsSelection: fromUserJobsSelection.userJobsSelectionReducer,
  })(state, reducers);
}

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `books` state.
 *
 * Selectors are used with the `let` operator. They take an input observable
 * and return a new observable. Here's how you would use this selector:
 *
 * ```ts
 * class MyComponent {
 * 	constructor(state$: Observable<AppState>) {
 * 	  this.booksState$ = state$.let(getBooksState());
 * 	}
 * }
 * ```
 */
export function getAccountState() {
  return (state$: Observable<AppState>) => state$
    .select((s: AppState) => s.account);
}

export function getOptionsState() {
  return (state$: Observable<AppState>) => state$
    .select((s: AppState) => s.options);
}

export function getProfileState() {
  return (state$: Observable<AppState>) => state$
    .select((s: AppState) => s.profile);
}

export function getAuthState() {
  return (state$: Observable<AppState>) => state$
    .select((s: AppState) => s.auth);
}
export function getEntitiesState() {
  return (state$: Observable<AppState>) => state$
    .select((s: AppState) => s.entities);
}

export function getUser(id: string|number) {
  return compose(fromEntities.getUser(id), getEntitiesState());
}
