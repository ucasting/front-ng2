import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class PageActions {
  static readonly CHANGE_TITLE = '[Page] Change title';
  static readonly TOGGLE_NAVIGATION = '[Page] Toggle navigation';

  changeTitle(payload: string): Action {
    return {
      type: PageActions.CHANGE_TITLE,
      payload: payload
    };
  }


  toggleNavigation(): Action {
    return {
      type: PageActions.TOGGLE_NAVIGATION,
      payload: null
    };
  }
}
