import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {NormalizedResponse} from '../models/schemas';

@Injectable()
export class ProfileActions {

  static readonly ADD_ALBUM = '[Profile] Add album start';
  static readonly ADD_ALBUM_SUCCESS = '[Profile] Add album success';
  static readonly ADD_ALBUM_FAIL = '[Profile] Add album fail';
  static readonly EDIT_ALBUM = '[Profile] Edit album start';
  static readonly EDIT_ALBUM_SUCCESS = '[Profile] Edit album success';
  static readonly EDIT_ALBUM_FAIL = '[Profile] Edit album fail';
  static readonly REMOVE_ALBUM = '[Profile] Remove album start';
  static readonly REMOVE_ALBUM_SUCCESS = '[Profile] Remove album success';
  static readonly REMOVE_ALBUM_FAIL = '[Profile] Remove album fail';
  static readonly ADD_ALBUM_IMAGE = '[Profile] Add album image start';
  static readonly ADD_ALBUM_IMAGE_SUCCESS = '[Profile] Add album image success';
  static readonly ADD_ALBUM_IMAGE_FAIL = '[Profile] Add album image fail';
  static readonly EDIT_ALBUM_IMAGE = '[Profile] Edit album image start';
  static readonly EDIT_ALBUM_IMAGE_SUCCESS = '[Profile] Edit album image success';
  static readonly EDIT_ALBUM_IMAGE_FAIL = '[Profile] Edit album image fail';
  static readonly REMOVE_ALBUM_IMAGE = '[Profile] Remove album image start';
  static readonly REMOVE_ALBUM_IMAGE_SUCCESS = '[Profile] Remove album image success';
  static readonly REMOVE_ALBUM_IMAGE_FAIL = '[Profile] Remove album image fail';


  addAlbum(): Action {
    return {
      type: ProfileActions.ADD_ALBUM,
      payload: null
    };
  }


  addAlbumSuccess(response: NormalizedResponse): Action {
    return {
      type: ProfileActions.ADD_ALBUM_SUCCESS,
      payload: response
    };
  }


  addAlbumFail(fail): Action {
    return {
      type: ProfileActions.ADD_ALBUM_FAIL,
      payload: fail
    };
  }


  editAlbum(id: string, formValues: any): Action {
    return {
      type: ProfileActions.EDIT_ALBUM,
      payload: {
        id: id,
        values: formValues
      }
    };
  }


  editAlbumSuccess(payload): Action {
    return {
      type: ProfileActions.EDIT_ALBUM_SUCCESS,
      payload: payload
    };
  }


  editAlbumFail(error): Action {
    return {
      type: ProfileActions.EDIT_ALBUM_FAIL,
      payload: error
    };
  }


  removeAlbum(id: string): Action {
    return {
      type: ProfileActions.REMOVE_ALBUM,
      payload: id
    };
  }


  removeAlbumSuccess(payload): Action {
    return {
      type: ProfileActions.REMOVE_ALBUM_SUCCESS,
      payload: payload
    };
  }


  removeAlbumFail(message: string): Action {
    return {
      type: ProfileActions.REMOVE_ALBUM_FAIL,
      payload: message
    };
  }


  addAlbumImage(id): Action {
    return {
      type: ProfileActions.ADD_ALBUM_IMAGE,
      payload: {
        id: id
      }
    };
  }


  addAlbumImageSuccess(normalizedResponse: any): Action {
    console.log(normalizedResponse);
    return {
      type: ProfileActions.ADD_ALBUM_IMAGE_SUCCESS,
      payload: normalizedResponse
    };
  }


  addAlbumImageFail(error): Action {
    return {
      type: ProfileActions.ADD_ALBUM_IMAGE_FAIL,
      payload: error
    };
  }


  removeAlbumImage(id): Action {
    return {
      type: ProfileActions.REMOVE_ALBUM_IMAGE,
      payload: {
        id: id
      }
    };
  }


  removeAlbumImageSuccess(payload): Action {
    console.log(payload);
    return {
      type: ProfileActions.REMOVE_ALBUM_IMAGE_SUCCESS,
      payload: payload
    };
  }


  removeAlbumImageFail(id): Action {
    return {
      type: ProfileActions.REMOVE_ALBUM_IMAGE_FAIL,
      payload: {
        id: id
      }
    };
  }
}
