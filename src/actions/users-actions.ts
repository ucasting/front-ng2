import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {PaginatedResponse} from '../models/paginated-response.interface';


@Injectable()
export class UsersActions {
  static readonly CHANGE_CANDIDATES_FILTERS = '[Users] Change candidates filters';
  static readonly SEARCH_CANDIDATES = '[Users] Search candidates start';
  static readonly SEARCH_CANDIDATES_SUCCESS = '[Users] Search candidates success';
  static readonly SEARCH_CANDIDATES_FAIL = '[Users] Search candidates fail';
  static readonly CHANGE_CANDIDATES_PAGE = '[Users] Change candidates page';

  changeCandidatesFilters(filters): Action {
    return {
      type: UsersActions.CHANGE_CANDIDATES_FILTERS,
      payload: filters
    };
  }

  searchCandidates(): Action {
    return {
      type: UsersActions.SEARCH_CANDIDATES,
      payload: null
    };
  }

  searchCandidatesSuccess(response: PaginatedResponse): Action {
    return {
      type: UsersActions.SEARCH_CANDIDATES_SUCCESS,
      payload: response
    };
  }

  searchCandidatesFail(response): Action {
    return {
      type: UsersActions.SEARCH_CANDIDATES_FAIL,
      payload: response
    };
  }

  changeCandidatesPage(pageNumber: number): Action {
    return {
      type: UsersActions.CHANGE_CANDIDATES_PAGE,
      payload: pageNumber
    };
  }
}
