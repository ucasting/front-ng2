import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {NormalizedResponse} from '../models/schemas';
import {ApiError} from '../models/api-error.interface';
import {FormErrorResponse} from '../models/form-error-response.interface';
import {UserJob} from '../models/user-job.model';

@Injectable()
export class ProjectsActions {
  static readonly LOAD_MY_COMPANY_PROJECTS = '[Projects] Load my company projects';
  static readonly LOAD_MY_COMPANY_PROJECTS_SUCCESS = '[Projects] Load my company projects success';
  static readonly LOAD_MY_COMPANY_PROJECTS_FAIL = '[Projects] Load my company projects fail';
  static readonly REFRESH_FORM = '[Projects] refresh project form';
  static readonly REFRESH_USERJOB_FORM = '[Projects] refresh userjob form';
  static readonly CREATE_PROJECT = '[Projects] Create project';
  static readonly CREATE_PROJECT_SUCCESS = '[Projects] Create project success';
  static readonly CREATE_PROJECT_FAIL = '[Projects] Create project failed';
  static readonly SAVE_PROJECT = '[Projects] Save project';
  static readonly SAVE_PROJECT_SUCCESS = '[Projects] Save project success';
  static readonly SAVE_PROJECT_FAIL = '[Projects] Save project failed';
  static readonly NEW_USERJOB_FORM = '[Projects] New UserJob Form';
  static readonly CLOSE_USERJOB_FORM = '[Projects] Close UserJob Form';
  static readonly CREATE_USERJOB = '[Projects] Create UserJob start';
  static readonly CREATE_USERJOB_SUCCESS = '[Projects] Create UserJob success';
  static readonly CREATE_USERJOB_FAIL = '[Projects] Create UserJob fail';
  static readonly SAVE_USERJOB = '[Projects] Save UserJob start';
  static readonly SAVE_USERJOB_SUCCESS = '[Projects] Save UserJob success';
  static readonly SAVE_USERJOB_FAIL = '[Projects] Save UserJob fail';
  static readonly PUBLISH_USERJOB = '[Projects] Publish UserJob start';
  static readonly PUBLISH_USERJOB_SUCCESS = '[Projects] Publish UserJob success';
  static readonly PUBLISH_USERJOB_FAIL = '[Projects] Publish UserJob fail';
  static readonly CANCEL_USERJOB = '[Projects] Cancel UserJob start';
  static readonly CANCEL_USERJOB_SUCCESS = '[Projects] Cancel UserJob success';
  static readonly CANCEL_USERJOB_FAIL = '[Projects] Cancel UserJob fail';
  static readonly EDIT_USERJOB = '[Projects] Edit UserJob';

  loadMyCompanyProjects(): Action {
    return {
      type: ProjectsActions.LOAD_MY_COMPANY_PROJECTS,
      payload: null
    };
  }

  loadMyCompanyProjectsSuccess(payload: NormalizedResponse): Action {
    return {
      type: ProjectsActions.LOAD_MY_COMPANY_PROJECTS_SUCCESS,
      payload: payload
    };
  }

  loadMyCompanyProjectsFail(payload: ApiError): Action {
    return {
      type: ProjectsActions.LOAD_MY_COMPANY_PROJECTS_FAIL,
      payload: payload
    };
  }

  refreshForm(): Action {
    return {
      type: ProjectsActions.REFRESH_FORM,
      payload: null
    };
  }

  refreshUserJobForm(): Action {
    return {
      type: ProjectsActions.REFRESH_USERJOB_FORM,
      payload: null
    };
  }

  createProject(payload: any): Action {
    return {
      type: ProjectsActions.CREATE_PROJECT,
      payload: payload
    };
  }

  createProjectSuccess(payload: NormalizedResponse): Action {
    return {
      type: ProjectsActions.CREATE_PROJECT_SUCCESS,
      payload: payload
    };
  }

  createProjectFail(errorData: FormErrorResponse): Action {
    return {
      type: ProjectsActions.CREATE_PROJECT_FAIL,
      payload: errorData
    };
  }

  saveProject(id: number, project: any): Action {
    return {
      type: ProjectsActions.SAVE_PROJECT,
      payload: {id: id, project: project}
    };
  }

  saveProjectSuccess(payload: NormalizedResponse): Action {
    return {
      type: ProjectsActions.SAVE_PROJECT_SUCCESS,
      payload: payload
    };
  }

  saveProjectFail(errorData: FormErrorResponse): Action {
    return {
      type: ProjectsActions.SAVE_PROJECT_FAIL,
      payload: errorData
    };
  }

  newUserJobForm(): Action {
    return {
      type: ProjectsActions.NEW_USERJOB_FORM,
      payload: null
    };
  }

  closeUserJobForm(): Action {
    return {
      type: ProjectsActions.CLOSE_USERJOB_FORM,
      payload: null
    };
  }

  createUserJob(projectId, userJob: UserJob): Action {
    return {
      type: ProjectsActions.CREATE_USERJOB,
      payload: {projectId, userJob}
    };
  }

  createUserJobSuccess(payload: NormalizedResponse): Action {
    return {
      type: ProjectsActions.CREATE_USERJOB_SUCCESS,
      payload: payload
    };
  }

  createUserJobFail(errorData: FormErrorResponse): Action {
    return {
      type: ProjectsActions.CREATE_USERJOB_FAIL,
      payload: errorData
    };
  }

  saveUserJob(projectId, userJobId: any, userJob: UserJob): Action {
    return {
      type: ProjectsActions.SAVE_USERJOB,
      payload: {projectId, userJobId, userJob}
    };
  }

  saveUserJobSuccess(payload: NormalizedResponse): Action {
    return {
      type: ProjectsActions.SAVE_USERJOB_SUCCESS,
      payload: payload
    };
  }

  saveUserJobFail(errorData: FormErrorResponse): Action {
    return {
      type: ProjectsActions.SAVE_USERJOB_FAIL,
      payload: errorData
    };
  }

  publishUserJob(editingUserJobId: number): Action {
    return {
      type: ProjectsActions.PUBLISH_USERJOB,
      payload: editingUserJobId
    };
  }

  publishUserJobSuccess(response: NormalizedResponse): Action {
    return {
      type: ProjectsActions.PUBLISH_USERJOB_SUCCESS,
      payload: response
    };
  }

  publishUserJobFail(error: ApiError): Action {
    return {
      type: ProjectsActions.PUBLISH_USERJOB_FAIL,
      payload: error
    };
  }

  cancelUserJob(editingUserJobId: number): Action {
    return {
      type: ProjectsActions.CANCEL_USERJOB,
      payload: editingUserJobId
    };
  }

  cancelUserJobSuccess(response: NormalizedResponse): Action {
    return {
      type: ProjectsActions.CANCEL_USERJOB_SUCCESS,
      payload: response
    };
  }

  cancelUserJobFail(error: ApiError): Action {
    return {
      type: ProjectsActions.CANCEL_USERJOB_FAIL,
      payload: error
    };
  }

  editUserJob(userJob: UserJob): Action {
    return {
      type: ProjectsActions.EDIT_USERJOB,
      payload: userJob
    };
  }

}
