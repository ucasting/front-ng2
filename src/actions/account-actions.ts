import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {FormErrorResponse} from '../models/form-error-response.interface';

@Injectable()
export class AccountActions {

  static readonly EDIT_ACCOUNT = '[Account] Edit account';
  static readonly EDIT_ACCOUNT_SUCCESS = '[Account] Edit account success';
  static readonly EDIT_ACCOUNT_FAIL = '[Account] Edit account fail';
  static readonly EDIT_AVATAR = '[Account] Edit avatar ';
  static readonly EDIT_AVATAR_SUCCESS = '[Account] Edit avatar success';
  static readonly EDIT_AVATAR_FAIL = '[Account] Edit avatar fail';
  static readonly EDIT_COVER = '[Account] Edit cover ';
  static readonly EDIT_COVER_SUCCESS = '[Account] Edit cover success';
  static readonly EDIT_COVER_FAIL = '[Account] Edit cover fail';

  editAccount(accountData: any): Action {
    return {
      type: AccountActions.EDIT_ACCOUNT,
      payload: accountData
    };
  }

  editAccountSuccess(accountData: any): Action {
    return {
      type: AccountActions.EDIT_ACCOUNT_SUCCESS,
      payload: accountData
    };
  }


  editAccountFail(errorData: FormErrorResponse): Action {
    return {
      type: AccountActions.EDIT_ACCOUNT_FAIL,
      payload: errorData
    };
  }


  editAvatar(accountData: any): Action {
    return {
      type: AccountActions.EDIT_AVATAR,
      payload: accountData
    };
  }


  editAvatarSuccess(accountData: any): Action {
    return {
      type: AccountActions.EDIT_AVATAR_SUCCESS,
      payload: accountData
    };
  }


  editAvatarFail(errorData: any): Action {
    return {
      type: AccountActions.EDIT_AVATAR_FAIL,
      payload: errorData
    };
  }


  editCover(accountData: any): Action {
    return {
      type: AccountActions.EDIT_COVER,
      payload: accountData
    };
  }


  editCoverSuccess(accountData: any): Action {
    return {
      type: AccountActions.EDIT_COVER_SUCCESS,
      payload: accountData
    };
  }


  editCoverFail(errorData: any): Action {
    return {
      type: AccountActions.EDIT_COVER_FAIL,
      payload: errorData
    };
  }
}
