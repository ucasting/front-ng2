import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class CompanyJobsActions {
  static readonly POST_JOB = '[CompanyJobs] Post Job';
  static readonly POST_JOB_SUCCESS = '[CompanyJobs] Post Job success';
  static readonly POST_JOB_FAIL = '[CompanyJobs] Post Job fail';

  postJob(payload: any): Action {
    return {
      type: CompanyJobsActions.POST_JOB,
      payload: payload
    };
  }


  postJobSuccess(payload: any): Action {
    return {
      type: CompanyJobsActions.POST_JOB_SUCCESS,
      payload: payload
    };
  }


  postJobFail(payload: any): Action {
    return {
      type: CompanyJobsActions.POST_JOB_FAIL,
      payload: payload
    };
  }
}
