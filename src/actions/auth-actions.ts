import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class AuthActions {
  static readonly LOGIN = '[Auth] Login';
  static readonly LOGIN_SUCCESS = '[Auth] Login Success';
  static readonly LOGIN_FAIL = '[Auth] Login Fail';
  static readonly LOGOUT = '[Auth] Logout';

  login(username: string, password: string): Action {
    return {
      type: AuthActions.LOGIN,
      payload: {_username: username, _password: password}
    };
  }


  loginSuccess(authData: {token: string, userId: string, profileId?: string, companyId?: string}): Action {
    return {
      type: AuthActions.LOGIN_SUCCESS,
      payload: authData
    };
  }


  loginFail(error: any): Action {
    return {
      type: AuthActions.LOGIN_FAIL,
      payload: error
    };
  }


  logout(): Action {
    return {
      type: AuthActions.LOGOUT,
      payload: null
    };
  }
}
