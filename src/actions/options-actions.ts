import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class OptionsActions {
  static readonly LOAD = '[Options] Load start';
  static readonly LOAD_SUCCESS = '[Options] Load success';
  static readonly LOAD_FAIL = '[Options] Load fail';

  load(payload: any): Action {
    return {
      type: OptionsActions.LOAD,
      payload: null
    };
  }


  loadSuccess(payload: any): Action {
    return {
      type: OptionsActions.LOAD_SUCCESS,
      payload: payload
    };
  }


  loadFail(payload: any): Action {
    return {
      type: OptionsActions.LOAD_FAIL,
      payload: payload
    };
  }
}
