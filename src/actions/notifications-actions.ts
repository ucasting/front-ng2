import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';

@Injectable()
export class NotificationsActions {
  static readonly ADD = '[Notifications] Add notification';
  static readonly REMOVE = '[Notifications] Remove notification';

  add(title: string, message: string, type: string, id: number): Action {
    return {
      type: NotificationsActions.ADD,
      payload: {
        id: id,
        title: title,
        message: message,
        type: type
      }
    };
  }


  remove(id: number): Action {
    return {
      type: NotificationsActions.REMOVE,
      payload: {id: id}
    };
  }

}
