import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {User} from '../models/user.model';
import {UserJob} from '../models/user-job.model';
import {NormalizedResponse} from '../models/schemas';
import {FormErrorResponse} from '../models/form-error-response.interface';
import {ApiError} from '../models/api-error.interface';

@Injectable()
export class UserJobsActions {
  static readonly REFRESH_FORM = '[UserJobs] refresh job form';
  static readonly CREATE_JOB = '[UserJobs] Create job';
  static readonly CREATE_JOB_SUCCESS = '[UserJobs] Create job success';
  static readonly CREATE_JOB_FAIL = '[UserJobs] Create job failed';
  static readonly SAVE_JOB = '[UserJobs] Save job';
  static readonly SAVE_JOB_SUCCESS = '[UserJobs] Save job success';
  static readonly SAVE_JOB_FAIL = '[UserJobs] Save job failed';
  static readonly PUBLISH_JOB = '[UserJobs] Publish job';
  static readonly PUBLISH_JOB_SUCCESS = '[UserJobs] Publish job success';
  static readonly PUBLISH_JOB_FAIL = '[UserJobs] Publish job failed';
  static readonly INVITE_CANDIDATE = '[UserJobs] Candidate invited';
  static readonly INVITE_CANDIDATE_SUCCESS = '[UserJobs] Candidate invited success';
  static readonly INVITE_CANDIDATE_FAIL = '[UserJobs] Candidate invited fail';
  static readonly SELECT_CANDIDATE = '[UserJobs] Candidate selected';
  static readonly SELECT_CANDIDATE_SUCCESS = '[UserJobs] Candidate selected success';
  static readonly SELECT_CANDIDATE_FAIL = '[UserJobs] Candidate selected fail';
  static readonly SELECT_CANDIDATE_LEGACY = '[UserJobs] Candidate selected (legacy)';
  static readonly SELECT_CANDIDATE_LEGACY_SUCCESS = '[UserJobs] Candidate selected success (legacy)';
  static readonly SELECT_CANDIDATE_LEGACY_FAIL = '[UserJobs] Candidate selected fail (legacy)';


  refreshForm(): Action {
    return {
      type: UserJobsActions.REFRESH_FORM,
      payload: null
    };
  }


  createJob(projectId, payload: any): Action {
    return {
      type: UserJobsActions.CREATE_JOB,
      payload: {projectId: projectId, job: payload}
    };
  }


  createJobSuccess(payload: NormalizedResponse): Action {
    return {
      type: UserJobsActions.CREATE_JOB_SUCCESS,
      payload: payload
    };
  }


  createJobFail(errorData: FormErrorResponse): Action {
    return {
      type: UserJobsActions.CREATE_JOB_FAIL,
      payload: errorData
    };
  }


  saveJob(projectId, id: number, job: any): Action {
    return {
      type: UserJobsActions.SAVE_JOB,
      payload: {id: id, projectId: projectId, job: job}
    };
  }


  saveJobSuccess(payload: NormalizedResponse): Action {
    return {
      type: UserJobsActions.SAVE_JOB_SUCCESS,
      payload: payload
    };
  }


  saveJobFail(errorData: FormErrorResponse): Action {
    return {
      type: UserJobsActions.SAVE_JOB_FAIL,
      payload: errorData
    };
  }


  publishJob(payload: any): Action {
    return {
      type: UserJobsActions.PUBLISH_JOB,
      payload: payload
    };
  }


  publishJobSuccess(payload: NormalizedResponse): Action {
    return {
      type: UserJobsActions.PUBLISH_JOB_SUCCESS,
      payload: payload
    };
  }


  publishJobFail(): Action {
    return {
      type: UserJobsActions.PUBLISH_JOB_FAIL,
      payload: null
    };
  }


  inviteCandidate(job: UserJob, candidate: User): Action {
    return {
      type: UserJobsActions.INVITE_CANDIDATE,
      payload: {job: job, candidate: candidate}
    };
  }


  inviteCandidateSuccess(result: any): Action {
    return {
      type: UserJobsActions.INVITE_CANDIDATE_SUCCESS,
      payload: result
    };
  }


  inviteCandidateFail(error: ApiError, candidate, job): Action {
    return {
      type: UserJobsActions.INVITE_CANDIDATE_FAIL,
      payload: { error, candidate, job}
    };
  }


  selectCandidate(job: UserJob, candidate: User): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE,
      payload: {job: job, candidate: candidate}
    };
  }


  selectCandidateSuccess(result: any): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE_SUCCESS,
      payload: result
    };
  }


  selectCandidateFail(): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE_FAIL,
      payload: null
    };
  }


  selectCandidateLegacy(candidate: User): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE_LEGACY,
      payload: candidate
    };
  }


  selectCandidateLegacySuccess(): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE_LEGACY_SUCCESS,
      payload: null
    };
  }


  selectCandidateLegacyFail(): Action {
    return {
      type: UserJobsActions.SELECT_CANDIDATE_LEGACY_FAIL,
      payload: null
    };
  }

}
