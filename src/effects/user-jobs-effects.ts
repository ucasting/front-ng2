import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {normalize} from 'normalizr';
import {userJob} from '../models/schemas';
import {NotificationsService} from '../services/notifications.service';
import {UserJobsActions} from '../actions/user-jobs-actions';
import {UserJobsApiService} from '../services/user-jobs-api.service';


@Injectable()
export class UserJobsEffects {

  @Effect() inviteCandidate$ = this.actions$
    .ofType(UserJobsActions.INVITE_CANDIDATE)
    // Gets the payload from the action
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addInfo('Seleção', `Convidando ${payload.candidate.firstName}...`);
    })
    // only care about the most recent result - aka flatMapLatest
    .mergeMap(payload => this.api.inviteCandidate(payload.job.id, payload.candidate.id)
      .retry(1)
      // If successful, dispatch success action with result
      .map(res => {
        const normalized = normalize(res.json(), userJob);
        return this.userJobsActions.inviteCandidateSuccess(normalized);
      })
      // If request fails, dispatch failed action
      .catch((res) => Observable.of(
        this.userJobsActions.inviteCandidateFail(
          res.json(),
          payload.candidate,
          payload.job
        ))
      )
    );

  @Effect() inviteCandidateSuccess$ = this.actions$
    .ofType(UserJobsActions.INVITE_CANDIDATE_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Seleção', 'Candidato convidado com sucesso.');
    })
    .filter(() => false);

  @Effect() inviteCandidateFail$ = this.actions$
    .ofType(UserJobsActions.INVITE_CANDIDATE_FAIL)
    .do((action) => {
      this.notificationsService.addDanger('Seleção', `${action.payload.error.message}`);
    })
    .filter(() => false);

  @Effect() selectCandidateLegacy$ = this.actions$
    .ofType(UserJobsActions.SELECT_CANDIDATE_LEGACY)
    // Gets the payload from the action
    .map(action => action.payload)
    .do((payload) => {
      console.log(payload);
      this.notificationsService.addInfo('Seleção', `Selecionando ${payload.firstName}...`);
    })
    // only care about the most recent result - aka flatMapLatest
    .switchMap(payload => this.http.post(
      `${environment.APIPath}selections/select-candidate/${payload.id}`, {})
      .retry(3)
      // If successful, dispatch success action with result
      .map(res => {
        return this.userJobsActions.selectCandidateLegacySuccess();
      })
      // If request fails, dispatch failed action
      .catch((res) => Observable.of(this.userJobsActions.selectCandidateLegacyFail()))
    );

  @Effect() selectCandidateLegacySuccess$ = this.actions$
    .ofType(UserJobsActions.SELECT_CANDIDATE_LEGACY_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Seleção', 'Candidato selecionado com sucesso.');
    })
    .filter(() => false);

  @Effect() selectCandidateLegacyFail$ = this.actions$
    .ofType(UserJobsActions.SELECT_CANDIDATE_LEGACY_FAIL)
    .do(() => {
      this.notificationsService.addDanger('Seleção', 'Erro selecionando candidato.');
    })
    .filter(() => false);

  constructor(private http: AuthHttp,
              private actions$: Actions,
              private userJobsActions: UserJobsActions,
              private notificationsService: NotificationsService,
              private api: UserJobsApiService) {
  }

}
