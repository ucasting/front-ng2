import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {UsersActions} from '../actions/users-actions';
import {user} from '../models/schemas';
import {normalize, arrayOf} from 'normalizr';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {URLSearchParams} from '@angular/http';
import {convertPayloadToSearchParams} from '../utils/helpers';
import {AppState} from '../reducers/index';
import {Store} from '@ngrx/store';

@Injectable()
export class UsersEffects {

  @Effect() searchCandidates$ = this.actions$
    .ofType(UsersActions.SEARCH_CANDIDATES)
    .debounceTime(5)
    .withLatestFrom(this.store$)
    .map(([action, state]) => {
      let usersState = state.users;
      // merge filters and current page
      return Object.assign({}, usersState.candidatesFilters, {page: usersState.currentCandidatesPage});
    })
    // .do(payload => console.log(payload))
    // only care about the most recent result - aka flatMapLatest
    .switchMap(
      payload => {
        // adds the eligible filter
        payload.eligible = true;
        let params: URLSearchParams = convertPayloadToSearchParams(payload);
        return this.http.get(`${environment.APIPath}users`, {search: params})
        // If successful, dispatch success action with result
          .retry(3)
          .map(res => {
            let result = res.json();
            let normalized = normalize(result.items, arrayOf(user));
            result.entities = normalized.entities;
            result.ids = normalized.result;
            return this.usersActions.searchCandidatesSuccess(result);
          })
          // If request fails, dispatch failed action
          .catch((res) => Observable.of(this.usersActions.searchCandidatesFail(res.json())));
      }
    );

  @Effect() candidatesSearchChanged$ = this.actions$
    .ofType(UsersActions.CHANGE_CANDIDATES_FILTERS, UsersActions.CHANGE_CANDIDATES_PAGE)
    .map(() => {
      return this.usersActions.searchCandidates();
    });

  constructor(private http: AuthHttp, private actions$: Actions, private store$: Store<AppState>, private usersActions: UsersActions) {
  }

}
