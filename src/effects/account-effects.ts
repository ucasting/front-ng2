import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {AccountActions} from '../actions/account-actions';
import {Observable} from 'rxjs/Rx';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {environment} from '../environments/environment';
import {user} from '../models/schemas';
import {normalize} from 'normalizr';
import {NotificationsService} from '../services/notifications.service';


@Injectable()
export class AccountEffects {

  @Effect() editAccount$ = this.actions$
    .ofType(AccountActions.EDIT_ACCOUNT)
    // Gets the payload from the update action
    .map(action => action.payload)
    // only care about the most recent result - aka flatMapLatest
    .switchMap(payload => this.http.post(`${environment.APIPath}my/account`, JSON.stringify(payload))
      // If successful, dispatch success action with result
        .map(res => {
          return this.accountActions.editAccountSuccess(normalize(res.json(), user));
        })
        // If request fails, dispatch failed action
        .catch((res) =>
          Observable
            .of(this.accountActions.editAccountFail(res.json()))
            .do(() => {
              this.notificationsService.addDanger('Editar perfil', 'Erro editando perfil.');
            })
        )
    );

  @Effect() editAccountSuccess$ = this.actions$
    .ofType(AccountActions.EDIT_ACCOUNT_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Editar perfil', 'Seu perfil foi atualizado com sucesso.');
    })
    .filter(() => false);

  @Effect() editAvatarSuccess$ = this.actions$
    .ofType(AccountActions.EDIT_AVATAR_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Avatar', 'Seu avatar foi atualizado com sucesso.');
    })
    .filter(() => false);

  @Effect() editCoverSuccess$ = this.actions$
    .ofType(AccountActions.EDIT_COVER_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Capa', 'Sua capa foi atualizada com sucesso.');
    })
    .filter(() => false);

  @Effect() editAvatarFail$ = this.actions$
    .ofType(AccountActions.EDIT_AVATAR_FAIL)
    .do(() => {
      this.notificationsService.addDanger('Avatar', 'Erro atualizando avatar.');
    })
    .filter(() => false);

  @Effect() editCoverFail$ = this.actions$
    .ofType(AccountActions.EDIT_COVER_FAIL)
    .do(() => {
      this.notificationsService.addDanger('Capa', 'Erro atualizando capa.');
    })
    .filter(() => false);

  constructor(private http: AuthHttp, private actions$: Actions,
              private accountActions: AccountActions,
              private notificationsService: NotificationsService) {
  }

}
