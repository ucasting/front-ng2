import {Injectable, Inject} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Effect, Actions} from '@ngrx/effects';
import {AuthActions} from '../actions/auth-actions';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {normalize} from 'normalizr';
import {user} from '../models/schemas';
import {JwtHelper} from 'angular2-jwt/angular2-jwt';
import {NotificationsService} from '../services/notifications.service';
import {Store} from '@ngrx/store';
import {go} from '@ngrx/router-store';
import {WindowObject} from '../utils/window.token';

@Injectable()
export class AuthEffects {

  private defaultHeaders: Headers;

  private jwtHelper: JwtHelper;

  private tokenName = 'id_token';

  private window: Window;

  @Effect() login$ = this.actions$
  // Listen for the 'LOGIN' action
    .ofType(AuthActions.LOGIN)
    // Map the payload into JSON to use as the request body
    .map(action => action.payload)
    .switchMap(payload => {
        return this.http.post(`${environment.APIPath}login_check`,
          // JSON.stringify(payload)
          payload
        )
        // If successful, dispatch success action with result
          .map(res => res.json())
          .map((data) => {
            const userId = data.data.user.id;
            let profileId = null;
            let companyId = null;
            if (data.data.user.profile) {
              profileId = data.data.user.profile.id;
            }
            if (data.data.user.company) {
              companyId = data.data.user.company.id;
            }
            const normalizedUser = normalize(data.data.user, user);
            const actionPayload = Object.assign({}, normalizedUser,
              {
                token: data.token,
                userId: userId,
                profileId: profileId,
                companyId: companyId
              });
            localStorage.setItem(this.tokenName, data.token);
            return this.authActions.loginSuccess(actionPayload);
          })
          // If request fails, dispatch failed action
          .catch((res) => {
              return Observable
                .of(this.authActions.loginFail(res))
                .do(() => {
                  this.notificationsService.addDanger('Autenticação', 'Erro efetuando login.');
                });
            }
          );
      }
    );

  @Effect() loginSuccess$ = this.actions$
    .ofType(AuthActions.LOGIN_SUCCESS)
    .map(action => action.payload)
    // Gets the payload from the update action
    .do(() => {
      this.notificationsService.addSuccess('Autenticação', 'Login efetuado com sucesso.');
    })
    .filter(() => false)
    ;


  @Effect() logout$ = this.actions$
    .ofType(AuthActions.LOGOUT)
    .do(() => {
      this.notificationsService.addSuccess('Sair', 'Sua sessão foi encerrada.');
      // removes token and redirects to login
      localStorage.removeItem(this.tokenName);
      this.store.dispatch(go(['/security/login']));
    })
    .filter(() => false)
    ;

  constructor(private http: Http,
              private actions$: Actions,
              private authActions: AuthActions,

              private notificationsService: NotificationsService,
              private store: Store<any>) {
    this.defaultHeaders = new Headers();
    this.defaultHeaders.append('Content-type', 'application/json');
    this.jwtHelper = new JwtHelper();
  }
}
