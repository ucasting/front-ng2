import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Rx';
import {OptionsActions} from '../actions/options-actions';
import {environment} from '../environments/environment';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {AuthActions} from '../actions/auth-actions';
import {normalize} from 'normalizr';
import {option} from '../models/schemas';
import {AccountService} from '../services/account.service';
import {ROLE_CANDIDATE, ROLE_COMPANY_USER} from '../models/roles.constants';
import {go} from '@ngrx/router-store';
import {Store} from '@ngrx/store';


@Injectable()
export class OptionsEffects {

  @Effect() load$ = this.actions$
    .ofType(OptionsActions.LOAD, AuthActions.LOGIN_SUCCESS)
    // Gets the payload from the update action
    .map(action => action.payload)
    // only care about the most recent result - aka flatMapLatest
    .switchMap(payload => this.http.get(`${environment.APIPath}util/basic-data`)
      .retry(2)
      // If successful, dispatch success action with result
      .map(res => {
        const normalized = normalize(res.json(), option);
        delete normalized.entities.options;
        return this.optionsActions.loadSuccess(normalized);
      })
      // If request fails, dispatch failed action
      .catch((res) => Observable.of(this.optionsActions.loadFail(res.json())))
    );

  @Effect() optionsLoadedSuccess = this.actions$
    .ofType(OptionsActions.LOAD_SUCCESS)
    .map(action => action.payload)
    // Gets the payload from the update action
    .do(() => {
      if (this.accountService.hasRole(ROLE_CANDIDATE)) {
        this.store.dispatch(go(['/main/candidate/edit-account']));
      } else if (this.accountService.hasRole(ROLE_COMPANY_USER)) {
        this.store.dispatch(go(['/main/hirer/projects']));
      }
    })
    .filter(() => false)
    ;

  constructor(private http: AuthHttp,
              private actions$: Actions,
              private optionsActions: OptionsActions,
              private accountService: AccountService,
              private store: Store<any>) {

  }
}
