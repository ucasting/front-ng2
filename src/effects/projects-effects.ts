import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {Response} from '@angular/http';
import {normalize, arrayOf} from 'normalizr';
import {project, NormalizedResponse, userJob} from '../models/schemas';
import {NotificationsService} from '../services/notifications.service';
import {FormErrorResponse} from '../models/form-error-response.interface';
import {ProjectsActions} from '../actions/projects-actions';
import {ApiError} from '../models/api-error.interface';
import {Store} from '@ngrx/store';
import {go} from '@ngrx/router-store';


@Injectable()
export class ProjectsEffects {

  @Effect() loadMyCompanyProjects$ = this.actions$
    .ofType(ProjectsActions.LOAD_MY_COMPANY_PROJECTS)
    .map(action => action.payload)
    .switchMap(payload => this.http.get(`${environment.APIPath}projects/my/company`)
      .map((res: Response) => {
        const normalized = normalize(res.json(), arrayOf(project));
        return this.projectsActions.loadMyCompanyProjectsSuccess(normalized);
      })
      .retry(2)
      .catch((res) => {
        const apiError: ApiError = res.json();
        return Observable.of(this.projectsActions.loadMyCompanyProjectsFail(apiError));
      })
    );

  @Effect() loadMyCompanyProjectsSuccess$ = this.actions$
    .ofType(ProjectsActions.LOAD_MY_COMPANY_PROJECTS_SUCCESS)
    .map(action => action.payload)
    .do((payload) => {
      console.log(payload);
    })
    .filter(() => false);

  @Effect() loadMyCompanyProjectsFail$ = this.actions$
    .ofType(ProjectsActions.LOAD_MY_COMPANY_PROJECTS_FAIL)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addDanger('Trabalhos', 'Erro carregando trabalhos.');
    })
    .filter(() => false);

  @Effect() createProject$ = this.actions$
    .ofType(ProjectsActions.CREATE_PROJECT)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addInfo('Projeto', 'Criando projeto...');
    })
    .switchMap(payload => this.http.post(`${environment.APIPath}projects`, payload)
      .map((res: Response) => {
        const normalized = normalize(res.json(), project);
        return this.projectsActions.createProjectSuccess(normalized);
      })
      .catch((res) => {
        const formErrorResponse: FormErrorResponse = res.json();
        return Observable.of(this.projectsActions.createProjectFail(formErrorResponse));
      })
    )
  ;

  @Effect() saveProject$ = this.actions$
    .ofType(ProjectsActions.SAVE_PROJECT)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addInfo('Projeto', 'Salvando projeto...');
    })
    .switchMap(payload => this.http.post(`${environment.APIPath}projects/${payload.id}`, payload.project)
      .map((res: Response) => {
        const normalized = normalize(res.json(), project);
        return this.projectsActions.saveProjectSuccess(normalized);
      })
      .catch((res) => {
        const formErrorResponse: FormErrorResponse = res.json();
        return Observable.of(this.projectsActions.saveProjectFail(formErrorResponse));
      })
    )
  ;

  @Effect() createProjectSuccess$ = this.actions$
    .ofType(ProjectsActions.CREATE_PROJECT_SUCCESS)
    .map(action => action.payload)
    .do((payload: NormalizedResponse) => {
      this.store.dispatch(go(['/main/hirer/projects', payload.result]));
      this.notificationsService.addSuccess('Projeto', 'Projeto criado com sucesso.');
    })
    .filter(() => false);

  @Effect() saveProjectSuccess$ = this.actions$
    .ofType(ProjectsActions.SAVE_PROJECT_SUCCESS)
    .map(action => action.payload)
    .do((payload) => {
      this.store.dispatch(go(['/main/hirer/projects', payload.result]));
      this.notificationsService.addSuccess('Projeto', 'Projeto salvo com sucesso.');
    })
    .filter(() => false);

  @Effect() createProjectFail$ = this.actions$
    .ofType(ProjectsActions.CREATE_PROJECT_FAIL)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addDanger('Projeto', 'Erro criando projeto.');
    })
    .filter(() => false);

  @Effect() saveProjectFail$ = this.actions$
    .ofType(ProjectsActions.SAVE_PROJECT_FAIL)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addDanger('Projeto', 'Erro salvando projeto.');
    })
    .filter(() => false);


  @Effect() createJob$ = this.actions$
    .ofType(ProjectsActions.CREATE_USERJOB)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addInfo('Perfil', 'Criando Perfil...');
    })
    .switchMap(payload => this.http.post(`${environment.APIPath}projects/${payload.projectId}/userjobs`, payload.userJob)
      .map((res: Response) => {
        const normalized = normalize(res.json(), userJob);
        return this.projectsActions.createUserJobSuccess(normalized);
      })
      .catch((res) => {
        const formErrorResponse: FormErrorResponse = res.json();
        return Observable.of(this.projectsActions.createUserJobFail(formErrorResponse));
      })
    )
  ;

  @Effect() saveJob$ = this.actions$
    .ofType(ProjectsActions.SAVE_USERJOB)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addInfo('Perfil', 'Salvando Perfil...');
    })
    .switchMap(payload => this.http.post(`${environment.APIPath}userjobs/${payload.userJobId}`, payload.userJob)
      .map((res: Response) => {

        const normalized = normalize(res.json(), userJob);
        return this.projectsActions.saveUserJobSuccess(normalized);
      })
      .catch((res) => {
        const formErrorResponse: FormErrorResponse = res.json();
        return Observable.of(this.projectsActions.saveUserJobFail(formErrorResponse));
      })
    )
  ;

  @Effect() createJobSuccess$ = this.actions$
    .ofType(ProjectsActions.CREATE_USERJOB_SUCCESS)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addSuccess('Perfil', 'Perfil criado com sucesso.');
    })
    .filter(() => false);

  @Effect() saveJobSuccess$ = this.actions$
    .ofType(ProjectsActions.SAVE_USERJOB_SUCCESS)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addSuccess('Perfil', 'Perfil salvo com sucesso.');
    })
    .filter(() => false);

  @Effect() createJobFail$ = this.actions$
    .ofType(ProjectsActions.CREATE_USERJOB_FAIL)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addDanger('Perfil', 'Erro criando Perfil.');
    })
    .filter(() => false);

  @Effect() saveJobFail$ = this.actions$
    .ofType(ProjectsActions.SAVE_USERJOB_FAIL)
    .map(action => action.payload)
    .do((payload) => {
      this.notificationsService.addDanger('Perfil', 'Erro salvando Perfil.');
    })
    .filter(() => false);

  @Effect() publishUserJob$ = this.actions$
    .ofType(ProjectsActions.PUBLISH_USERJOB)
    .do((action) => {
      this.notificationsService.addInfo('Perfil', 'Publicando Perfil...');
    })
    .switchMap(action => this.http.post(`${environment.APIPath}userjobs/${action.payload}/publish`, {})
      .map((res: Response) => {
        const normalized = normalize(res.json(), userJob);
        return this.projectsActions.publishUserJobSuccess(normalized);
      })
      .catch((res) => {
        const apiError: ApiError = res.json();
        return Observable.of(this.projectsActions.publishUserJobFail(apiError));
      })
    )
  ;

  @Effect() publishUserJobSuccess$ = this.actions$
    .ofType(ProjectsActions.PUBLISH_USERJOB_SUCCESS)
    .do((action) => {
      this.notificationsService.addSuccess('Perfil', 'Perfil publicado com sucesso.');
    })
    .filter(() => false);

  @Effect() publishUserJobFail$ = this.actions$
    .ofType(ProjectsActions.PUBLISH_USERJOB_FAIL)
    .do((action) => {
      this.notificationsService.addDanger('Perfil', 'Erro publicando perfil. ' + action.payload.message);
    })
    .filter(() => false);


  @Effect() cancelUserJob$ = this.actions$
    .ofType(ProjectsActions.CANCEL_USERJOB)
    .do((action) => {
      this.notificationsService.addInfo('Perfil', 'Cancelando Perfil...');
    })
    .switchMap(action => this.http.post(`${environment.APIPath}userjobs/${action.payload}/cancel`, {})
      .map((res: Response) => {
        const normalized = normalize(res.json(), userJob);
        return this.projectsActions.cancelUserJobSuccess(normalized);
      })
      .catch((res) => {
        const apiError: ApiError = res.json();
        return Observable.of(this.projectsActions.cancelUserJobFail(apiError));
      })
    )
  ;

  @Effect() cancelUserJobSuccess$ = this.actions$
    .ofType(ProjectsActions.CANCEL_USERJOB_SUCCESS)
    .do((action) => {
      this.notificationsService.addSuccess('Perfil', 'Perfil cancelado com sucesso.');
    })
    .filter(() => false);

  @Effect() cancelUserJobFail$ = this.actions$
    .ofType(ProjectsActions.CANCEL_USERJOB_FAIL)
    .do((action) => {
      this.notificationsService.addDanger('Perfil', 'Erro cancelando perfil. ' + action.payload.message);
    })
    .filter(() => false);

  constructor(private http: AuthHttp,
              private actions$: Actions,
              private projectsActions: ProjectsActions,
              private notificationsService: NotificationsService,
              private store: Store<any>) {
  }

}
