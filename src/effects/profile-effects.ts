import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {ProfileActions} from '../actions/profile-actions';
import {Observable} from 'rxjs/Rx';
import {environment} from '../environments/environment';
import {AuthHttp} from 'angular2-jwt/angular2-jwt';
import {normalize} from 'normalizr';
import {gallery, profile, NormalizedResponse} from '../models/schemas';
import {NotificationsService} from '../services/notifications.service';
import {ApiError} from '../models/api-error.interface';

@Injectable()
export class ProfileEffects {

  @Effect() addAlbum$ = this.actions$
    .ofType(ProfileActions.ADD_ALBUM)
    // Gets the payload from the update action
    .map(action => action.payload)
    .mergeMap(payload => this.http.post(`${environment.APIPath}my/profile/album`, {})
      // If successful, dispatch success action with result
        .map(res => {
          return this.profileActions.addAlbumSuccess(
            normalize(res.json(), profile) as NormalizedResponse
          );
        })
        // If request fails, dispatch failed action
        .catch((res) => Observable.of(this.profileActions.addAlbumFail(res.json())))
    );

  @Effect() addAlbumSuccess$ = this.actions$
    .ofType(ProfileActions.ADD_ALBUM_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Portfolio', 'Album criado com sucesso.');
    })
    .filter(() => false);


  @Effect() editAlbum$ = this.actions$
    .ofType(ProfileActions.EDIT_ALBUM)
    // Gets the payload from the update action
    .map(action => action.payload)
    .switchMap(payload => this.http.post(`${environment.APIPath}my/profile/album/${payload.id}`, payload.values)
      // If successful, dispatch success action with result
        .map(res => {
          return this.profileActions.editAlbumSuccess(normalize(res.json(), profile));
        })
        // If request fails, dispatch failed action
        .catch((res) => Observable.of(this.profileActions.editAlbumFail(res.json())))
    );

  @Effect() editAlbumSuccess$ = this.actions$
    .ofType(ProfileActions.EDIT_ALBUM_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Portfolio', 'Álbum atualizado com sucesso.');
    })
    .filter(() => false);

  @Effect() addAlbumImageSuccess$ = this.actions$
    .ofType(ProfileActions.ADD_ALBUM_IMAGE_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Album', 'Imagem adicionada com sucesso.');
    })
    .filter(() => false);

  @Effect() addAlbumImageFail$ = this.actions$
    .ofType(ProfileActions.ADD_ALBUM_IMAGE_FAIL)
    .map(action => action.payload)
    .do((apiError) => {
      this.notificationsService.addDanger('Album', apiError.message);
    })
    .filter(() => false);

  @Effect() removeAlbumFail$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM_FAIL)
    .map(action => action.payload)
    .do((message) => {
      this.notificationsService.addDanger('Album', message);
    })
    .filter(() => false);

  @Effect() removeAlbum$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM)
    // Gets the payload from the update action
    .map(action => action.payload)
    .mergeMap(payload => this.http.delete(`${environment.APIPath}my/profile/album/${payload.id}`)
      // If successful, dispatch success action with result
        .map(res => {
          return this.profileActions.removeAlbumSuccess(normalize(res.json(), profile));
        })
        // If request fails, dispatch failed action
        .catch((res) => {
          let apiError: ApiError = res.json();
          return Observable.of(this.profileActions.removeAlbumFail(apiError.message));
        })
    );

  @Effect() removeGalleryImage$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM_IMAGE)
    // Gets the payload from the update action
    .map(action => action.payload)
    .mergeMap(payload => this.http.delete(`${environment.APIPath}my/profile/album/image/${payload.id}`)
      // If successful, dispatch success action with result
        .map(res => {
          return this.profileActions.removeAlbumImageSuccess(normalize(res.json(), gallery));
        })
        .catch((res) => {
          let apiError: ApiError = res.json();
          return Observable.of(this.profileActions.removeAlbumImageFail(apiError.message));
        })
    );

  @Effect() removeAlbumSuccess$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Portfolio', 'Album removido com sucesso.');
    })
    .filter(() => false);

  @Effect() removeGalleryImageSuccess$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM_IMAGE_SUCCESS)
    .do(() => {
      this.notificationsService.addSuccess('Album', 'Imagem removida com sucesso.');
    })
    .filter(() => false);

  @Effect() removeGalleryImageFail$ = this.actions$
    .ofType(ProfileActions.REMOVE_ALBUM_IMAGE_FAIL)
    .do(() => {
      this.notificationsService.addDanger('Album', 'Erro removendo imagem.');
    })
    .filter(() => false);

  // @Effect() addImage$ = this.actions$
  //   .ofType(ProfileActions.ADD_ALBUM_IMAGE)
  //   // Gets the payload from the update action
  //   .map(action => action.payload)
  //   //only care about the most recent result - aka mergeMapLatest
  //   .switchMap(payload => this.http.post(`${environment.APIPath}my/profile/album/${payload.id}/image`, null)
  //     // If successful, dispatch success action with result
  //       .map(res => {
  //         return this.profileActions.removeAlbumSuccess(normalize(res.json(), profile));
  //       })
  //       // If request fails, dispatch failed action
  //       .catch((res) => Observable.of(this.profileActions.removeAlbumFail()))
  //   );

  constructor(private http: AuthHttp, private actions$: Actions,
              private profileActions: ProfileActions,
              private notificationsService: NotificationsService) {
  }


}
