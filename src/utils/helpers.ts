import {URLSearchParams} from '@angular/http';

export const STATES = this.states =
  [
    'SP', 'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES',
    'GO', 'MA', 'MT', 'MS', 'MG', 'PR', 'PB', 'PA', 'PE',
    'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SE', 'TO'
  ];

export const getIdOrNull = (entity: Entity) => {
  if (entity) {
    return entity.id;
  }
  return null;
};

interface Entity {
  id: string|number;
}

export const getIds = (entities: Entity[]) => {
  if (entities) {
    return entities.map(entity => entity.id);
  }
  return [];
};

export const convertPayloadToSearchParams = (payload: any): URLSearchParams => {
  let params = new URLSearchParams();
  let keys = Object.keys(payload);
  keys.forEach((key) => {
    let value = payload[key];
    if (Array.isArray(value)) {
      value.forEach((valueInArray) => {
        // adds a [] to array parameters to conform with PHP's $_GET way of interpreting arrays
        params.append(key + '[]', valueInArray);
      });
    } else {
      if (value !== null) {
        params.set(key, value);
      }
    }
  });
  return params;
};
