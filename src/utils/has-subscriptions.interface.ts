import {Subscription} from 'rxjs';
export interface HasSubscriptionsInterface {

  addSubscription(subscription: Subscription);

  removeSubscriptions();
}
