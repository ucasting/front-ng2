import {HasSubscriptionsInterface} from './has-subscriptions.interface';
import {Subscription} from 'rxjs';
import {OnDestroy} from '@angular/core';
export abstract class HasSubscriptions implements HasSubscriptionsInterface, OnDestroy {

  private subscriptions: Subscription[] = [];

  public addSubscription(subscription: Subscription) {
    this.subscriptions.push(subscription);
  }

  public removeSubscriptions() {
    this.subscriptions.forEach(
      (subscription) => {
        subscription.unsubscribe();
      }
    );
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(
      (subscription) => {
        subscription.unsubscribe();
      }
    );
  }
}
