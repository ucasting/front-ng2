require("harmonize")();
var gulp = require('gulp'),
    notify = require('gulp-notify'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    compass = require('gulp-compass'),
    spritesmith = require('gulp.spritesmith'),
    batch = require('gulp-batch'),
    merge = require('merge-stream'),
    cleancss = require('gulp-clean-css'),
    concatCss = require('gulp-concat-css'),
    sourcemaps = require('gulp-sourcemaps'),
    buffer = require('vinyl-buffer');


gulp.task('compass', function() {
    gulp.src('src/assets/*.scss')
        .pipe(compass({
            css: 'src/assets/css',
            sass: 'src/assets/scss',
            image:  'src/assets/img'
        }))
        .pipe(cleancss())
        .pipe(gulp.dest('src/assets/css'));
});

gulp.task('csslibs', function(){
    gulp.src(
            [
                'node_modules/bootstrap/dist/css/bootstrap.css',
                'node_modules/bootstrap/dist/css/bootstrap.css'
            ]
        )
        //.pipe(sourcemaps.init())
        .pipe(cleancss({
            debug: true,
            keepSpecialComments: 0
        }, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        //.pipe(sourcemaps.write())
        .pipe(concatCss('libs.min.css'))
        .pipe(gulp.dest('src/assets/css/'));
});

gulp.task('sprite', function(){
    var spriteData = gulp.src('src/assets/gen-sprites/*.png').pipe(spritesmith({
        imgName: 'sprites.png',
        cssName: '_sprite.scss',
        imgPath: '../img/sprites.png'
    }));

  // Pipe CSS stream through CSS optimizer and onto disk
  var cssStream = spriteData.css
    .pipe(gulp.dest('src/assets/scss/common/'));

    var imgStream = spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
        .pipe(buffer())
       // .pipe(imagemin())
        .pipe(gulp.dest('src/assets/img/'));

    return merge(imgStream, cssStream);
});

gulp.task('build', ['sprite', 'compass', 'csslibs'], function(){

});
