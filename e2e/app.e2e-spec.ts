import { UcastingAppPage } from './app.po';

describe('ucasting-app App', function() {
  let page: UcastingAppPage;

  beforeEach(() => {
    page = new UcastingAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
