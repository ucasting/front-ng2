import { UcastingFrontNg2Page } from './app.po';

describe('ucasting-app App', function() {
  let page: UcastingFrontNg2Page;

  beforeEach(() => {
    page = new UcastingFrontNg2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('ucasting-front-ng2 works!');
  });
});
